import React from 'react'
import IconArrowRight from './arrow-right.svg'

export default () => <IconArrowRight style={{ transform: 'scaleX(-1)' }} />
