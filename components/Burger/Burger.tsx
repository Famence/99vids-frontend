import React from 'react'
import cn from 'classnames'
import './Burger.sass'

export enum BurgerLayout {
  Left = 'left',
  Top = 'top',
}

interface BurgerProps {
  isActive: boolean
  onClick: React.MouseEventHandler
  layout?: BurgerLayout
  className?: string
}

export const Burger: React.FunctionComponent<BurgerProps> = React.memo(({
  isActive = false,
  onClick,
  layout = BurgerLayout.Left,
  className,
}) => (
  <button
    type='button'
    className={cn('Burger', `Burger-layout-${layout}`, { isActive }, className)}
    onClick={onClick}
  >
    <span className='Burger__lines' />
  </button>
))
