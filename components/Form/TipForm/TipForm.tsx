import React from 'react'
import { useSelector } from 'react-redux'
import { Formik, FormikConfig, FormikProps } from 'formik'
import { Currency, Theme } from '@models/commonTypes'
import { Form, FormProps } from '@components/Form/Form'
import { Input } from '@components/Input/Input'
import { Button, ButtonTheme } from '@components/Button/Button'
import { minTipSelector } from '@store/References/selectors'

export interface TipFormValues {
  sum: number
  message: string
}

export interface TipFormProps extends Partial<FormikProps<TipFormValues>> {
  formProps?: FormProps
  onSubmit: FormikConfig<TipFormValues>['onSubmit']
}

export const TipForm: React.FunctionComponent<TipFormProps> = React.memo(({
  formProps = {},
  initialValues: _initialValues,
  ...formikProps
}) => {
  const minTip = useSelector(minTipSelector)
  const initialValues = { ..._initialValues, sum: minTip } as TipFormValues

  return (
    <Formik enableReinitialize {...formikProps} initialValues={initialValues}>
      {({ isSubmitting }) => (
        <Form className='TipForm' {...formProps} theme={Theme.Light}>
          <Input type='money' currency={Currency.USD} min={minTip} name='sum' label='Amount' />
          <Input type='text' name='message' label='Message' />
          <div className='Form__footer'>
            <Button
              type='submit'
              disabled={isSubmitting}
              showSpinner={isSubmitting}
              theme={ButtonTheme.LIGHT}
            >
              send tips
            </Button>
          </div>
        </Form>
      )}
    </Formik>
  )
})

TipForm.defaultProps = {
  initialValues: { sum: 0, message: '' },
}
