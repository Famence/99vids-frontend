import React from 'react'
import { Formik, FormikErrors, FormikProps } from 'formik'
import { Theme } from '@models/commonTypes'
import { Form, FormProps } from '@components/Form/Form'
import { Input } from '@components/Input/Input'
import { SocialLogin, SocialLoginTheme, socialLoginThemes } from '@components/SocialLogin/SocialLogin'
import { Button, ButtonTheme } from '@components/Button/Button'
import { LinkWithA } from '@components/Link/LinkWithA'
import { emailValidation } from '@utils/validation/email'
import { passwordValidation } from '@utils/validation/password'
import { clearObject } from '@utils'
import '@components/Form/AuthForm.sass'
import './SignInForm.sass'

interface SignInFormValues {
  email: string
  password: string
}

export interface SignInFormProps extends Partial<FormikProps<SignInFormValues>> {
  formProps?: FormProps
  buttonTheme?: ButtonTheme
  socialLoginTheme?: SocialLoginTheme
  buttonText?: string
}

export const SignInForm: React.FunctionComponent<SignInFormProps> = React.memo(({
  formProps,
  socialLoginTheme = socialLoginThemes.LIGHT as unknown as SocialLoginTheme,
  buttonTheme = ButtonTheme.LIGHT,
  buttonText = 'Log In',
  ...formikProps
}) => (
  <Formik
    validate={({ email, password }): FormikErrors<SignInFormValues> => clearObject({
      email: emailValidation.findError(email),
      password: passwordValidation.findError(password),
    })}
    {...formikProps}
  >
    {({ isSubmitting }) => (
      <Form className='SignInForm AuthForm' theme={Theme.Light} {...formProps}>
        <div className='AuthForm__socialLogin'>
          <div className='AuthForm__socialLogin-title'>
            <span>login with</span>
          </div>
          <div className='AuthForm__socialLogin-holder'>
            <SocialLogin theme={socialLoginTheme} />
          </div>
        </div>
        <Input type='email' name='email' label='Email' />
        <Input type='password' name='password' label='Password' allowDisplayPassword />
        <div className='Form__footer'>
          <Button
            type='submit'
            disabled={isSubmitting}
            showSpinner={isSubmitting}
            theme={buttonTheme}
          >
            {buttonText}
          </Button>
          <LinkWithA route='restoreAccess' className='small'>Forgot your password?</LinkWithA>
        </div>
        <div className='small gray' style={{ marginTop: 43 }}>
          Don’t have an account? <LinkWithA route='signUp'>Sign up</LinkWithA>
        </div>
      </Form>
    )}
  </Formik>
))

SignInForm.defaultProps = {
  initialValues: { email: '', password: '' },
}
