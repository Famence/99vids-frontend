import React, { useCallback, useEffect } from 'react'
import { Formik, FormikErrors, FormikHelpers, FormikProps } from 'formik'
import { Form, FormProps } from '@components/Form/Form'
import { Input } from '@components/Input/Input'
import { SocialLogin } from '@components/SocialLogin/SocialLogin'
import { Button } from '@components/Button/Button'
import { emailValidation } from '@utils/validation/email'
import { passwordValidation } from '@utils/validation/password'
import { stringEqualityValidation } from '@utils/validation/string'
import { UserRoles } from '@models/Users'
import { LinkWithA } from '@components/Link/LinkWithA'
import { clearObject } from '@utils'
import '@components/Form/AuthForm.sass'
import './SignUpForm.sass'

interface SignUpFormValues {
  role: string
  email: string
  password: string
  confirmPassword: string
}

export interface SignUpFormProps extends Partial<FormikProps<SignUpFormValues>> {
  role: UserRoles
  onChangeRole: (newRole: UserRoles) => void
  formProps: FormProps
}

export const SignUpForm: React.FunctionComponent<SignUpFormProps> = React.memo(({
  role,
  onChangeRole,
  formProps,
  ...formikProps
}) => (
  <Formik
    validate={({ email, password, confirmPassword }): FormikErrors<SignUpFormValues> => clearObject({
      email: emailValidation.findError(email),
      password: passwordValidation.findError(password),
      confirmPassword: !stringEqualityValidation.validate(password, confirmPassword) ? 'Passwords doesn\'t match' : undefined,
    })}
    {...formikProps}
  >
    {({ isSubmitting, setFieldValue }) => (
      <Form className='SignUpForm AuthForm' {...formProps}>
        <SelectRole selectedRole={role} onChangeRole={onChangeRole} setFieldValue={setFieldValue} />
        {role && (
          <>
            <div className='AuthForm__socialLogin'>
              <div className='AuthForm__socialLogin-title'>
                <span>sign up with</span>
              </div>
              <div className='AuthForm__socialLogin-holder'>
                <SocialLogin />
              </div>
            </div>
            <Input type='email' name='email' label='Email' />
            <Input type='password' name='password' label='Password' allowDisplayPassword />
            <Input type='password' name='confirmPassword' label='Repeat password' allowDisplayPassword />
            <div className='Form__footer'>
              <Button
                type='submit'
                disabled={isSubmitting}
                showSpinner={isSubmitting}
              >
                Register
              </Button>
              <LinkWithA route='restoreAccess' className='SignUpForm__forgotPassword small'>Forgot your password?</LinkWithA>
            </div>
            <div className='small gray' style={{ marginTop: 43 }}>
              I have an account. <LinkWithA route='logIn'>Log in</LinkWithA>
            </div>
          </>
        )}
      </Form>
    )}
  </Formik>
))

SignUpForm.defaultProps = {
  initialValues: { email: '', password: '', confirmPassword: '', role: '' },
}

interface SelectRoleProps {
  selectedRole: UserRoles
  onChangeRole: (newRole: UserRoles) => void
  setFieldValue: FormikHelpers<SignUpFormValues>['setFieldValue']
}

const SelectRole: React.FunctionComponent<SelectRoleProps> = React.memo(({
  selectedRole, onChangeRole, setFieldValue
}) => {
  const onChange = useCallback((event) => onChangeRole(event.target.value), [])

  useEffect(() => {
    setFieldValue('role', selectedRole)
  }, [setFieldValue, selectedRole])

  return (
    <>
      <h2 className='SelectRole__title'>Choose your role</h2>
      <div className='SelectRole'>
        <div className="SelectRole__item">
          <label className="radio">
            <img src='/assets/img/authorization/model.svg' alt='model' className='SelectRole__roleImage' />
            <input type="radio" name="signup" value={UserRoles.MODEL} onChange={onChange} checked={selectedRole === UserRoles.MODEL} />
            <div className='radio__label'>
              <div className="radio__text">FOR MODELS</div>
              <div className="radio__sub">I want to earn money.</div>
            </div>
          </label>
        </div>
        <div className="SelectRole__item">
          <label className="radio">
            <img src='/assets/img/authorization/fan.svg' alt='fan' className='SelectRole__roleImage' />
            <input type="radio" name="signup" value={UserRoles.FAN} onChange={onChange} checked={selectedRole === UserRoles.FAN} />
            <div className='radio__label'>
              <div className="radio__text">FOR FAN</div>
              <div className="radio__sub">I want to follow models.</div>
            </div>
          </label>
        </div>
      </div>
    </>
  )
})
