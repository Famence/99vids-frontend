import React from 'react'
import cn from 'classnames'
import { Form as FormikForm, FormikFormProps } from 'formik'
import { Theme } from '@models/commonTypes'
import './Form.sass'

export interface FormProps extends FormikFormProps {
  theme?: Theme
  className?: string
}

export const Form: React.FunctionComponent<FormProps> = React.memo(({
  className,
  theme,
  ...props
}) => (
  <FormikForm
    className={cn( 'Form', className, { [`theme-${theme}`]: theme })}
    {...props}
  />
))
