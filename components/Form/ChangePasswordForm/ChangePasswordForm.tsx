import React from 'react'
import { Formik, FormikErrors, FormikProps } from 'formik'
import { Form, FormProps } from '@components/Form/Form'
import { Input } from '@components/Input/Input'
import { Button } from '@components/Button/Button'
import { passwordValidation } from '@utils/validation/password'
import { stringEqualityValidation } from '@utils/validation/string'
import { clearObject } from '@utils'
import './ChangePasswordForm.sass'

interface ChangePasswordFormValues {
  oldPassword: string
  newPassword: string
  checkPassword: string
}

export interface ChangePasswordFormProps extends Partial<FormikProps<ChangePasswordFormValues>> {
  formProps?: FormProps
}

export const ChangePasswordForm: React.FunctionComponent<ChangePasswordFormProps> = React.memo(({
  formProps,
  ...formikProps
}) => (
  <Formik
    validate={({ newPassword, checkPassword }): FormikErrors<ChangePasswordFormValues> => clearObject({
      newPassword: passwordValidation.findError(newPassword),
      checkPassword: !stringEqualityValidation.validate(newPassword, checkPassword) ? 'Passwords doesn\'t match' : undefined,
    })}
    {...formikProps}
  >
    {({ isSubmitting }) => (
      <Form className='ChangePasswordForm' {...formProps}>
        <Input type='password' name='oldPassword' label='Old password' allowDisplayPassword />
        <Input type='password' name='newPassword' label='New password' allowDisplayPassword />
        <Input type='password' name='checkPassword' label='Confirm new password' allowDisplayPassword />
        <div className='Form__footer'>
          <Button
            type='submit'
            disabled={isSubmitting}
            showSpinner={isSubmitting}
          >
            Save
          </Button>
        </div>
      </Form>
    )}
  </Formik>
))

ChangePasswordForm.defaultProps = {
  initialValues: { oldPassword: '', newPassword: '', checkPassword: '' },
}
