import React from 'react'
import { Formik, FormikErrors, FormikProps } from 'formik'
import { Form, FormProps } from '@components/Form/Form'
import { Input } from '@components/Input/Input'
import { Button, ButtonTheme } from '@components/Button/Button'
import { emailValidation } from '@utils/validation/email'
import { clearObject } from '@utils'
import '@components/Form/AuthForm.sass'
import './RestoreAccessForm.sass'

interface RestoreAccessFormValues {
  email: string
}

export interface RestoreAccessFormProps extends Partial<FormikProps<RestoreAccessFormValues>> {
  formProps?: FormProps
  buttonTheme?: ButtonTheme
}

export const RestoreAccessForm: React.FunctionComponent<RestoreAccessFormProps> = React.memo(({
  formProps,
  buttonTheme = ButtonTheme.PURPLE,
  ...formikProps
}) => (
  <Formik
    validate={({ email }): FormikErrors<RestoreAccessFormValues> => clearObject({
      email: emailValidation.findError(email),
    })}
    {...formikProps}
  >
    {({ isSubmitting }) => (
      <Form className='RestoreAccessForm AuthForm' {...formProps}>
        <Input type='email' name='email' label='Email' />
        <div className='Form__footer'>
          <Button
            type='submit'
            disabled={isSubmitting}
            showSpinner={isSubmitting}
            theme={buttonTheme}
          >
            Reset password
          </Button>
        </div>
      </Form>
    )}
  </Formik>
))

RestoreAccessForm.defaultProps = {
  initialValues: { email: '' },
}
