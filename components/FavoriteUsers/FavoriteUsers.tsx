import React, { useCallback } from 'react'
import { useSelector } from 'react-redux'
import { favoriteUsersCountSelector, favoriteUsersSelector } from '@store/Favorites/selectors'
import { useLoaderSelector } from '@store/Loaders/selectors'
import { LoaderTypes } from '@store/Loaders/constants'
import { UsersGrid, UsersGridItemControlsType } from '@components/UsersGrid/UsersGrid'
import { useStore } from '@utils'
import { PostsFeed } from '@components/PostsFeed/PostsFeed'
import { PostDummy } from '@components/PostDummy/PostDummy'

export const FavoriteUsers: React.FunctionComponent = React.memo(() => {
  const store = useStore()
  const favoriteUsers = useSelector(favoriteUsersSelector)
  const favoriteUsersCount = useSelector(favoriteUsersCountSelector)
  const isLoading = useLoaderSelector(LoaderTypes.FavoritesUsersLoader)

  const onCanLoadMore = useCallback(async () => {
    if (typeof favoriteUsersCount === 'number' && favoriteUsersCount <= favoriteUsers.length) return
    await store.dispatch.favorites.getFavoriteUsers({ pagination: { offset: favoriteUsers.length } })
  }, [favoriteUsers.length, favoriteUsersCount])

  return (
    <>
      <UsersGrid
        users={favoriteUsers}
        isLoading={isLoading}
        withSubscription
        withPostsStatistics
        onCanLoadMore={onCanLoadMore}
        itemControlType={UsersGridItemControlsType.Favorite}
      />
      {!favoriteUsers.length && !isLoading && <PostDummy text='There are no favorite persons yet' />}
    </>
  )
})
