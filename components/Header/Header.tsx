import React, { useCallback } from 'react'
import { Logo } from '@components/Logo/Logo'
import { LinkWithA } from '@components/Link/LinkWithA'
import { Burger } from '@components/Burger/Burger'
import { SearchBar } from '@components/SearchBar/SearchBar'
import { CurrentUserControls } from '@components/CurrentUserControls/CurrentUserControls'
import './Header.sass'

interface HeaderProps {
  isSidebarOpened: boolean
  setIsSidebarOpened: React.Dispatch<React.SetStateAction<boolean>>
}

export const Header: React.FunctionComponent<HeaderProps> = ({
  isSidebarOpened = false,
  setIsSidebarOpened,
}) => {
  const toggleSidebar = useCallback(() => {
    setIsSidebarOpened(!isSidebarOpened)
  }, [isSidebarOpened, setIsSidebarOpened])

  return (
    <header className='Header'>
      <div className='HeaderLogoWrapper'>
        <LinkWithA route='index' className='HeaderLogo'>
          <Logo />
        </LinkWithA>
      </div>
      <Burger isActive={isSidebarOpened} onClick={toggleSidebar} className='HeaderSidebarToggler' />
      <SearchBar />
      <CurrentUserControls />
    </header>
  )
}
