import React from 'react'
import Slider from 'react-rangeslider'
import IconPic from '@icons/pic.svg'
import 'react-rangeslider/lib/index.css'
import './RangeScale.scss'

export const RangeScale: React.FunctionComponent = React.memo((props) => (
  <div className='range-scale'>
    <div className="range-scale__icon range-scale__icon--sm">
      <IconPic />
    </div>

    <div className="range-scale__holder">
      <Slider {...props} />
    </div>

    <div className="range-scale__icon range-scale__icon--md">
      <IconPic />
    </div>
  </div>
))
