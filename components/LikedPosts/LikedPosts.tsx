import React, { useCallback } from 'react'
import { useSelector } from 'react-redux'
import { likedPostsCountSelector, likedPostsSelector } from '@store/Favorites/selectors'
import { useLoaderSelector } from '@store/Loaders/selectors'
import { LoaderTypes } from '@store/Loaders/constants'
import { useStore } from '@utils'
import { PostsFeed } from '@components/PostsFeed/PostsFeed'
import { PostDummy } from '@components/PostDummy/PostDummy'

export const LikedPosts: React.FunctionComponent = React.memo(() => {
  const store = useStore()
  const likedPosts = useSelector(likedPostsSelector)
  const likedPostsCount = useSelector(likedPostsCountSelector)
  const isLoading = useLoaderSelector(LoaderTypes.LikedPostsLoader)

  const onCanLoadMore = useCallback(async () => {
    if (typeof likedPostsCount === 'number' && likedPostsCount <= likedPosts.length) return
    await store.dispatch.favorites.getLikedPosts({ pagination: { offset: likedPosts.length } })
  }, [likedPosts.length, likedPostsCount])

  return (
    <>
      <PostsFeed
        posts={likedPosts}
        isLoading={isLoading}
        onCanLoadMore={onCanLoadMore}
      />
      {!likedPosts.length && !isLoading && <PostDummy text='You have no liked posts yet' />}
    </>
  )
})
