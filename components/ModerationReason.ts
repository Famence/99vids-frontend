import React, { useEffect, useMemo } from 'react'
import { useStore } from '@utils/store'
import { useSelector } from 'react-redux'
import { rejectReasonsSelector } from '@store/References/selectors'

const ModerationReason: React.FunctionComponent<{ reason: string }> = ({ reason }) => {
  const store = useStore()
  const rejectReasons = useSelector(rejectReasonsSelector)

  useEffect( () => {
    store.dispatch.references.getRejectReasons()
  }, [])

  return useMemo(() => {
    if (rejectReasons instanceof Array) {
      const foundReason = rejectReasons.find(_reason => _reason._id === reason)
      if (!foundReason) return reason
      return foundReason.text
    }
    return reason
  }, [rejectReasons, reason])
}

export default ModerationReason
