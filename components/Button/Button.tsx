import React from 'react'
import cn from 'classnames'
import { Url } from '@models/commonTypes'
import { LinkWithA } from '@components/Link/LinkWithA'
import { RouteParams } from '@routes/config'
import './Button.sass'

export enum ButtonTheme {
  ORANGE = 'orange',
  PURPLE = 'purple',
  GREEN = 'green',
  RED = 'red',
  LIGHT = 'light',
}

export interface ButtonProps extends React.ButtonHTMLAttributes<any> {
  className?: string
  theme?: ButtonTheme
  small?: boolean
  large?: boolean
  outline?: boolean
  isTransparent?: boolean
  showSpinner?: boolean
  justLink?: boolean
  route?: string
  params?: RouteParams
  href?: Url
}

export const Button = ({
  className,
  theme = ButtonTheme.PURPLE,
  small = false,
  large = false,
  outline = false,
  isTransparent = false,
  children,
  showSpinner,
  justLink,
  route,
  params,
  href,
  onClick,
  ...props
}: React.PropsWithChildren<ButtonProps>) => {
  const size = small ? 'small' : large ? 'large' : null

  const classes = !justLink ? cn(
    'Button',
    className,
    {
      [`Button--${size}`]: size,
      [`Button--theme-${theme}`]: theme,
      'Button--outline': outline,
      'Button--transparent': isTransparent,
      'Button--spinning': showSpinner,
    }
  ) : className

  const content = (
    <>
      {children}
      {showSpinner && (
        <div className='Button__spinner'>
          <span />
          <span />
          <span />
        </div>
      )}
    </>
  )

  if (route) {
    return (
      <LinkWithA
        route={route}
        params={params}
        className={classes}
        {...props}
      >
        {content}
      </LinkWithA>
    )
  }
  if (href) {
    return (
      <a
        href={href as string}
        className={classes}
        {...props}
      >
        {content}
      </a>
    )
  }
  if (justLink) {
    return (
      <a
        onClick={onClick}
        className={classes}
        {...props}
      >
        {content}
      </a>
    )
  }
  return (
    <button
      type='button'
      onClick={onClick}
      className={classes}
      {...props}
    >
      {content}
    </button>
  )
}
