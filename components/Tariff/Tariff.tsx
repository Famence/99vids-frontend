import React from 'react'
import cn from 'classnames'
import { TariffType } from '@models/TariffType'
import IconCrown from '@icons/tariff-crown.svg'
import { Button, ButtonTheme } from '@components/Button/Button'
import { SubscribedButtonPlug } from '@components/SubscribeButton/SubscribeButton'
import { getTariffMonths, price } from '@utils'
import './Tariff.scss'

interface TariffProps {
  tariff: TariffType
  onClickSelect: React.MouseEventHandler
  isSubscribed?: boolean
  isSubscribeMode?: boolean
  isLoading?: boolean
}

export const Tariff: React.FunctionComponent<TariffProps> = React.memo(({
  tariff,
  isSubscribed = false,
  isSubscribeMode = false,
  isLoading = false,
  onClickSelect,
}) => (
  <div className="Tariff">
    <header className="Tariff__header">
      {tariff._id === 'vip' && <IconCrown className='Tariff__decoration Tariff__decoration-crown' />}
      <div className="Tariff__header-title">{getTariffMonths(tariff._id)} month subscription</div>
      <div className="Tariff__header-price">
        <div className="Tariff__price">
          <span className='small'>$</span>
          <span>{price(tariff.priceRules[tariff._id] || 0)}</span>
        </div>
      </div>
    </header>

    <div className="Tariff__body">
      <div className={cn('Tariff__discount', { 'zero': !tariff.discount })}>
        {tariff.discount}% off
      </div>

      <div className="Tariff__button">
        {isSubscribed ? (
          <SubscribedButtonPlug theme={ButtonTheme.PURPLE} />
        ) : (
          <Button
            showSpinner={isLoading}
            onClick={onClickSelect}
            theme={ButtonTheme.LIGHT}
            outline
          >
            {isSubscribeMode ? 'Get this' : 'Upgrade'}
          </Button>
        )}
      </div>
    </div>
  </div>
))
