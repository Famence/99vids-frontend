import React from 'react'
import cn from 'classnames'
import { User } from '@models/Users'
import { Avatar } from '@components/Avatar/Avatar'
import './UserIconWithName.sass'

interface UserIconWithNameProps {
  user: User
  className?: string
}

export const UserIconWithName: React.FunctionComponent<UserIconWithNameProps> = React.memo(({
  user,
  className,
}) => (
  <div className={cn('UserIconWithName', className)}>
    <Avatar user={user} />
    <span className='UserIconWithName-name'>{user.firstname}</span><br />
    <span className='UserIconWithName-nickname'>@{user.nickname}</span>
  </div>
))
