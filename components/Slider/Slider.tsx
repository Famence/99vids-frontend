import SlickSlider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import 'react-magic-slider-dots/dist/magic-dots.css'
import './Slider.sass'

export const Slider = SlickSlider
