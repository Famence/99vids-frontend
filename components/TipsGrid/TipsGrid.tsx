import React, { useRef } from 'react'
import { useSelector } from 'react-redux'
import cn from 'classnames'
import moment from 'moment'
import { Loader } from '@components/Loader/Loader'
import { OnCanLoadMoreCallback, price, useCanLoadMore } from '@utils'
import { Img } from '@components/Img/Img'
import { Payment } from '@models/Payment'
import { usersSelector } from '@store/Users/selectors'
import { ChatButton } from '@components/Chat/ChatButton/ChatButton'
import IconEnvelope from '@icons/send.svg'
import IconTips from '@icons/tips.svg'
import './TipsGrid.sass'

const useCanLoadMoreOptions = { numberOfScreens: .5 }

interface TipsGridProps {
  tips: Payment[]
  isLoading?: boolean
  onCanLoadMore?: OnCanLoadMoreCallback
  className?: string
}

export const TipsGrid: React.FunctionComponent<TipsGridProps> = React.memo(({
  tips,
  isLoading = false,
  onCanLoadMore,
  className,
}) => {
  const wrapperRef = useRef(null)
  const { isLoadingMore } = useCanLoadMore(onCanLoadMore, wrapperRef, useCanLoadMoreOptions)
  const users = useSelector(usersSelector)

  return (
    <div className={cn('TipsGrid', className)} ref={wrapperRef}>
      <Loader loaded={!isLoading || isLoadingMore}>
        <ul className='TipsGrid__list'>
          {tips.map((tip) => (
            <li className='TipsGrid__list-item' key={tip._id}>
              <Img src={users[tip.recipient].backgroundPhoto.paths} className='TipsGrid__list-item-bg' />
              <div className='TipsGrid__list-item-top'>
                <div className='TipsGrid__list-item-tipSum'>
                  <IconTips isFilled isInline />
                  ${price(tip.amount)}
                </div>
                <div className='TipsGrid__list-item-tipDate'>
                  {moment(tip.createdAt).format('DD MMMM')}
                </div>
              </div>
              <div className='TipsGrid__list-item-bottom'>
                {users[tip.recipient].displayName}
                <ChatButton targetUser={users[tip.recipient]} small>
                  <IconEnvelope isFilled />
                </ChatButton>
              </div>
            </li>
          ))}
        </ul>
        <Loader loaded={!isLoadingMore} />
      </Loader>
    </div>
  )
})
