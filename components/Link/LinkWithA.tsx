import React from 'react'
import { Link, LinkProps } from '@routes/config'

interface LinkWithAProps extends LinkProps {
  className?: string
}

export const LinkWithA: React.FunctionComponent<LinkWithAProps> = React.memo(({
  children,
  className,
  ...props
}) => (
  <Link {...props}>
    <a className={className}>{children}</a>
  </Link>
))
