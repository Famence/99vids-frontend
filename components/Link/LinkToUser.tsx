import React from 'react'
import { Url } from '@models/commonTypes'
import { User } from '@models/Users'
import { LinkWithA } from '@components/Link/LinkWithA'

interface LinkToUserProps {
  user: User | User['_id']
  href?: Url
}

export const LinkToUser: React.FunctionComponent<LinkToUserProps> = React.memo(({ user, href, ...props }) => (
  <LinkWithA
    route={href?.toString() || 'userPage'}
    params={{ userId: typeof user === 'string' ? user : (user.nickname || user._id) }}
    {...props}
  />
))
