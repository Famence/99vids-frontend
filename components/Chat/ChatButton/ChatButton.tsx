import React, { useCallback } from 'react'
import cn from 'classnames'
import { User } from '@models/Users'
import { Button, ButtonProps } from '@components/Button/Button'
// import { useStore } from '@utils'
import './ChatButton.sass'

interface ChatButtonProps extends ButtonProps {
  targetUser: User
  className?: string
}

export const ChatButton: React.FunctionComponent<ChatButtonProps> = React.memo(({
  targetUser,
  className,
  ...props
}) => {
  // const store = useStore()
  const onClick = useCallback(() => {
    // store.dispatch.chat.open({ targetUser })
  }, [targetUser])

  return (
    <Button onClick={onClick} className={cn('ChatButton', className)} {...props} />
  )
})
