import React, { useCallback, useEffect, useMemo, useState } from 'react'
import cn from 'classnames'
import { User } from '@models/Users'
import { Loader, LoaderSize } from '@components/Loader/Loader'
import DummyAvatarFemale from '@icons/dummy-avatar-female.svg'
import DummyAvatarMale from '@icons/dummy-avatar-male.svg'
import { resolveApiUri } from '@utils/helpers'
import { PhotoPaths } from '@models/MediaFile'
import './Avatar.sass'

interface AvatarProps {
  user?: User
  src?: PhotoPaths
  showFull?: boolean
  className?: string
}

export const Avatar: React.FunctionComponent<AvatarProps> = React.memo(({
  className,
  src: _source,
  showFull = false,
  user,
  ...wrapperProps
}) => {
  const [loaded, setLoaded] = useState(true)
  const [error, setError] = useState(false)
  const [src, setSrc] = useState<string | undefined>()

  const source = useMemo(() => {
    let newSource
    if (_source)
      newSource = _source
    if (user) {
      newSource = (
        user.photoByUser instanceof Array && user.photoByUser[0] && user.photoByUser[0].paths
      ) || (user.photoByProvider && user.photoByProvider) || undefined
    }

    if (!newSource) return undefined

    if (typeof newSource === 'string')
      return newSource
    return resolveApiUri((showFull ? newSource.full : newSource.thumb) || newSource.tmp)
  }, [_source, user, showFull])

  const updateSrc = useCallback(() => {
    setLoaded(false)
    setError(false)
    setSrc(source)
  }, [source, showFull])

  useEffect(() => {
    updateSrc()
  }, [source])

  const [onImageLoaded, onImageLoadError] = useMemo(() => [
    () => setLoaded(true),
    () => setError(true),
  ], [])

  const imgStyle = useMemo(() => ({
    opacity: loaded ? 1 : 0,
  }), [loaded])

  return (
    <div className={cn('Avatar', className)} {...wrapperProps}>
      {source !== null || src ? (
        <img
          src={src}
          onLoad={onImageLoaded}
          onError={onImageLoadError}
          style={imgStyle}
          alt={`${user?.displayName}'s avatar`}
        />
      ) : (
        <Dummy user={user} />
      )}
      <Loader size={LoaderSize.SMALL} white loaded={source === null || loaded || error} />
    </div>
  )
})

interface DummyProps {
  user?: User
  useFemale?: boolean
}

const Dummy = React.memo(({ user, useFemale = false }: DummyProps) => {
  const isFemale = useFemale || (user && user.roleId === 'model')
  return (
    <div className={cn('Avatar__Dummy', { female: isFemale })}>
      {isFemale ? <DummyAvatarFemale /> : <DummyAvatarMale />}
    </div>
  )
})
