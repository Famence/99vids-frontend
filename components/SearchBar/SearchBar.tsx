import React, { useCallback, useRef, useState } from 'react'
import cn from 'classnames'
import IconSearch from '@icons/search.svg'
import './SearchBar.sass'

export const SearchBar: React.FunctionComponent = React.memo(() => {
  // const store = useStore()
  const inputRef = useRef(null)
  const [query, setQuery] = useState('')
  const [isFocused, setIsFocused] = useState(false)
  const [isOpened, setIsOpened] = useState(false)

  const onInputFocus = useCallback(() => setIsFocused(true), [])
  const onInputBlur = useCallback(() => setIsFocused(false), [])

  const onQueryChange = useCallback((event) => {
    // store.dispatch.search.updateQuery(event.target.value)
    setQuery(event.target.value)
  }, [])

  const toggleIsOpened = useCallback(() => setIsOpened((_isOpened) => !_isOpened), [])
  const clearQuery = useCallback(() => {
    // store.dispatch.search.clear()
    setQuery('')
    if (inputRef.current) inputRef.current.blur()
  }, [])

  return (
    <div className={cn('SearchBar', { isOpened, isFocused, hasQuery: query, isResultsOpened: isFocused && query })}>
      <div className='SearchBar__input'>
        <button type='button' className='SearchBar__openButton' onClick={toggleIsOpened}>
          <IconSearch isFilled />
        </button>
        <input
          type='search'
          inputMode='search'
          placeholder={!isFocused ? 'Search' : undefined}
          value={query}
          onChange={onQueryChange}
          onFocus={onInputFocus}
          onBlur={onInputBlur}
          ref={inputRef}
        />
        <button type='button' className='SearchBar__clearButton' onClick={clearQuery} />
      </div>
      <div className='SearchBar__results'>
        Search results
      </div>
    </div>
  )
})
