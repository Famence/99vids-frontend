import React, { useEffect, useMemo } from 'react'
import { useSelector } from 'react-redux'
import cn from 'classnames'
import { recommendedStateSelector, recommendedUsersSelector } from '@store/Recommended/selectors'
import { User } from '@models/Users'
import { Block } from '@components/Block/Block'
import { Button, ButtonTheme } from '@components/Button/Button'
import { UsersList, UsersListProps } from '@components/UsersList/UsersList'
import { useStore } from '@utils'
import { useLoaderSelector } from '@store/Loaders/selectors'
import { LoaderTypes } from '@store/Loaders/constants'
import { Loader } from '@components/Loader/Loader'
import './RecommendationsBlock.sass'

interface RecommendationsBlockProps extends Omit<UsersListProps, 'users'> {
  itemsNumber?: number
  title?: string
  excludeUserIds?: User['_id'][]
  className?: string
}

export const RecommendationsBlock: React.FunctionComponent<RecommendationsBlockProps> = React.memo(({
  itemsNumber = 4,
  title,
  excludeUserIds,
  className,
  ...usersListProps
}) => {
  const store = useStore()
  const recommendedUsers = useSelector(recommendedUsersSelector)
  const { isNoMore } = useSelector(recommendedStateSelector)
  const isLoading = useLoaderSelector(LoaderTypes.RecommendedLoader)

  const recommendedUsersToDisplay = useMemo(() => {
    const usersToInclude = excludeUserIds?.length ? (
      recommendedUsers.filter((user) => !excludeUserIds.includes(user._id))
    ) : recommendedUsers

    return usersToInclude.slice(0, itemsNumber)
  }, [recommendedUsers, excludeUserIds, itemsNumber])

  useEffect(() => {
    if (!recommendedUsers.length && !isNoMore && !isLoading)
      store.dispatch.recommended.getRecommended()
  }, [])

  return (
    <Block
      title={title || 'Recommended'}
      className={cn('RecommendationsBlock', className)}
    >
      <Loader loaded={!isLoading}>
        <UsersList users={recommendedUsersToDisplay} {...usersListProps} />
      </Loader>
      <Button theme={ButtonTheme.LIGHT} route='recommended'>view more</Button>
    </Block>
  )
})
