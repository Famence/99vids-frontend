import React from 'react'
import cn from 'classnames'
import { Url } from '@models/commonTypes'
import { Button, ButtonTheme } from '@components/Button/Button'
import GoogleIcon from '@icons/google.svg'
import FacebookIcon from '@icons/facebook.svg'
import { resolveApiUri } from '@utils'
import { SvgIcon } from '*.svg'
import './SocialLogin.sass'

interface SocialLoginItem {
  id: string
  icon: SvgIcon
  text: string
  class: string
  href: Url
  buttonTheme: ButtonTheme
}

export const SOCIAL_LIST: SocialLoginItem[] = [
  {
    id: 'google',
    icon: GoogleIcon,
    text: 'google',
    class: 'icon-login-gp',
    href: resolveApiUri('/auth/google'),
    buttonTheme: ButtonTheme.LIGHT,
  },
  {
    id: 'facebook',
    icon: FacebookIcon,
    text: 'facebook',
    class: 'icon-login-fb',
    href: resolveApiUri('/auth/facebook'),
    buttonTheme: ButtonTheme.PURPLE,
  },
]

export enum SocialLoginThemeEnum {
  Colored = 'colored',
}

export const socialLoginThemes = { ...SocialLoginThemeEnum, ...ButtonTheme }
export type SocialLoginTheme = typeof socialLoginThemes

interface SocialLoginProps {
  list?: SocialLoginItem[]
  theme?: SocialLoginTheme
}

export const SocialLogin: React.FunctionComponent<SocialLoginProps> = React.memo(({
  list = SOCIAL_LIST,
  theme = socialLoginThemes.Colored,
}) => (
  <div className={cn('SocialLogin', `theme-${theme}`)}>
    {list.map((item) => (
      <Button
        href={item.href}
        className={cn('SocialLogin__item', item.class)}
        key={item.id}
        theme={(theme === socialLoginThemes.Colored ? item.buttonTheme : theme) as ButtonTheme}
      >
        <span className='SocialLogin__item-icon'><item.icon isFilled /></span>
        <span>{item.text}</span>
      </Button>
    ))}
  </div>
))
