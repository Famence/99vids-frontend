import React, { Component } from 'react'
import AvatarEditor from 'react-avatar-editor'
import { dataURLToBlob } from 'blob-util'
import { RangeScale } from '@components/RangeScale/RangeScale'
import { Loader } from '@components/Loader/Loader'
import './Editor.scss'

export class Editor extends Component {
  constructor(props) {
    super(props)

    this.state = {
      scale: this.props.initScale || 1,
      loaded: false,
    }

    this.getImage = this.getImage.bind(this)
    this.handleScaleRange = this.handleScaleRange.bind(this)
  }

  getImage() {
    const canvas = this.editor.getImage()
    const imgUrl = canvas.toDataURL()

    return {
      file: dataURLToBlob(imgUrl),
      filePreview: imgUrl,
      fileName: this.props.img.name || 'noName',
    }
  }

  handleScaleRange(value) {
    this.setState({
      scale: +value.toFixed(1),
    })
  }

  render() {

    return (
      <div className='editor'>
        <div className="editor__holder">
          <Loader loaded={this.state.loaded} />
          <AvatarEditor
            ref={editor => this.editor = editor}
            onImageReady={() => this.setState({ loaded: true })}
            image={this.props.img}
            width={this.props.width - this.props.border[0] - this.props.border[2]}
            height={this.props.height - this.props.border[1] - this.props.border[3]}
            border={this.props.border}
            color={[0, 0, 0, 0.4]}
            scale={this.state.scale}
            rotate={0}
            borderRadius={this.props.borderRadius}
          />
        </div>

        <div className="popup-ava__range">
          <RangeScale className='range'
                      min={1}
                      max={this.props.maxRange || 5}
                      step={0.1}
                      tooltip={false}
                      value={this.state.scale}
                      onChange={this.handleScaleRange} />
        </div>
      </div>
    )
  }
}
