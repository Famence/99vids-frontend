import React, { useCallback, useMemo } from 'react'
import { useSelector } from 'react-redux'
import { Formik, FormikErrors } from 'formik'
import { UserName } from '@components/EditProfile/UserName'
import { Form } from '@components/Form/Form'
import { Button } from '@components/Button/Button'
import { currentUserSelector } from '@store/Users/selectors'
import { clearObject, useStore } from '@utils'
import { User } from '@models/Users'
import { stringValidation } from '@utils/validation/string'
import { UploadAvatar } from '@components/EditProfile/UploadAvatar/UploadAvatar'

type GeneralModelFormValues = Pick<User, 'firstname' | 'nickname'>

export const GeneralModel: React.FunctionComponent = React.memo(() => {
  const store = useStore()
  const currentUser = useSelector(currentUserSelector)

  const initialValues = useMemo(() => ({
    firstname: currentUser?.firstname,
    nickname: currentUser?.nickname,
  }), [currentUser])

  const onSubmit = useCallback(async (values, { setSubmitting }) => {
    await store.dispatch.users.updateProfile(values)
    setSubmitting(false)
  }, [])

  return currentUser && (
    <>
      <div className="profile-edit__top">General</div>
      <Formik
        validate={({ firstname, nickname }): FormikErrors<GeneralModelFormValues> => clearObject({
          firstname: stringValidation.findError(firstname),
          nickname: stringValidation.findError(nickname) || (!nickname?.startsWith('@') && 'Nickname should starts with "@"')
        })}
        initialValues={initialValues}
        onSubmit={onSubmit}
      >
        {({ isSubmitting }) => (
          <Form>
            <div className="profile-edit__row">
              <div className="profile-edit__right">
                <UploadAvatar model={currentUser} />
              </div>
              <div className="profile-edit__left">
                <UserName />
                {/*<div className="profile-edit__top">*/}
                {/*  {isApprovedModel ?  'Your account is approved' : 'Upload your ID'}*/}
                {/*</div>*/}
                {/*{!isApprovedModel && (*/}
                {/*  <UploadId*/}
                {/*    onChange={this.onIdChange}*/}
                {/*    init={this.state.idPhoto}*/}
                {/*  />*/}
                {/*)}*/}
              </div>
            </div>

            <div className="profile-edit__block-bottom flex flex-align-center">
              <Button
                type='submit'
                disabled={isSubmitting}
                showSpinner={isSubmitting}
              >
                Save
              </Button>

              {/*{moderationStatus === MODERATION_STATUS.consideration && (*/}
              {/*  <div className='color-warning'>*/}
              {/*    You ID sent to moderation. Please wait for confirmation.*/}
              {/*  </div>*/}
              {/*)}*/}
            </div>
          </Form>
        )}
      </Formik>

      {/*<ProfileBottom onLogOutClick={() => this.props.userActions.logOut()}*/}
      {/*               onDeleteClick={() => this.confirm.showModal()}/>*/}

      {/*<ConfirmModal onRef={ref => this.confirm = ref}*/}
      {/*              className='delete-account-modal'*/}
      {/*              title='Are you sure you want to DELETE your account?'*/}
      {/*              onAccept={() => this.props.userActions.deleteProfile()}*/}
      {/*              btns={['cancel', 'delete']}/>*/}
    </>
  )
})
