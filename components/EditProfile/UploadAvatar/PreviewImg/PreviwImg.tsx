import React from 'react'
import { PhotoPaths } from '@models/MediaFile'
import { Avatar } from '@components/Avatar/Avatar'
import '../UploadAvatar.scss'

interface PreviewImgProps {
  photoPreview: PhotoPaths
  onClick?: React.MouseEventHandler
}

export const PreviewImg: React.FunctionComponent<PreviewImgProps> = React.memo(({
  onClick,
  photoPreview
}) => (
  <div className="ava-upload__label filled">
    {onClick && (
      <div
        className='UploadProfilePhoto__hover'
        style={{ position: 'relative', zIndex: 5 }}
        onClick={onClick}
      >
        <span>Upload avatar</span>
      </div>
    )}
    <Avatar src={photoPreview || null} />
  </div>
))
