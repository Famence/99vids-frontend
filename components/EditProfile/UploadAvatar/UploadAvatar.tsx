import React, { useCallback, useMemo, useRef } from 'react'
import cn from 'classnames'
import { User } from '@models/Users'
import { ModerationReason } from '@components/ModerationReason/ModerationReason'
import { PreviewImg } from './PreviewImg/PreviwImg'
import { PreviewDropZone } from './PreviewDropZone/PreviewDropZone'
import IconDanger from '@icons/danger.svg'
import { useStore } from '@utils'

interface UploadModelPhotoProps {
  model: User
  renderNudeNotice?: boolean
}

export const UploadAvatar: React.FunctionComponent<UploadModelPhotoProps> = React.memo(({
  model,
  renderNudeNotice = true,
}) => {
  const store = useStore()
  const dropZoneRef = useRef()

  const moderatedAvatar = useMemo(() => (model.photoByUser[0].paths || { full: model.photoByProvider }), [model])
  const unmoderatedAvatar = useMemo(() => (model.unmoderatedPhoto?.paths), [model])
  const avatarRejectionReason = useMemo(() => model.moderation?.photo.reason, [model])

  const startUploadAvatar = useCallback(() => store.dispatch.users.uploadAvatar(), [])

  return (
    <div className='profile-edit__inputs-block UploadProfilePhoto'>
      <div className={cn('ava-upload' /*, { error: hasError } */)}>
        <div className='ava-upload__wrapper'>
          {unmoderatedAvatar || moderatedAvatar ? (
            <div>
              <PreviewImg photoPreview={unmoderatedAvatar || moderatedAvatar} onClick={startUploadAvatar} />
              {unmoderatedAvatar ? (
                avatarRejectionReason ? (
                  <div className='ava-upload__tooltip red'>
                    <b>This avatar didn't pass the control</b><br /><br />
                    <ModerationReason reason={avatarRejectionReason} /><br />
                    Please choose another one WITHOUT NUDITY
                  </div>
                ) : (
                  <div className='ava-upload__tooltip green'>
                    <b>Your avatar is on moderation</b><br /><br />
                    It will be published automatically as soon as it pass the nudity control
                  </div>
                )
              ) : (
                renderNudeNotice && <div className='ava-upload__tooltip'>Avatar should be NON NUDE</div>
              )}
            </div>
          ) : (
            <div>
              <PreviewDropZone onClick={startUploadAvatar} dropZoneRef={dropZoneRef} />
              {renderNudeNotice && <div className='ava-upload__tooltip'>Avatar should be NON NUDE</div>}
            </div>
          )}
          {moderatedAvatar && unmoderatedAvatar && (
            <div className='ava-upload__currentAvatar'>
              <PreviewImg photoPreview={moderatedAvatar} />
              <div className='ava-upload__tooltip'>Old avatar</div>
            </div>
          )}
        </div>
        <div className="input__error">
          <IconDanger isFilled />
          Please fill in the field
        </div>
      </div>
    </div>
  )
})
