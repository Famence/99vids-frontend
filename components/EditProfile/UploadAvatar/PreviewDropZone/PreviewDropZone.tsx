import React from 'react'
import IconCloud from '@icons/cloud.svg'

interface PreviewDropZoneProps {
  dropZoneRef: React.RefObject<any>
  onClick: React.MouseEventHandler
}

export const PreviewDropZone: React.FunctionComponent<PreviewDropZoneProps> = React.memo(({
  dropZoneRef,
  onClick,
}) => (
  <div
    className='ava-upload__label'
    onClick={onClick}
    ref={dropZoneRef}
  >
    <div className="ava-upload__label-inner">
      <IconCloud isFilled />
      <span>Upload your Avatar photo</span>
    </div>
  </div>
))
