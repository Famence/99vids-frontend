import React, { useCallback, useMemo } from 'react'
import { useRouter } from 'next/router'
import Head from 'next/head'
import { Block } from '@components/Block/Block'
import { Tabs } from '@components/Tabs/Tabs'
import Routes from '@routes/config'
import { EditProfilePageTabs } from '@components/EditProfile/constants'
import { GeneralModel } from '@components/EditProfile/General/GeneralModel'
import { ChangePassword } from '@components/EditProfile/ChangePassword/ChangePassword'
import './EditProfile.sass'

const pageTabs = [{
  value: EditProfilePageTabs.General,
  label: 'General',
  pageTitle: 'Edit',
  key: 'general',
  Component: GeneralModel,
}, {
  value: EditProfilePageTabs.ChangePassword,
  label: 'Change Password',
  pageTitle: 'Change Password',
  key: 'changePassword',
  Component: ChangePassword,
}]

export const EditProfileModel: React.FunctionComponent = React.memo(() => {
  const router = useRouter()
  const { tab } = router.query

  const currentTab = useMemo(() => pageTabs.find((pageTab) => pageTab.value === tab), [tab])

  return currentTab && (
    <Block bigTabs topContent={<EditProfileModelTabs />}>
      <Head>
        <title>{currentTab.pageTitle} — 99vids</title>
      </Head>
      <currentTab.Component />
    </Block>
  )
})

const EditProfileModelTabs: React.FunctionComponent = React.memo(() => {
  const router = useRouter()

  const active = useMemo(() => (
    pageTabs.find((tab) => tab.value === router.query.tab)
  ), [router.query.tab])

  const onChange = useCallback((tab) => {
    Routes.Router.pushRoute('editProfile', { tab: tab.value })
  }, [])

  return (
    <Tabs tabs={pageTabs} active={active} onChange={onChange} />
  )
})
