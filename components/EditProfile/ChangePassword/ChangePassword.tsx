import React, { useCallback } from 'react'
import { ChangePasswordForm } from '@components/Form/ChangePasswordForm/ChangePasswordForm'
import { useStore } from '@utils'

export const ChangePassword: React.FunctionComponent = React.memo(() => {
  const store = useStore()

  const onSubmit = useCallback(async (values, { setSubmitting }) => {
    await store.dispatch.users.changePassword(values)
    setSubmitting(false)
  }, []);

  return (
    <div className='row'>
      <div className='col md-4'>
        <ChangePasswordForm onSubmit={onSubmit} />
      </div>
    </div>
  )
})
