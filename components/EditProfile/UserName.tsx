import React, { useCallback } from 'react'
import cn from 'classnames'
import Notice from '@components/Notice/Notice'
import { Input } from '@components/Input/Input'

interface UserNameProps {
  className?: string,
}

export const UserName: React.FunctionComponent<UserNameProps> = React.memo(({
  className
}) => {
  const formatNickname = useCallback((nickname) => (
    nickname.startsWith('@') ? nickname : `@${nickname}`
  ), [])

  return (
    <div className={cn('profile-edit__inputs-block', className)}>
      <div className="input-box">
        <Input
          label='Nickname*'
          name='nickname'
          formatValue={formatNickname}
          required
        />
        <Notice status='gray'>Use your instagram or twitter nickname so people can easily find you</Notice>
      </div>
      <div className="input-box">
        <Input
          label='Name*'
          name='firstname'
          required
        />
      </div>
    </div>
  )
})
