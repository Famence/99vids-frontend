import React from 'react'
import { Url } from '@models/commonTypes'
import './PostDummy.sass'

interface PostDummyProps {
  imageUri?: Url
  text?: string
}

export const PostDummy: React.FunctionComponent<PostDummyProps> = React.memo(({
  children,
  imageUri = '/assets/img/post-dummy.png',
  text = 'There are no posts yet...',
}) => (
  <div className='PostDummy'>
    {children || (
      <>
        <img className='PostDummy__img' src={imageUri.toString()} alt='Image of no posts plug' />
        <div className='PostDummy__text'>{text}</div>
      </>
    )}
  </div>
))
