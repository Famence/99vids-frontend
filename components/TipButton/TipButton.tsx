import React, { useCallback } from 'react'
import cn from 'classnames'
import { User } from '@models/Users'
import { useStore } from '@root/utils'
import { PostType } from '@models/Posts'

interface TipButtonProps {
  model: User
  className?: string
  post?: PostType
}

export const TipButton: React.FunctionComponent<TipButtonProps> = React.memo(({
  model,
  post,
  className,
  ...props
}) => {
  const store = useStore()

  const onClick = useCallback(() => {
    store.dispatch.users.sendTip({ modelId: model._id, postId: post?._id })
  }, [model._id, post?._id])

  return <button type='button' className={cn('TipButton', className)} onClick={onClick} {...props} />
})
