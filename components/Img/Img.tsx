import React, { useCallback, useEffect, useState } from 'react'
import cn from 'classnames'
import { ImgSource } from '@models/commonTypes'
import { Loader } from '@components/Loader/Loader'
import { AspectRatio, useMediaSourceState } from '@root/utils'
import './Img.sass'

interface ImgProps {
  src: ImgSource
  aspectRatio?: AspectRatio
  isFull?: boolean
  forceLoader?: boolean
  whiteLoader?: boolean
  renderAfterLoad?: React.ReactNode
  className?: string
  onClick?: React.MouseEventHandler
}

export const Img: React.FunctionComponent<ImgProps> = React.memo(({
  src = null,
  aspectRatio,
  isFull,
  forceLoader,
  whiteLoader,
  renderAfterLoad,
  className,
  ...wrapperProps
}) => {
  const [source, setSource] = useMediaSourceState()
  const [isLoaded, setIsLoaded] = useState(false)
  const [isError, setIsError] = useState(false)

  const containerStyle: React.CSSProperties = {}
  const imageStyle: React.CSSProperties = {}

  if (aspectRatio) {
    const aspectsValues = aspectRatio.split(':')
    containerStyle.paddingTop = `${+aspectsValues[1] / +aspectsValues[0] * 100}%`
  }

  useEffect(() => {
    if (!src) {
      setSource(undefined)
      setIsError(true)
      setIsLoaded(true)
      return
    } else {
      setIsLoaded(false)
      setIsError(false)
    }
    if (typeof src === 'string') {
      setSource(src)
      return
    }
    setSource(isFull ? (src.full || src.tmp) : (src.thumb || src.tmp))
  }, [src, isFull])

  const onLoad = useCallback(() => setIsLoaded(true), [])
  const onError = useCallback(() => setIsError(true), [])

  const loaded = !forceLoader && isLoaded

  return (
    <div
      className={cn('Img', className)}
      style={containerStyle}
      {...wrapperProps}
    >
      <img
        src={source}
        alt={source}
        onLoad={onLoad}
        onError={onError}
        style={imageStyle}
        className={cn({ isLoaded: loaded && !isError })}
      />
      <Loader loaded={loaded || isError} white={whiteLoader}>
        {renderAfterLoad}
      </Loader>
    </div>
  )
})
