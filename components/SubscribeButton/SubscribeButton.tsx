import React, { useCallback } from 'react'
import cn from 'classnames'
import { User } from '@models/Users'
import { Button, ButtonProps, ButtonTheme } from '@components/Button/Button'
import IconCheck from '@icons/check.svg'
import { useIsSubscribed, useStore } from '@utils'
import './SubscribeButton.sass'

export enum SubscribeButtonMode {
  Keep = 'keep',
  Upgrade = 'upgrade',
  Unsubscribe = 'unsubscribe',
}

export interface SubscribeButtonProps extends ButtonProps {
  model: User
  mode?: SubscribeButtonMode
}

export const SubscribeButton: React.FunctionComponent<SubscribeButtonProps> = React.memo(({
  model,
  mode = SubscribeButtonMode.Upgrade,
  children,
  className,
  ...buttonProps
}) => {
  const store = useStore()
  const isSubscribed = useIsSubscribed(model)

  const onClick = useCallback(() => {
    switch (mode) {
      default:
      case SubscribeButtonMode.Keep:
        break
      case SubscribeButtonMode.Upgrade:
        store.dispatch.users.subscribe({ modelId: model._id })
        break
      case SubscribeButtonMode.Unsubscribe:
        store.dispatch.users.unsubscribe({ modelId: model._id })
        break
    }
  }, [model, mode])

  return mode === SubscribeButtonMode.Keep ? (
    <SubscribedButtonPlug />
  ) : (
    <Button
      className={cn('SubscribeButton', `SubscribeButton-mode-${mode}`, className)}
      {...buttonProps}
      onClick={onClick}
    >
      {children || (
        mode === SubscribeButtonMode.Upgrade ? (
          isSubscribed ? 'upgrade' : 'subscribe'
        ) : 'unsubscribe'
      )}
    </Button>
  )
})

export const SubscribedButtonPlug: React.FunctionComponent<ButtonProps> = React.memo((props) => (
  <Button className='SubscribedButtonPlug' isTransparent theme={ButtonTheme.LIGHT} {...props}>
    <IconCheck isFilled isInline />
    subscribed
  </Button>
))
