import React, { useCallback, useMemo } from 'react'
import { User } from '@models/Users'
import { PostsGroup } from '@models/Posts'
import { Block } from '@components/Block/Block'
import { PostsFeed } from '@components/PostsFeed/PostsFeed'
import { PostDummy } from '@components/PostDummy/PostDummy'
import { UserPostsFeedTabsProps, UserPostsFeedTabs } from '@components/PostsFeed/PostsFeedGroups/PostsFeedGroups'
import { useStore, useUserPosts } from '@utils'
import { useLoaderSelector } from '@store/Loaders/selectors'
import { LoaderTypes } from '@store/Loaders/constants'

interface UserFeedBlockProps {
  user: User
}

export const UserFeedBlock: React.FunctionComponent<UserFeedBlockProps> = React.memo(({
  user,
}) => {
  const store = useStore()
  const { group, totalPostsOfGroup, posts } = useUserPosts(user._id)
  const isLoading = useLoaderSelector(LoaderTypes.UserPostsLoader)

  const onGroupChange = useCallback<UserPostsFeedTabsProps['onGroupChange']>((newGroup) => {
    store.dispatch.posts.getUserPosts({ userId: user._id, group: newGroup })
  }, [user._id])

  const topContent = useMemo(() => (
    <UserPostsFeedTabs user={user} onGroupChange={onGroupChange} />
  ), [user, onGroupChange])

  const onCanLoadMore = useCallback(async () => {
    let offset = posts.length
    let withPromo = true

    if (group === PostsGroup.All) {
      if (posts.length) offset -= 1
      withPromo = false
    }

    if (offset >= totalPostsOfGroup) return

    await store.dispatch.posts.getUserPosts({
      userId: user._id,
      group,
      pagination: { offset },
      withPromo,
    })
  }, [group, posts.length, totalPostsOfGroup, user._id])

  return (
    <Block title='Feed' topContent={topContent} withTopBorder>
      <PostsFeed posts={posts} isLoading={isLoading} onCanLoadMore={onCanLoadMore} />
      {!posts.length && !isLoading && <PostDummy />}
    </Block>
  )
})
