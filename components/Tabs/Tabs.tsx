import React, { useCallback } from 'react'
import cn from 'classnames'
import './Tabs.sass'

export enum TabsTheme {
  Simple = 'simple',
}

export interface TabOption<T = any> {
  value: T
  label: React.ReactNode
  key?: string | number
}

export type TabChangeHandler<T = any> = (tab: TabOption<T>) => void

export interface TabsProps<T = any> {
  tabs: TabOption<T>[]
  active: TabOption | undefined
  onChange: TabChangeHandler<T>
  theme?: TabsTheme
  className?: string
}

export const Tabs: React.FunctionComponent<TabsProps> = React.memo(({
  tabs,
  active,
  onChange,
  theme = TabsTheme.Simple,
  className,
}) => {
  const handleTabClick = useCallback((tab) => () => {
    if (active === tab) return
    onChange(tab)
  }, [onChange, active])

  return (
    <ul className={cn('Tabs', `theme-${theme}`, className)}>
      {tabs.map((tab) => (
        <li className={cn('Tabs__item', { 'isActive': active === tab })} key={tab.key || tab.value}>
          <button type='button' onClick={handleTabClick(tab)}>
            {tab.label}
          </button>
        </li>
      ))}
    </ul>
  )
})
