import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import { ControlBar, ForwardControl, PlaybackRateMenuButton, Player, ReplayControl } from 'video-react'
import { Url } from '@models/commonTypes'
import { VideoPaths, VideoQuality } from '@models/MediaFile'
import { SelectQuality } from '@components/Video/SelectQuality/SelectQuality'
import {
  AspectRatio,
  checkIsIOS,
  ensureAbsoluteUrl,
  getVideoQualityByNetworkSpeed,
  isLess,
  NetworkSpeed,
} from '@root/utils'
import 'video-react/dist/video-react.css'
import './Video.sass'

let testNetworkSpeed = new NetworkSpeed()

const playbackRates = [16, 10, 8, 6, 4, 2, 1, .5, .1]

interface PlayerProps {
  poster?: Url
  aspectRatio?: AspectRatio
  muted?: boolean
  autoPlay?: boolean
}

export interface VideoProps extends PlayerProps {
  src: VideoPaths
  playing?: boolean
  withPlaybackRate?: boolean
  defaultPlaybackRate?: typeof playbackRates
  disablePlayingProp?: boolean
}

export const Video: React.FunctionComponent<VideoProps> = React.memo(({
  src,
  playing,
  withPlaybackRate = false,
  defaultPlaybackRate,
  disablePlayingProp = false,
  ...playerProps
}) => {
  const player = useRef()
  const lastPlaybackRate = useRef()
  const [quality, setQuality] = useState<VideoQuality>(VideoQuality.auto)
  const [canAutoPlay, setCanAutoPlay] = useState(true)

  const availableQualities = Object.keys(src) as VideoQuality[]

  const source = useMemo(() => ensureAbsoluteUrl(src[quality]), [src, quality])

  const detectQualityAndSetSource = useCallback(() => {
    // TODO: Вызывается для каждого видео. Нужно что-то с этим делать. Замерять и хранить скорость в сторе например.
    testNetworkSpeed.checkDownloadSpeed(
      `/assets/dl_test?ver=${Math.random()}`,
      1089782
    ).then(({ mbps }) => {
      setQuality(getVideoQualityByNetworkSpeed(availableQualities, mbps))
      player.current?.load()
    })
  }, [src])

  const onChangeQuality = useCallback((key: VideoQuality) => {
    lastPlaybackRate.current = player.current.playbackRate

    if (key === VideoQuality.auto) {
      detectQualityAndSetSource()
    } else {
      setQuality(key)
    }
  }, [src])

  useEffect(() => {
    if (!player.current) return
    const isIos = checkIsIOS()
    player.current.video.video.addEventListener('click', () => {
      setCanAutoPlay(false)
    })
    player.current.video.video.addEventListener('pause', () => {
      if (isIos) {
        player.current.video.video.setAttribute('controls', 'true')
      }
    })
    player.current.video.video.addEventListener('play', () => {
      setCanAutoPlay(true)

      if (isIos) {
        let timeout
        const removeControls = () => {
          player.current?.video.video.removeAttribute('controls')
          player.current?.video.video.removeEventListener('click', removeControls)
          clearTimeout(timeout)
        }
        player.current.video.video.addEventListener('click', removeControls)
        timeout = setTimeout(removeControls, 1000)
      }
    })

    player.current.video.video.addEventListener('loadstart', () => {
      if (withPlaybackRate && (defaultPlaybackRate || lastPlaybackRate.current) && player.current) {
        player.current.playbackRate = lastPlaybackRate.current || defaultPlaybackRate
      }
    })
  }, [])

  useEffect(() => {
    if (typeof playing === 'boolean' || !player.current) {
      return
    }
    if (playing) {
      if (canAutoPlay && !disablePlayingProp) {
        player.current?.play()
      }
    } else {
      player.current?.pause()
    }
  }, [playing])

  useEffect(() => {
    detectQualityAndSetSource()
  }, [src])

  useEffect(() => {
    const { currentTime } = player.current.getState()
    player.current?.load()
    player.current?.seek(currentTime)

    setTimeout(() => {
      player.current?.play()
    }, 200)
  }, [source])

  return (
    <Player
      playsInline={checkIsIOS()}
      ref={player}
      {...playerProps}
    >
      <source src={source?.toString()} />
      <ControlBar disableCompletely={checkIsIOS()}>
        {!isLess(400) && <ReplayControl seconds={10} order={2} />}
        {!isLess(400) && <ForwardControl seconds={10} order={3} />}
        <SelectQuality
          onChange={onChangeQuality}
          list={availableQualities}
          selected={quality}
          order={7}
        />
        {withPlaybackRate && <PlaybackRateMenuButton rates={playbackRates} />}
      </ControlBar>
    </Player>
  )
})
