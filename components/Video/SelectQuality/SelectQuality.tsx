import React, { useCallback, useState } from 'react'
import cn from 'classnames'
import { VideoQuality, VideoQualities } from '@models/MediaFile'
import GearIcon from '@icons/gear.svg'
import './SelectQuality.sass'

interface SelectQualityProps {
  list: VideoQualities
  selected: VideoQuality
  onChange: (selectedQuality: VideoQuality) => void
}

export const SelectQuality: React.FunctionComponent<SelectQualityProps> = React.memo(({
  list,
  selected,
  onChange,
}) => {
  const [isOpened, setIsOpened] = useState(false)

  const open = useCallback(() => setIsOpened(true), [])
  const close = useCallback(() => setIsOpened(false), [])
  const handleSelect = useCallback((qualityKey) => () => onChange(qualityKey), [])

  return (
    <div className={cn('SelectQuality', { isOpened })}>
      <div className='SelectQuality__layer' onClick={close} />
      <div className='SelectQuality__holder'>
        <div className='SelectQuality__title' onClick={isOpened ? close : open}>
          <GearIcon />
        </div>
        <div className='SelectQuality__drop'>
          {list.map((qualityKey) => {
            if (['full', 'tmp'].includes(qualityKey)) return null
            const qualityResolution = +qualityKey.replace('h', '')
            return (
              <div
                className={cn('SelectQuality__drop-item', { selected: selected === qualityKey })}
                onClick={handleSelect(qualityKey)}
                key={qualityKey}
              >
                <span>
                  {qualityResolution}p
                  {qualityResolution >= 720 && <sup>HD</sup>}
                </span>
              </div>
            )
          })}
          <div
            className={cn('SelectQuality__drop-item', { selected: selected === VideoQuality.auto })}
            onClick={handleSelect(VideoQuality.auto)}
          >
            Auto
          </div>
        </div>
      </div>
    </div>
  )
})
