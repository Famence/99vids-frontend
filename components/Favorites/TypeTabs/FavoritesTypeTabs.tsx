import React, { useCallback, useMemo } from 'react'
import { FavoritesType, favoritesTypeLabels } from '@models/commonTypes'
import { TabChangeHandler, TabOption, Tabs, TabsProps } from '@components/Tabs/Tabs'

const favoritesTypes = [FavoritesType.Favorite, FavoritesType.Liked, FavoritesType.Commented, FavoritesType.Tipped]

const tabsOptions = favoritesTypes.map<TabOption<FavoritesType>>((status) => ({
  value: status,
  label: favoritesTypeLabels[status],
}))

export interface FavoritesTypeTabsProps extends Omit<TabsProps<FavoritesType>, 'tabs' | 'active' | 'onChange'> {
  active: FavoritesType
  onChange: (newStatus: FavoritesType) => void
}

export const FavoritesTypeTabs: React.FunctionComponent<FavoritesTypeTabsProps> = React.memo(({
  active,
  onChange,
  ...tabsProps
}) => {
  const activeTab = useMemo(() => (
    tabsOptions.find((tab) => tab.value === active)
  ), [active])

  const onTabChange = useCallback<TabChangeHandler<FavoritesType>>((selectedTab) => (
    onChange(selectedTab.value)
  ), [onChange])

  return (
    <Tabs
      {...tabsProps}
      tabs={tabsOptions}
      active={activeTab}
      onChange={onTabChange}
      className='FavoritesTypeTabs'
    />
  )
})
