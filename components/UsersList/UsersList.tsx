import React from 'react'
import cn from 'classnames'
import { LinkToUser } from '@components/Link/LinkToUser'
import { Avatar } from '@components/Avatar/Avatar'
import { User } from '@models/Users'
import './UsersList.sass'

export enum UsersListLayout {
  Vertical = 'vertical',
  Horizontal = 'horizontal',
}

export interface UsersListProps {
  users: User[]
  layout?: UsersListLayout
}

export const UsersList: React.FunctionComponent<UsersListProps> = React.memo(({
  users,
  layout = UsersListLayout.Vertical,
}) => {

  // const spaceHolders = useMemo(() => (
  //   layout === RecommendationsBlockLayout.Horizontal && recommendedUsersToDisplay.length < itemsNumber ? (
  //     new Array(itemsNumber - recommendedUsersToDisplay.length)
  //       .fill(undefined)
  //       .map((value, index) => <li key={index} />)
  //   ) : null
  // ), [layout, recommendedUsersToDisplay, itemsNumber])

  return (
    <ul className={cn('UsersList', `layout-${layout}`)}>
      {users.map((user) => (
        <li
          className='UsersListItem'
          data-tooltip={layout === UsersListLayout.Horizontal ? user.displayName : undefined}
          key={user._id}
        >
          <UsersListItemContent user={user} layout={layout} />
        </li>
      ))}
      {/*{spaceHolders}*/}
    </ul>
  )
})

interface UsersListItemProps {
  user: User
  layout: UsersListLayout
}

const UsersListItemContent: React.FunctionComponent<UsersListItemProps> = React.memo(({
  user,
  layout,
}) => {
  switch (layout) {
    default:
    case UsersListLayout.Vertical:
      return (
        <LinkToUser user={user}>
          <Avatar user={user} />
          <div className='UsersListItem__container'>
            <span className='UsersListItem__name'>
              {user.displayName}
            </span><br />
            <span className='UsersListItem__nickname'>
              @{user.nickname}
            </span>
          </div>
        </LinkToUser>
      )
    case UsersListLayout.Horizontal:
      return (
        <LinkToUser user={user}>
          <Avatar user={user} />
        </LinkToUser>
      )
  }

})
