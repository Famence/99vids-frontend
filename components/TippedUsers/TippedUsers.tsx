import React, { useCallback } from 'react'
import { useSelector } from 'react-redux'
import { tipsCountSelector, tipsSelector } from '@store/Favorites/selectors'
import { TipsGrid } from '@components/TipsGrid/TipsGrid'
import { useLoaderSelector } from '@store/Loaders/selectors'
import { LoaderTypes } from '@store/Loaders/constants'
import { useStore } from '@utils'
import { PostDummy } from '@components/PostDummy/PostDummy'

export const TippedUsers: React.FunctionComponent = React.memo(() => {
  const store = useStore()
  const tips = useSelector(tipsSelector)
  const tipsCount = useSelector(tipsCountSelector)
  const isLoading = useLoaderSelector(LoaderTypes.TippedUsersLoader)

  const onCanLoadMore = useCallback(async () => {
    if (typeof tipsCount === 'number' && tipsCount <= tips.length) return
    await store.dispatch.favorites.getTips({ pagination: { offset: tips.length } })
  }, [tips.length, tipsCount])

  return (
    <>
      <TipsGrid
        tips={tips}
        isLoading={isLoading}
        onCanLoadMore={onCanLoadMore}
      />
      {!tips.length && !isLoading && <PostDummy text='There are no tipped persons yet' />}
    </>
  )
})
