import React from 'react'
import { User } from '@models/Users'
import { SubscribeButton } from '@components/SubscribeButton/SubscribeButton'
import LockIcon from '@icons/lock.svg'
import { Button } from '@components/Button/Button'
import './LockedContentPlug.sass'

export const LockedContentPlug: React.FunctionComponent<{ model: User }> = React.memo(({ model }) => (
  <SubscribeButton model={model} className='LockedContentPlug' justLink>
    <LockIcon />
    <div className='LockedContentPlug__text'>Content locked</div>
    <Button>follow</Button>
  </SubscribeButton>
))
