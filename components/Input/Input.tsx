import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import { useField, useFormikContext } from 'formik'
import cn from 'classnames'
import { Currency } from '@models/commonTypes'
import EyeIcon from '@icons/eye.svg'
import ErrorIcon from '@icons/error.svg'
import { useForceUpdate } from '@utils'
import { currencySigns } from '@config/constants'
import './Input.sass'

const symbolsAfterDecimalDelimiterRegExp = /\.(\d*)$/

interface InputProps extends React.InputHTMLAttributes<any> {
  label?: string
  currency?: Currency
  allowDisplayPassword?: boolean
  formatValue?: <T = any>(value: T) => T
  className?: string
}

export const Input: React.FunctionComponent<InputProps> = React.memo(({
  label,
  currency,
  className,
  allowDisplayPassword,
  formatValue: _formatValue,
  ...props
}) => {
  const keepStartZeros = useRef(-1)
  const keepEndZeros = useRef(-1)
  const decimalDelimiterPosition = useRef(-1)
  const [forceUpdateDep, forceUpdate] = useForceUpdate()
  const [displayPassword, setDisplayPassword] = useState(false)
  const [field, meta, helpers] = useField(props)
  const form = useFormikContext()

  const isDigitalType = useMemo(() => ['number', 'decimal', 'money'].includes(props.type), [props.type])
  const isDecimalType = useMemo(() => ['decimal', 'money'].includes(props.type), [props.type])

  const onClickDisplayPassword = useMemo(() => (
    () => {
      if (!allowDisplayPassword) return
      setDisplayPassword(!displayPassword)
    }
  ), [displayPassword, allowDisplayPassword])

  const type = useMemo(() => {
    if (props.type === 'password' && displayPassword) return 'text'
    if (isDigitalType) return 'text'
    return props.type
  }, [props.type, displayPassword, isDigitalType])

  const formatValue = useCallback((rawValue) => {
    let formattedValue = rawValue

    if (isDigitalType) {
      let cleanedValue = rawValue.toString()
      if (decimalDelimiterPosition.current !== -1) cleanedValue = cleanedValue.replace(/(\d*\.\d*)(?:\.(\d*))?/, '$1')
      const regExp = isDecimalType ? /[^\d.]+/g : /\D+/g
      cleanedValue = cleanedValue.replace(regExp, '')

      if (props.type === 'money') {
        const symbolsAfterDecimalDelimiter = cleanedValue.match(symbolsAfterDecimalDelimiterRegExp)?.[1] || ''
        if (symbolsAfterDecimalDelimiter.length > 2) cleanedValue = cleanedValue.slice(0, -1)
      }

      let cleanedValueNumber = +cleanedValue || 0
      const zerosMatch = cleanedValue?.match(/(^0*)\d*(\.?)\d*?(0*$)/)
      if (zerosMatch) {
        keepStartZeros.current = zerosMatch[1].length
        decimalDelimiterPosition.current = zerosMatch[2] ? cleanedValue.indexOf('.') : -1
        keepEndZeros.current = zerosMatch[3].length
      } else {
        keepStartZeros.current = 0
        decimalDelimiterPosition.current = -1
        keepEndZeros.current = 0
      }

      if (props.type === 'money') cleanedValueNumber *= 100

      formattedValue = cleanedValueNumber
    }

    if (_formatValue) {
      formattedValue = _formatValue(formattedValue)
    }

    helpers.setValue(formattedValue)
    if (formattedValue === field.value) forceUpdate()
  }, [props.type, isDigitalType, isDecimalType, _formatValue, helpers.setValue, field.value, forceUpdate])

  const onChange = useCallback((event) => {
    field.onChange(event)
    formatValue(event.target.value)
  }, [field.onChange, formatValue])

  const value = useMemo(() => {
    let resultValue = field.value

    if (props.type === 'money') resultValue /= 100

    resultValue = resultValue?.toString() || resultValue

    if (isDigitalType) {
      resultValue = resultValue.replace(/^0*/, '')
      if (keepStartZeros.current > 0) resultValue = new Array(keepStartZeros.current).fill(0).join('') + resultValue

      if (decimalDelimiterPosition.current !== -1) {
        resultValue = resultValue.replace('.', '')
        resultValue = (
          resultValue.substring(0, decimalDelimiterPosition.current)
          + '.'
          + resultValue.substring(decimalDelimiterPosition.current)
        )
      }

      if (keepEndZeros.current > 0) resultValue += new Array(keepEndZeros.current).fill(0).join('')
    }

    return resultValue
  }, [field.value, isDigitalType, props.type, forceUpdateDep])

  useEffect(() => formatValue(value), [])

  const icon = useMemo(() => {
    if (props.type === 'password' && allowDisplayPassword) {
      return (
        <button
          type='button'
          className={cn('Input__control__display-password', { crossed: displayPassword })}
          onClick={onClickDisplayPassword}
        >
          <EyeIcon isFilled />
        </button>
      )
    }
    if (props.type === 'money' && currency)
      return currencySigns[currency]
    return null
  }, [props.type, allowDisplayPassword, displayPassword, onClickDisplayPassword, currency])

  return (
    <label className={cn('Input', className, `type-${props.type}`, { allowDisplayPassword })}>
      {label && (
        <>
          <span className='Input__label'>{label}</span>
          <br />
        </>
      )}
      <div className={cn('Input__control-container', { withIcon: icon })}>
        <input {...field} onChange={onChange} value={value} {...props} type={type} className='Input__control' />
        {icon && (<div className='Input__control-icon'>{icon}</div>)}
        {meta.touched && meta.error && form.submitCount > 0 && (
          <div className='Input__error'>
            <ErrorIcon className='Input__error-icon' isFilled />
            {meta.error}
          </div>
        )}
      </div>
    </label>
  )
})
