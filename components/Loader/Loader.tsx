import React from 'react'
import ReactLoader from 'react-loader'
import './Loader.sass'

export interface LoaderOptions {
  lines?: number
  length?: number
  width?: number
  radius?: number
  scale?: number
  corners?: number
  color?: string
  opacity?: number
  rotate?: number
  direction?: number
  speed?: number
  trail?: number
  fps?: number
  zIndex?: number
  top?: string
  left?: string
  shadow?: boolean
  hwaccel?: boolean
  position?: string
  loadedClassName?: string
}

export enum LoaderSize {
  SMALL = 'sm',
  NORMAL = 'md'
}

type LoaderProps = {
  loaded?: boolean
  options?: LoaderOptions
  className?: string
  size?: LoaderSize
  white?: boolean
}

export const Loader = React.memo(({
  size = LoaderSize.NORMAL,
  white = false,
  ...reactLoaderProps
}: LoaderProps) => {
  const options = {
    lines: 12,
    width: 3,
    radius: 10,
    length: 3,
    color: '#637280',
  }

  if (size === LoaderSize.SMALL) {
    options.lines = 10
    options.width = 2
    options.radius = 5
    options.length = 2
  }

  if (white) options.color = '#fff'


  return (
    <ReactLoader options={options} {...reactLoaderProps} />
  )
})

Loader.defaultProps = {
  loaded: false,
}
