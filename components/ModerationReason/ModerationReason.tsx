import React, { useEffect, useMemo } from 'react'
import { useSelector } from 'react-redux'
import { rejectReasonsSelector } from '@store/References/selectors'
import { useStore } from '@utils'

interface ModerationReasonProps {
  reason: string
}

export const ModerationReason: React.FunctionComponent<ModerationReasonProps> = React.memo(({ reason }) => {
  const store = useStore()
  const rejectReasons = useSelector(rejectReasonsSelector)

  useEffect( () => {
    if (!rejectReasons) store.dispatch.references.getRejectReasons()
  }, [])

  const displayReason = useMemo(() => {
    if (rejectReasons instanceof Array) {
      const foundReason = rejectReasons.find(_reason => _reason._id === reason)
      if (!foundReason) return reason
      return foundReason.text
    } else {
      return reason
    }
  }, [rejectReasons, reason])

  return displayReason
})
