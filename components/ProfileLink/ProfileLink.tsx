import React from 'react'
import cn from 'classnames'
import { UrlObject } from 'url'
import { User } from '@models/Users'
import { Avatar } from '@components/Avatar/Avatar'
import CrownIcon from '@icons/crown.svg'
import RecommendedIcon from '@icons/recommended.svg'
import './ProfileLink.sass'
import { LinkToUser } from '@components/Link/LinkToUser'

export interface ProfileLinkProps {
  user: User
  nick?: React.ReactElement | string
  href?: string | UrlObject
  size?: string
  isRecommended?: boolean
  crown?: boolean
  className?: string
}

export const ProfileLink: React.FunctionComponent<ProfileLinkProps> = React.memo(({
  user,
  nick,
  href,
  size,
  isRecommended = false,
  crown = false,
  className,
}) => (
  <LinkToUser user={user} href={href}>
    <div className={cn('ProfileLink', { isRecommended }, { [`size-${size}`]: size }, className)}>
      <Avatar user={user} />
      <span className='ProfileLink__DisplayName'>
        <span className='ProfileLink__DisplayName-name'>
          {user.displayName}{isRecommended && <RecommendedIcon fill='#7352a1' />}
          {crown && <CrownIcon />}
        </span>
        <span className='ProfileLink__DisplayName-nick'>{nick || user.nickname}</span>
      </span>
    </div>
  </LinkToUser>
))
