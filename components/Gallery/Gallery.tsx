import React, { useCallback, useEffect, useRef, useState } from 'react'
import { useSelector } from 'react-redux'
import Lightbox from 'react-image-lightbox'
import 'react-image-lightbox/style.css'
import { galleryStateSelector } from '@store/Gallery/selectors'
import { checkIsTouch, resolveApiUri, useStore } from '@root/utils'
import './Gallery.sass'

export const Gallery: React.FunctionComponent = () => {
  const hideGalleryTimeout = useRef<number | undefined>()
  const store = useStore()
  const { isOpen, postPhotos, photoIndex } = useSelector(galleryStateSelector)

  const [isNavigationShown, setIsNavigationShown] = useState(true)

  const close = useCallback(() => store.dispatch.gallery.close(), [])

  const onMouseMoveInsideGallery = useCallback(() => {
    setIsNavigationShown(true)

    clearTimeout(hideGalleryTimeout.current)
    // @ts-ignore
    hideGalleryTimeout.current = setTimeout(() => setIsNavigationShown(false), 1000)
  }, [hideGalleryTimeout])

  const onAfterOpen = useCallback(() => {
    if (!checkIsTouch()) setTimeout(() => setIsNavigationShown(false), 1000)
  }, [])

  const openPreviousImage = useCallback(() => {
    store.dispatch.gallery.setPhotoIndex(photoIndex - 1)
  }, [photoIndex])

  const openNextImage = useCallback(() => {
    store.dispatch.gallery.setPhotoIndex(photoIndex + 1)
  }, [photoIndex])

  const reactModalProps = useCallback(() => ({
    portalClassName: isNavigationShown ? 'hidden-ctrl ReactModalPortal' : 'ReactModalPortal',
  }), [isNavigationShown])

  useEffect(() => {
    return () => setIsNavigationShown(true)
  }, [])

  const currentPhotoPaths = postPhotos[photoIndex]?.photoInfo.paths
  const prevPhotoPaths = postPhotos[photoIndex - 1]?.photoInfo.paths
  const nextPhotoPaths = postPhotos[photoIndex + 1]?.photoInfo.paths

  return isOpen && (
    <div onMouseMove={onMouseMoveInsideGallery}>
      <Lightbox
        animationOnKeyInput
        imagePadding={0}
        wrapperClassName={!isNavigationShown ? 'hidden-ctrl' : undefined}
        reactModalProps={reactModalProps}
        mainSrc={currentPhotoPaths && resolveApiUri(currentPhotoPaths.full)}
        prevSrc={prevPhotoPaths && resolveApiUri(prevPhotoPaths.full)}
        nextSrc={nextPhotoPaths && resolveApiUri(nextPhotoPaths.full)}
        mainSrcThumbnail={currentPhotoPaths && resolveApiUri(currentPhotoPaths.thumb)}
        prevSrcThumbnail={prevPhotoPaths && resolveApiUri(prevPhotoPaths.thumb)}
        nextSrcThumbnail={nextPhotoPaths && resolveApiUri(nextPhotoPaths.thumb)}
        onCloseRequest={close}
        onAfterOpen={onAfterOpen}
        onMovePrevRequest={openPreviousImage}
        onMoveNextRequest={openNextImage}
      />
    </div>
  )
}
