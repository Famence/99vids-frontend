import React, { useRef } from 'react'
import cn from 'classnames'
import moment from 'moment'
import { User } from '@models/Users'
import { Loader } from '@components/Loader/Loader'
import { UserIconWithName } from '@components/UserIconWithName/UserIconWithName'
import { LinkWithA } from '@components/Link/LinkWithA'
import {
  EqualHeightGroup,
  getTariffMonths, OnCanLoadMoreCallback,
  price, useCanLoadMore,
  useEqualHeight,
  useEqualHeightElement,
  useIsSubscribed,
} from '@utils'
import { SubscribeButton, SubscribeButtonMode } from '@components/SubscribeButton/SubscribeButton'
import { ButtonTheme } from '@components/Button/Button'
import { ChatButton } from '@components/Chat/ChatButton/ChatButton'
import IconEnvelope from '@icons/send.svg'
import { SubscriptionStatus } from '@models/Subscription'
import './UsersGrid.sass'

const useCanLoadMoreOptions = { numberOfScreens: .5 }

export enum UsersGridItemControlsType {
  Common = 'common',
  Favorite = 'favorite',
}

interface UsersGridProps {
  users: User[]
  isLoading?: boolean
  showStar?: boolean
  withSubscription?: boolean
  withPostsStatistics?: boolean
  onCanLoadMore?: OnCanLoadMoreCallback
  itemControlType?: UsersGridItemControlsType
  className?: string
}

export const UsersGrid: React.FunctionComponent<UsersGridProps> = React.memo(({
  users,
  isLoading = false,
  showStar = false,
  withSubscription = false,
  withPostsStatistics = false,
  itemControlType = UsersGridItemControlsType.Common,
  onCanLoadMore,
  className,
}) => {
  const wrapperRef = useRef(null)
  const { isLoadingMore } = useCanLoadMore(onCanLoadMore, wrapperRef, useCanLoadMoreOptions)
  const equalHeightGroup = useEqualHeight()

  return (
    <div className={cn('UsersGrid', className)} ref={wrapperRef}>
      <Loader loaded={!isLoading || isLoadingMore}>
        <ul className='UsersGrid__list'>
          {users.map((user) => (
            <li className='UsersGrid__list-item' key={user._id}>
              <LinkWithA
                route='userPage'
                params={{ userId: user.nickname || user._id }}
                className='UsersGrid__user-link'
              >
                <UserIconWithName user={user} />
              </LinkWithA>
              {(withSubscription || withPostsStatistics) && (
                <UsersGridItemTable
                  user={user}
                  withSubscription={withSubscription}
                  withPostsStatistics={withPostsStatistics}
                  equalHeightGroup={equalHeightGroup}
                />
              )}
              <div className={cn('UsersGrid__list-item-controls', `type-${itemControlType}`)}>
                <UsersGridItemControls type={itemControlType} user={user} />
              </div>
            </li>
          ))}
        </ul>
        <Loader loaded={!isLoadingMore} />
      </Loader>
    </div>
  )
})

type UsersGridItemTableProps = {
  user: User
  equalHeightGroup: EqualHeightGroup
} & Pick<UsersGridProps, 'withSubscription' | 'withPostsStatistics'>

const UsersGridItemTable: React.FunctionComponent<UsersGridItemTableProps> = React.memo(({
  user,
  withSubscription = false,
  withPostsStatistics = false,
  equalHeightGroup,
}) => {
  const wrapperRef = useRef(null)
  useEqualHeightElement(equalHeightGroup, wrapperRef)

  return (
    <div className='UsersGrid__list-item-table' ref={wrapperRef}>
      <table>
        <tbody>
          {withSubscription && user.subscription?.status === SubscriptionStatus.Active && (
            <>
              <tr>
                <td>
                  Subscription:
                </td>
                <td className='textEnd'>
                  ${price(user.subscription.price)}
                </td>
              </tr>
              <tr>
                <td>
                  Tariff:
                </td>
                <td className='textEnd'>
                  <SubscribeButton model={user} justLink>
                    {getTariffMonths(user.subscription.currentDuration)} month
                  </SubscribeButton>
                </td>
              </tr>
              <tr>
                <td>
                  End date:
                </td>
                <td className='textEnd'>
                  {moment(user.subscription.validUntil).format('DD/MM/YY')}
                </td>
              </tr>
            </>
          )}
          {withPostsStatistics && (
            <>
              <tr>
                <td>
                  Posts:
                </td>
                <td className='textEnd'>
                  {user.postsStatistic.postsCount}
                </td>
              </tr>
              <tr>
                <td>
                  Videos:
                </td>
                <td className='textEnd'>
                  {user.postsStatistic.videosCount}
                </td>
              </tr>
              <tr>
                <td>
                  Photos:
                </td>
                <td className='textEnd'>
                  {user.postsStatistic.photosCount}
                </td>
              </tr>
            </>
          )}
        </tbody>
      </table>
    </div>
  )
})

interface UsersGridItemControlsProps {
  user: User
  type?: UsersGridItemControlsType
}

export const UsersGridItemControls: React.FunctionComponent<UsersGridItemControlsProps> = ({
  user,
  type: controlsType = UsersGridItemControlsType.Common,
}) => {
  const isSubscribed = useIsSubscribed(user)

  switch (controlsType) {
    case UsersGridItemControlsType.Common:
      return (
        <>
          <SubscribeButton
            model={user}
            mode={SubscribeButtonMode.Unsubscribe}
            theme={ButtonTheme.LIGHT}
            small
          />
          <ChatButton targetUser={user} small>
            <IconEnvelope isFilled />
          </ChatButton>
          <SubscribeButton
            model={user}
            mode={SubscribeButtonMode.Upgrade}
            small
          >
            Change access
          </SubscribeButton>
        </>
      )
    case UsersGridItemControlsType.Favorite:
      return isSubscribed ? (
        <ChatButton
          targetUser={user}
          theme={ButtonTheme.LIGHT}
          small
        >
          Send message
        </ChatButton>
      ) : (
        <SubscribeButton
          model={user}
          small
        >
          Follow
        </SubscribeButton>
      )
    default:
      return null
  }
}
