import React from 'react'
import { useSelector } from 'react-redux'
import { currentUserSelector } from '@store/Users/selectors'
import { Button, ButtonTheme } from '@components/Button/Button'
import LogInIcon from '@icons/login.svg'
import './CurrentUserControls.sass'

export const CurrentUserControls: React.FunctionComponent = React.memo(() => {
  const currentUser = useSelector(currentUserSelector)

  return (
    <div className='CurrentUserControls'>
      {currentUser ? (
        currentUser.nickname
      ) : (
        <Button
          route='logIn'
          theme={ButtonTheme.LIGHT}
          className='CurrentUserControls__logInButton'
        >
          <LogInIcon isFilled isInline />
          LogIn
        </Button>
      )}
    </div>
  )
})
