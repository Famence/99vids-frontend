import React from 'react'
import cn from 'classnames'
import './Block.sass'

interface BlockProps {
  title?: React.ReactNode
  topContent?: React.ReactNode
  withTopBorder?: boolean
  bigTabs?: boolean
  className?: string
}

export const Block: React.FunctionComponent<BlockProps> = ({
  children,
  title = null,
  topContent = null,
  withTopBorder = false,
  bigTabs = false,
  className,
}) => (
  <div className={cn('Block', className, { withTopBorder, bigTabs })}>
    {(title || topContent) && (
      <div className='Block__top'>
        {title && <h3 className='Block__title'>{title}</h3>}
        {topContent}
      </div>
    )}
    {children}
  </div>
)
