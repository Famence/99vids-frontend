import React from 'react'
import cn from 'classnames'
import { NavigationMenu } from '@components/NavigationMenu/NavigationMenu'
import './Sidebar.sass'

interface SidebarProps {
  isOpened: boolean
}

export const Sidebar: React.FunctionComponent<SidebarProps> = ({ isOpened = false }) => {
  return (
    <div className={cn('Sidebar', { isOpened })}>
      <NavigationMenu />
    </div>
  )
}
