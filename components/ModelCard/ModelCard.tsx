import React from 'react'
import { useSelector } from 'react-redux'
import cn from 'classnames'
import { User, UserRoles } from '@models/Users'
import { currentUserRoleSelector } from '@store/Users/selectors'
import IconEye from '@icons/eye.svg'
import IconLike from '@icons/like.svg'
import { FavoriteStarButton } from '@components/FavoriteStarButton/FavoriteStarButton'
import { ModelCardSubscriptionBlock } from './ModelCardSubscriptionBlock/ModelCardSubscriptionBlock'
import { ModelCardAvatar } from './ModelCardAvatar/ModelCardAvatar'
import './ModelCard.sass'

interface ModelCardProps {
  model: User
  isOwn?: boolean
}

export const ModelCard: React.FunctionComponent<ModelCardProps> = React.memo(({
  model,
  isOwn = false,
}) => {
  const currentUserRole = useSelector(currentUserRoleSelector)

  const renderButtons = isOwn ? true : currentUserRole !== UserRoles.MODEL

  return (
    <div className='ModelCard'>
      <ModelCardAvatar model={model} isOwn={isOwn} />
      <div className={cn('ModelCard__bottom', { 'noButtons': !renderButtons })}>
        {model.postsStatistic && (
          <div className="ModelCard__rates">
            <div className="ModelCard__rates-item">
              <IconEye isInline isFilled />
              <span>{model.postsStatistic.viewsCount ?? 0}</span>
            </div>
            <div className="ModelCard__rates-item">
              <IconLike isInline isFilled />
              <span>{model.postsStatistic.likesCount ?? 0}</span>
            </div>
          </div>
        )}
        <div className="ModelCard__info">
          <span className='ModelCard__name'>{model.displayName}</span>
          {currentUserRole === UserRoles.FAN && <FavoriteStarButton user={model} />}
          <div className="ModelCard__nickname">{`@${model.nickname}`}</div>
        </div>
        {renderButtons && <ModelCardSubscriptionBlock model={model} isOwn={isOwn} />}
      </div>
    </div>
  )
})
