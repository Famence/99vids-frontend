import React from 'react'
import { Button, ButtonTheme } from '@components/Button/Button'
import { SubscriptionBlock } from '@components/SubscriptionBlock/SubscriptionBlock'
import { User } from '@models/Users'
import './ModelCardSubscriptionBlock.sass'

interface ModelCardSubscriptionBlockProps {
  model: User
  isOwn?: boolean
}

export const ModelCardSubscriptionBlock: React.FunctionComponent<ModelCardSubscriptionBlockProps> = React.memo(({
  model,
  isOwn = false,
}) => (
  <div className='ModelCardSubscriptionBlock'>
    {isOwn ? (
      <Button theme={ButtonTheme.LIGHT} route='editProfile'>edit profile</Button>
    ) : (
      <SubscriptionBlock model={model} />
    )}
  </div>
))
