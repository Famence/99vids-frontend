import React, { useCallback, useMemo } from 'react'
import { useSelector } from 'react-redux'
import cn from 'classnames'
import Routes from '@routes/config'
import { User, UserRoles } from '@models/Users'
import { currentUserRoleSelector } from '@store/Users/selectors'
import { Img } from '@components/Img/Img'
import { ModelCardActions } from '@components/ModelCard/ModelCardActions/ModelCardActions'
import FemaleDummyAvatar from '@icons/dummy-avatar-female.svg'
import { useStore } from '@utils'
import { Router } from '@routes'
import './ModelCardAvatar.sass'

interface ModelCardAvatarProps {
  model: User
  isOwn?: boolean
}

export const ModelCardAvatar: React.FunctionComponent<ModelCardAvatarProps> = React.memo(({
  model,
  isOwn = false,
}) => {
  const store = useStore()
  const currentUserRole = useSelector(currentUserRoleSelector)

  const {
    photoByUser,
    photoByProvider,
    backgroundPhoto,
  } = model

  const avatar = useMemo(() => (
    backgroundPhoto?.paths || photoByUser?.[0]?.paths || photoByProvider || null
  ), [backgroundPhoto, photoByUser, photoByProvider])

  const isDummy = avatar === null

  const onAvatarClick = useCallback(() => {
    if (isOwn && isDummy) {
      Router.pushRoute('profileEdit')
      store.dispatch.user.updateAvatar({ userId: model._id })
    }
  }, [currentUserRole, isDummy, model])

  return (
    <div className={cn('ModelCardAvatar', { isDummy, isOwn })} onClick={onAvatarClick}>
      {isDummy ? (
        <>
          <FemaleDummyAvatar />
          {isOwn && (
            <span className='ModelCardAvatar__text'>
              Upload Avatar
            </span>
          )}
        </>
      ) : (
        <Img src={avatar} isFull whiteLoader />
      )}
      {!isOwn && currentUserRole === UserRoles.FAN && (
        <ModelCardActions model={model} />
      )}
    </div>
  )
})
