import React from 'react'
import { User } from '@models/Users'
import { ChatButton } from '@components/Chat/ChatButton/ChatButton'
import { TipButton } from '@components/TipButton/TipButton'
import IconEnvelope from '@icons/envelope.svg'
import IconTips from '@icons/tips.svg'
import './ModelCardActions.sass'

interface ModelCardActionsProps {
  model: User
}

export const ModelCardActions: React.FunctionComponent<ModelCardActionsProps> = React.memo(({ model }) => (
  <div className="ModelCardActions">
    <ChatButton targetUser={model}>
      <IconEnvelope isFilled />
    </ChatButton>
    <TipButton model={model}>
      <IconTips isFilled />
    </TipButton>
  </div>
))
