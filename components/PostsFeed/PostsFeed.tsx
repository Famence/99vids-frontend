import React, { useRef } from 'react'
import cn from 'classnames'
import { PostType } from '@models/Posts'
import { Post, PostProps } from '@components/Post/Post'
import { Loader } from '@components/Loader/Loader'
import { OnCanLoadMoreCallback, useCanLoadMore } from '@utils'

interface PostsFeedProps extends Partial<PostProps> {
  posts: PostType[]
  isLoading?: boolean
  onCanLoadMore?: OnCanLoadMoreCallback
  className?: string
  postClassName?: string
}

export const PostsFeed: React.FunctionComponent<PostsFeedProps> = React.memo(({
  posts,
  isLoading = false,
  onCanLoadMore,
  className,
  postClassName,
  ...postProps
}) => {
  const wrapperRef = useRef(null)
  const { isLoadingMore } = useCanLoadMore(onCanLoadMore, wrapperRef)

  return (
    <Loader loaded={!isLoading || isLoadingMore}>
      <div className={cn('PostsFeed', className)} ref={wrapperRef}>
        {posts.map(post => (
          <Post
            {...postProps}
            className={postClassName}
            post={post}
            key={post._id}
          />
        ))}
      </div>
      <Loader loaded={!isLoadingMore} />
    </Loader>
  )
})
