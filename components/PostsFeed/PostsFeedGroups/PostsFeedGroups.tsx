import React, { useCallback, useMemo } from 'react'
import { useSelector } from 'react-redux'
import cn from 'classnames'
import { User } from '@models/Users'
import { PostsGroup, postsGroupsLabels, PostType, userPostsStatisticsKeysByPostsGroups } from '@models/Posts'
import { TabChangeHandler, TabOption, Tabs } from '@components/Tabs/Tabs'
import { currentUserSelector } from '@store/Users/selectors'
import IconPhoto from '@icons/photo.svg'
import IconVideo from '@icons/video.svg'
import { useUserPosts } from '@utils'
import './PostsFeedGroups.sass'

export interface CommonPostsFeedTabsProps {
  onGroupChange: (group: PostsGroup) => void
  className?: string
}

export interface UserPostsFeedTabsProps extends CommonPostsFeedTabsProps {
  user: User
}

export interface PostsFeedTabsProps extends CommonPostsFeedTabsProps {
  active: PostsGroup
}

const postsFeedGroups = [PostsGroup.Photos, PostsGroup.Videos, PostsGroup.All]
const userOwnPostsFeedGroups = [PostsGroup.Published, PostsGroup.Unpublished]

export const UserPostsFeedTabs: React.FunctionComponent<UserPostsFeedTabsProps> = React.memo(({
  user,
  onGroupChange,
  className,
}) => {
  const currentUser = useSelector(currentUserSelector)
  const userPostsState = useUserPosts(user._id)

  const availableGroups = useMemo<PostsGroup[]>(() => {
    return user._id === currentUser?._id ? userOwnPostsFeedGroups : postsFeedGroups
  }, [user, currentUser])

  const availableGroupsOptions = useMemo(() => (
    availableGroups.map((group) => ({
      group,
      count: user.postsStatistic[userPostsStatisticsKeysByPostsGroups[group]],
    }))
  ), [availableGroups, user.postsStatistic])

  const tabs = useMemo<TabOption<PostsGroup>[]>(() => (
    postsGroupsToTabs(availableGroupsOptions)
  ), [availableGroupsOptions])

  const activeTab = useMemo(() => (
    tabs.find((tab) => tab.value === userPostsState.group)
  ), [tabs, userPostsState.group])

  const onTabChange = useCallback<TabChangeHandler<PostsGroup>>((tab) => {
    onGroupChange(tab.value)
  }, [onGroupChange])

  return (
    <Tabs
      tabs={tabs}
      active={activeTab}
      onChange={onTabChange}
      className={cn('PostsFeedTabs UserPostsFeedTabs', className)}
    />
  )
})

export const PostsFeedTabs: React.FunctionComponent<PostsFeedTabsProps> = React.memo(({
  active = PostsGroup.All,
  onGroupChange,
  className,
}) => {
  const availableGroupsOptions = useMemo(() => (
    postsFeedGroups.map((group) => ({ group }))
  ), [])

  const tabs = useMemo<TabOption<PostsGroup>[]>(() => (
    postsGroupsToTabs(availableGroupsOptions)
  ), [availableGroupsOptions])

  const activeTab = useMemo(() => (
    tabs.find((tab) => tab.value === active)
  ), [tabs, active])

  const onTabChange = useCallback<TabChangeHandler<PostsGroup>>((tab) => {
    onGroupChange(tab.value)
  }, [onGroupChange])

  return (
    <Tabs
      tabs={tabs}
      active={activeTab}
      onChange={onTabChange}
      className={cn('PostsFeedTabs', className)}
    />
  )
})

interface PostsGroupOption {
  group: PostsGroup
  count?: number
}

const postsGroupsToTabs = (postsGroupsOptions: PostsGroupOption[]) => (
  postsGroupsOptions.map((option) => {
    let icon
    switch (option.group) {
      case PostsGroup.Photos:
        icon = <IconPhoto isFilled isInline />
        break
      case PostsGroup.Videos:
        icon = <IconVideo isFilled isInline />
        break
      default:
        icon = null
    }

    return {
      value: option.group,
      label: (
        <>
          {icon}
          {postsGroupsLabels[option.group]}
          {typeof option.count === 'number' && ` (${option.count})`}
        </>
      ),
    }
  })
)
