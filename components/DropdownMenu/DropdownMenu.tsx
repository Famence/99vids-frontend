import React, { useCallback, useEffect, useRef, useState } from 'react'
import cn from 'classnames'
import DotsIcon from '@icons/dots.svg'
import { SvgIcon } from '*.svg'
import './DropdownMenu.sass'

export interface DropdownMenuOptionType {
  label: React.ReactNode
  onClick: () => void
  icon?: SvgIcon
  className?: string
}

interface DropdownMenuProps {
  options: DropdownMenuOptionType[]
  closeOnClick?: boolean
  className?: string
}

export const DropdownMenu = React.memo(({ options, closeOnClick = true, className }: DropdownMenuProps) => {
  const wrapper = useRef<HTMLElement>(null)
  const [isOpened, setIsOpened] = useState(false)

  const open = useCallback(() => setIsOpened(true), [])
  const close = useCallback(() => setIsOpened(false), [])

  const onOptionClick = useCallback((optionClickCallback: DropdownMenuOptionType['onClick']) => {
    if (closeOnClick) close()
    if (optionClickCallback) optionClickCallback()
  }, [closeOnClick, close])

  useEffect(() => {
    const onDocumentClick = (event: MouseEvent) => {
      if (wrapper.current?.contains(event.target as Node)) return
      close()
    }
    document.addEventListener('mousedown', onDocumentClick)
    return () => document.removeEventListener('mousedown', onDocumentClick)
  }, [])

  return (
    <div className={cn('DropdownMenu', className, { isOpened })} ref={wrapper}>
      <button type='button' className='DropdownMenu__toggler' onClick={open}>
        <DotsIcon className='dotsIcon' isFilled />
      </button>
      <ul className='DropdownMenu__drop'>
        {options.map((option, index) => (
          <DropdownMenuOption option={option} onClick={onOptionClick} key={index} />
        ))}
      </ul>
    </div>
  )
})


interface DropdownMenuOptionProps {
  option: DropdownMenuOptionType
  onClick: (optionClickCallback: DropdownMenuOptionType['onClick']) => void
}

export const DropdownMenuOption = ({ option, onClick }: DropdownMenuOptionProps) => {
  const handleClick = useCallback(() => {
    onClick(option.onClick)
  }, [onClick, option.onClick])

  return (
    <li className={cn('DropdownMenu__Option', option.className)}>
      <button type='button' onClick={handleClick}>
        {option.icon && <option.icon className='DropdownMenu__Option-icon' isFilled />}
        <span className='DropdownMenu__Option-label'>{option.label}</span>
      </button>
    </li>
  )
}
