import React, { useEffect, useMemo } from 'react'
import { useSelector } from 'react-redux'
import cn from 'classnames'
import { followingsStateSelector, followingUsersSelector } from '@store/Followings/selectors'
import { User } from '@models/Users'
import { Block } from '@components/Block/Block'
import { Button, ButtonTheme } from '@components/Button/Button'
import { UsersList, UsersListProps } from '@components/UsersList/UsersList'
import { useStore } from '@utils'
import { useLoaderSelector } from '@store/Loaders/selectors'
import { LoaderTypes } from '@store/Loaders/constants'
import { Loader } from '@components/Loader/Loader'
import './FollowingUsersBlock.sass'

interface RecommendationsBlockProps extends Omit<UsersListProps, 'users'> {
  itemsNumber?: number
  title?: string
  excludeUserIds?: User['_id'][]
  className?: string
}

export const FollowingUsersBlock: React.FunctionComponent<RecommendationsBlockProps> = React.memo(({
  itemsNumber = 4,
  title,
  excludeUserIds,
  className,
  ...usersListProps
}) => {
  const store = useStore()
  const followingUsers = useSelector(followingUsersSelector)
  const { count } = useSelector(followingsStateSelector)
  const isLoading = useLoaderSelector(LoaderTypes.FollowingUsersLoader)

  const followingUsersToDisplay = useMemo(() => {
    const usersToInclude = excludeUserIds?.length ? (
      followingUsers.filter((user) => !excludeUserIds.includes(user._id))
    ) : followingUsers

    return usersToInclude.slice(0, itemsNumber)
  }, [followingUsers, excludeUserIds, itemsNumber])

  useEffect(() => {
    // if (typeof count === 'undefined' && !isLoading)
    //   store.dispatch.users.getFollowing()
  }, [])

  return (
    <Block
      title={title || 'Followed'}
      className={cn('FollowingUsersBlock', className)}
    >
      <Loader loaded={!isLoading}>
        <UsersList users={followingUsersToDisplay} {...usersListProps} />
      </Loader>
      <Button theme={ButtonTheme.LIGHT} route='following'>view more</Button>
    </Block>
  )
})
