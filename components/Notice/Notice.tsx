import React from 'react'
import cn from 'classnames'
import './Notice.sass'

interface NoticeProps {
  status: string // TODO: enum
  icon?: React.ReactNode
  buttons?: React.ReactNode
  className?: string
}

const Notice: React.FunctionComponent<NoticeProps> = ({ children, status, icon, buttons, className }) => (
  <div className={cn('Notice', `Notice--status-${status}`, className, { withIcon: icon })}>
    {icon && <div className='Notice-icon'>{icon}</div>}
    <div className='Notice-contents'>{children}</div>
    {buttons && <div className='Notice-buttons'>{buttons}</div>}
  </div>
)

export default Notice
