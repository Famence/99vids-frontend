import React, { useState } from 'react'
import { Header } from '@components/Header/Header'
import { Sidebar } from '@components/Sidebar/Sidebar'
import { Gallery } from '@components/Gallery/Gallery'
import './AppRoot.sass'

export const AppRoot: React.FunctionComponent = ({ children }) => {
  const [isSidebarOpened, setIsSidebarOpened] = useState(false)

  return (
    <div className='AppRoot'>
      <Header isSidebarOpened={isSidebarOpened} setIsSidebarOpened={setIsSidebarOpened} />
      <Sidebar isOpened={isSidebarOpened} />
      <main className='pageWrapper'>
        {children}
      </main>
      <Gallery />
    </div>
  )
}
