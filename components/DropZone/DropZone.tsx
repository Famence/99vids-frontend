import React, { useEffect, useMemo } from 'react'
import { DropzoneOptions, DropzoneProps as ReactDropzoneProps, useDropzone } from 'react-dropzone'
import { toast } from 'react-toastify'
import cn from 'classnames'

export interface DropZoneProps extends Omit<ReactDropzoneProps, 'children'> {
  fileInputRef?: React.Ref<any>
  className?: string
  children: React.ReactNode
}

export const DropZone: React.FunctionComponent<DropZoneProps> = React.memo(({
  children,
  className,
  fileInputRef,
  onDropRejected: _onDropRejected,
  ...dropZoneOptions
}) => {
  const accept = dropZoneOptions.accept || 'image/png,image/jpeg,video/*'
  const onDropRejected = useMemo<DropzoneOptions['onDropRejected']>(() => (
    (files, event) => {
      if (!dropZoneOptions.multiple && files.length > 1)
        toast.error('Please select just one file')
      else
        toast.error('File type not allowed')

      if (_onDropRejected) _onDropRejected(files, event)
    }
  ), [_onDropRejected, dropZoneOptions.multiple])

  const { getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject } = useDropzone({
    ...dropZoneOptions,
    accept,
    onDropRejected,
  })

  const inputProps = getInputProps()

  useEffect(() => {
    if (fileInputRef) fileInputRef.current = inputProps.ref.current
  }, [inputProps.ref])

  return (
    <div
      {...getRootProps()}
      className={cn(className, {
        'hover': isDragActive,
        'accept': isDragAccept,
        'reject': isDragReject,
      })}
    >
      <input {...inputProps} />
      {children}
    </div>
  )
})
