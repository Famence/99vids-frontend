import React, { useCallback, useMemo } from 'react'
import { FollowingStatus, followingStatusLabels } from '@models/Subscription'
import { TabChangeHandler, TabOption, Tabs } from '@components/Tabs/Tabs'

const followingStatuses = [FollowingStatus.Subscribed, FollowingStatus.Unsubscribed, FollowingStatus.All]

const tabsOptions = followingStatuses.map<TabOption<FollowingStatus>>((status) => ({
  value: status,
  label: followingStatusLabels[status],
}))

export interface FollowingStatusTabsProps {
  active: FollowingStatus
  onChange: (newStatus: FollowingStatus) => void
}

export const FollowingStatusTabs: React.FunctionComponent<FollowingStatusTabsProps> = React.memo(({
  active,
  onChange,
}) => {
  const activeTab = useMemo(() => (
    tabsOptions.find((tab) => tab.value === active)
  ), [active])

  const onTabChange = useCallback<TabChangeHandler<FollowingStatus>>((selectedTab) => (
    onChange(selectedTab.value)
  ), [onChange])

  return (
    <Tabs
      tabs={tabsOptions}
      active={activeTab}
      onChange={onTabChange}
      className='FollowingStatusTabs'
    />
  )
})
