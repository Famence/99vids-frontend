import React from 'react'
import { useSelector } from 'react-redux'
import { PostType } from '@models/Posts'
import { currentUserSelector } from '@store/Users/selectors'
import ModerationReason from '@components/ModerationReason'
import AttentionIcon from '@icons/attention.svg'
import HourGlassIcon from '@icons/hourglass.svg'
import Notice from '@components/Notice/Notice'

interface PostNoticeProps {
  post: PostType
}

export const PostNotice: React.FunctionComponent<PostNoticeProps> = ({ post }) => {
  const { isPromo, moderated, reason: rejectReason, author } = post
  const currentUser = useSelector(currentUserSelector)

  const isOwn = currentUser && currentUser._id === author._id
  const promoOnModeration = isPromo && !moderated

  if (!isOwn || !promoOnModeration) return null

  const noticeStatus = rejectReason ? 'fail' : 'info'
  const noticeContents = rejectReason ? (
    <>
      <b>This post didn't pass the control</b><br />
      <ModerationReason reason={rejectReason} /><br />
      Please edit this post or choose another one WITHOUT NUDITY
    </>
  ) : (
    <>
      <b>Your Promo post is on moderation</b><br />
      It will be published automatically as soon as it pass the nudity control
    </>
  )
  const noticeIcon = rejectReason ? (
    <AttentionIcon />
  ) : (
    <HourGlassIcon />
  )
  const noticeButtons = null

  return (
    <Notice
      icon={noticeIcon}
      buttons={noticeButtons}
      status={noticeStatus}
    >
      {noticeContents}
    </Notice>
  )
}
