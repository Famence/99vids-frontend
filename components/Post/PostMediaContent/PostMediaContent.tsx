import React, { useCallback, useContext, useEffect, useMemo, useRef, useState } from 'react'
import MagicSliderDots from 'react-magic-slider-dots'
import {
  ExtendedPostMediaItem,
  ExtendedPostMediaPhotoItem,
  ExtendedPostMediaVideoItem,
  PostMediaItemTypes,
  PostType,
} from '@models/Posts'
import { calculateAspectRatio, sortByPosition } from '@root/utils'
import { Slider } from '@components/Slider/Slider'
import { PostMediaPhotoItem, PostMediaVideoItem } from '@components/Post/PostMediaItem/PostMediaItem'
import { VideoProps } from '@components/Video/Video'
import { AppReactContext } from '@utils/app'
import './PostMediaContent.sass'

const sliderSettings = {
  dots: true,
  infinite: false, // WARNING!!! DO NOT USE infinite slider coz video inside cloned slides plays together with main
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  draggable: false,
}

interface PostMediaContentProps {
  post: PostType
  videoProps?: Partial<VideoProps>
}

export const PostMediaContent: React.FunctionComponent<PostMediaContentProps> = React.memo(({ post, videoProps }) => {
  const wrapper = useRef(null)
  const { isMobile } = useContext(AppReactContext)

  const [postMediaItems, setPostMediaItems] = useState<ExtendedPostMediaItem[]>([])

  const playVideoOnScroll = useCallback(() => {
    if (!post.media?.video.length || !wrapper.current || !postMediaItems.find(item => item.canPlayOnScroll)) return
    const offsetTop = wrapper.current.getBoundingClientRect().top
    const inRange = offsetTop > 0 && offsetTop < window.innerHeight
    let newPostMediaItems = postMediaItems

    if (inRange) {
      if (newPostMediaItems.find(item => !item.playing)) {
        newPostMediaItems = postMediaItems.map((item) => ({
          ...item,
          playing: item.canPlayOnScroll || item.playing,
        }))
      }
    } else if (newPostMediaItems.find(item => item.playing)) {
      newPostMediaItems = postMediaItems.map((item) => ({
        ...item,
        playing: item.canPlayOnScroll && item.playing ? false : item.playing,
      }))
    }

    if (newPostMediaItems !== postMediaItems) setPostMediaItems(newPostMediaItems)
  }, [post, postMediaItems])

  useEffect(() => {
    window.addEventListener('scroll', playVideoOnScroll)
    return () => window.removeEventListener('scroll', playVideoOnScroll)
  }, [playVideoOnScroll])

  useEffect(() => {
    const { photo, video } = post.media
    const list: ExtendedPostMediaItem[] = []

    photo.forEach((photoItem) => {
      list.push({
        ...photoItem,
        type: PostMediaItemTypes.PHOTO,
      })
    })

    video.forEach((videoItem) => {
      list.push({
        ...videoItem,
        type: PostMediaItemTypes.VIDEO,
        playing: false,
        canPlayOnScroll: videoItem.position === 1,
      })
    })

    setPostMediaItems(sortByPosition(list))
  }, [post])

  const firstPostMediaItem = postMediaItems[0]

  const firstMediaItemAspectRatio = useMemo(() => (
    calculateAspectRatio(firstPostMediaItem)
  ), [firstPostMediaItem])

  const beforeSlickChange = useCallback((oldIndex, newIndex) => {
    let newPostMediaItems = postMediaItems

    if (postMediaItems[newIndex] && postMediaItems[newIndex].type === 'video') {
      newPostMediaItems = [...newPostMediaItems]
      newPostMediaItems[newIndex].playing = true
      newPostMediaItems[newIndex].canPlayOnScroll = true
    }

    if (postMediaItems[oldIndex] && postMediaItems[oldIndex].type === 'video') {
      newPostMediaItems = [...newPostMediaItems]
      newPostMediaItems[oldIndex].playing = false
      newPostMediaItems[oldIndex].canPlayOnScroll = false
    }

    if (newPostMediaItems !== postMediaItems) setPostMediaItems(newPostMediaItems)
  }, [postMediaItems])

  const sliderAppendDots = useCallback((dots) => (
    <MagicSliderDots dots={dots} numDotsToShow={isMobile.any ? 5 : 10} dotWidth={20} />
  ), [isMobile.any])

  return (
    <div ref={wrapper} className='PostMediaContent'>
      <Slider
        {...sliderSettings}
        appendDots={sliderAppendDots}
        beforeChange={beforeSlickChange}
      >
        {postMediaItems.map((mediaItem) => {
          switch (mediaItem.type) {
            case PostMediaItemTypes.PHOTO:
              return (
                <PostMediaPhotoItem
                  mediaItem={mediaItem as ExtendedPostMediaPhotoItem}
                  post={post}
                  aspectRatio={firstMediaItemAspectRatio}
                  key={mediaItem._id}
                />
              )
            case PostMediaItemTypes.VIDEO:
              return (
                <PostMediaVideoItem
                  mediaItem={mediaItem as ExtendedPostMediaVideoItem}
                  post={post}
                  aspectRatio={firstMediaItemAspectRatio}
                  videoProps={videoProps}
                  key={mediaItem._id}
                />
              )
            default:
              return null
          }
        })}
      </Slider>
    </div>
  )
})
