import React, { useCallback } from 'react'
import { PostStatus, PostType } from '@models/Posts'
import PinIcon from '@icons/pin.svg'
import PencilIcon from '@icons/pencil.svg'
import PublishIcon from '@icons/publish.svg'
import UnpublishIcon from '@icons/unpublish.svg'
import TrashIcon from '@icons/trash.svg'
import ReportIcon from '@icons/report.svg'
import { DropdownMenu, DropdownMenuOptionType } from '@components/DropdownMenu/DropdownMenu'

interface PostMenuProps {
  post: PostType
  isPromo?: boolean
  isOwn?: boolean
  isNoPin?: boolean
}

export const PostMenu = React.memo(({
  post,
  isPromo = false,
  isOwn = false,
  isNoPin = false,
}: PostMenuProps) => {
  const isPublished = post.status === PostStatus.PUBLISHED

  const openReportDialog = useCallback(() => {}, [])
  const openPinAsPromoDialog = useCallback(() => {}, [])
  const editPost = useCallback(() => {}, [])
  const unpublishPost = useCallback(() => {}, [])
  const publishPost = useCallback(() => {}, [])
  const openDeletePostDialog = useCallback(() => {}, [])

  const options = (
    isOwn ? [
      !isPromo && !isNoPin && {
        label: 'pin as promo',
        icon: PinIcon,
        onClick: openPinAsPromoDialog,
      },
      {
        label: 'edit post',
        icon: PencilIcon,
        onClick: editPost,
      },
      isPublished ? {
        label: 'unpublish',
        icon: UnpublishIcon,
        onClick: unpublishPost,
      } : {
        label: 'publish now',
        icon: PublishIcon,
        onClick: publishPost,
      },
      {
        label: 'delete post',
        icon: TrashIcon,
        onClick: openDeletePostDialog,
      },
    ] : [
      {
        label: 'report post',
        icon: ReportIcon,
        onClick: openReportDialog,
      },
    ]
  ).filter(Boolean) as DropdownMenuOptionType[]

  return <DropdownMenu options={options} className='PostMenu' />
})