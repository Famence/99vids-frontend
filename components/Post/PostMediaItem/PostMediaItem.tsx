import React, { useCallback, useMemo } from 'react'
import { ExtendedPostMediaPhotoItem, ExtendedPostMediaVideoItem, PostType } from '@models/Posts'
import { FileStatus } from '@models/File'
import { Img } from '@components/Img/Img'
import { LockedContentPlug } from '@components/LockedContentPlug/LockedContentPlug'
import { AspectRatio, useStore } from '@root/utils'
import './PostMediaItem.sass'
import { Video, VideoProps } from '@components/Video/Video'
import { PhotoPaths, VideoPaths } from '@models/MediaFile'

interface PostMediaPhotoItemProps {
  mediaItem: ExtendedPostMediaPhotoItem
  post: PostType
  aspectRatio: AspectRatio
}

export const PostMediaPhotoItem: React.FunctionComponent<PostMediaPhotoItemProps> = React.memo(({
  mediaItem,
  post,
  aspectRatio,
}) => {
  const store = useStore()

  const src = useMemo(() => {
    if (post.demo && mediaItem.photoInfo.paths?.demo)
      return { full: mediaItem.photoInfo.paths.demo }
    return mediaItem.photoInfo.paths
  }, [])

  const onClick = useCallback(() => {
    if (post.demo) return
    store.dispatch.gallery.open({ postId: post._id, mediaItemId: mediaItem._id })
  }, [post.demo, post._id, mediaItem._id])

  return (
    <div className='PostMediaItem PostMediaItem-photo'>
      <Img
        src={src}
        isFull
        aspectRatio={aspectRatio}
        renderAfterLoad={post.demo && <LockedContentPlug model={post.author} />}
        onClick={onClick}
      />
    </div>
  );
})

interface PostMediaVideoItemProps {
  mediaItem: ExtendedPostMediaVideoItem
  post: PostType
  aspectRatio: AspectRatio
  videoProps?: Partial<VideoProps>
}

export const PostMediaVideoItem: React.FunctionComponent<PostMediaVideoItemProps> = React.memo(({
  mediaItem,
  post,
  aspectRatio,
  videoProps,
}) => {
  const { status, paths } = mediaItem.videoInfo
  const isVideoStatusInProgress = status === FileStatus.IN_PROGRESS
  const isVideoPreview = post.demo || isVideoStatusInProgress

  const src = useMemo(() => {
    if (isVideoStatusInProgress)
      return mediaItem.coverInfo.paths.full || mediaItem.coverInfo.paths.thumb
    return paths
  }, [paths, isVideoStatusInProgress])

  const videoPreviewSource = useMemo<PhotoPaths | null>(() => {
    if (post.demo)
      return { full: mediaItem.coverInfo.paths.demo }
    if (isVideoStatusInProgress)
      return { full: mediaItem.coverInfo.paths.full || mediaItem.coverInfo.paths.thumb }
    return null
  }, [post.demo])

  return (
    <div className='PostMediaItem PostMediaItem-video'>
      {isVideoPreview ? (
        <div>
          <Img
            src={videoPreviewSource}
            isFull
            aspectRatio={aspectRatio}
            renderAfterLoad={post.demo && <LockedContentPlug model={post.author} />}
          />
          {status === 'inProgress' && (
            <div className="post-item__overlay">
              <div className="post-item__overlay-inner">
                <div className="post-item__overlay-title">Video processing</div>
                <div className="post-item__overlay-subtitle">
                  The video in your post is being processed.
                </div>
              </div>
            </div>
          )}
        </div>
      ) : (
        <Video
          src={src as VideoPaths}
          aspectRatio={aspectRatio}
          playing={mediaItem.playing}
          muted
          {...(videoProps || {})}
        />
      )}
    </div>
  );
})
