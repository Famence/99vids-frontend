import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import cn from 'classnames'
import { PostType } from '@models/Posts'
import { User } from '@models/Users'
import { getPostDate } from '@utils/posts'
import { ProfileLink } from '@components/ProfileLink/ProfileLink'
import { currentUserSelector } from '@store/Users/selectors'
import { PostMenu } from '@components/Post/PostMenu'
import { PostMediaContent } from '@components/Post/PostMediaContent/PostMediaContent'
import { PostNotice } from '@components/Post/PostNotice'
import { VideoProps } from '@components/Video/Video'
import { PostFooter } from '@components/Post/PostFooter/PostFooter'
import './Post.sass'
import { SubscribeButton } from '@components/SubscribeButton/SubscribeButton'

export interface PostProps {
  post: PostType<User>
  topContent?: React.ReactElement
  beforePost?: React.ReactElement
  beforeBody?: React.ReactElement
  isRecommended?: boolean
  noRecommendedDesign?: boolean
  isPinned?: boolean
  insideModal?: boolean
  displayPostMenu?: boolean
  displayPostFooter?: boolean
  className?: string
  videoProps?: Partial<VideoProps>
}

export const Post = React.memo(({
  post,
  isPinned = false,
  isRecommended = false,
  noRecommendedDesign = false,
  beforePost,
  topContent,
  beforeBody,
  insideModal = false,
  displayPostMenu = true,
  displayPostFooter = true,
  className,
  videoProps,
}: PostProps): React.ReactElement => {
  const {
    status,
    media,
    isPromo,
    liked,
    subscribed,
    demo,
    author,
    moderated,
  } = post

  const [isCommentsOpened, setIsCommentsOpened] = useState(false)
  const currentUser = useSelector(currentUserSelector)
  const isUserLoggedIn = Boolean(currentUser)
  const isOwn = isUserLoggedIn && currentUser?._id === author._id

  const postClass = insideModal && cn({
    isPromo: isOwn && isPromo && !isRecommended && moderated,
    promoOnModeration: isOwn && isPromo && !moderated,
    isPinned,
  })

  const hasMedia = post.media && (post.media.photo.length || post.media.video.length)

  return (
    <>
      <PostNotice post={post} />
      <div className={cn('Post', { [postClass as string]: postClass }, post.status, className)}>
        {beforePost}
        <div className='Post-top'>
          <ProfileLink
            user={author}
            nick={getPostDate(post)}
            size='sm'
            isRecommended={isRecommended && !noRecommendedDesign}
          />
          {topContent}
          {isRecommended && <SubscribeButton model={author} small />}
          {isUserLoggedIn && !isRecommended && displayPostMenu && (
            <PostMenu
              post={post}
              isPromo={isPromo}
              isOwn={isOwn}
              isNoPin={['deferred', 'unpublished'].includes(status)}
            />
          )}
        </div>
        {beforeBody}
        <div className='Post-body'>
          <div className='Post-text'>{post.text}</div>
        </div>
        {hasMedia && <PostMediaContent post={post} videoProps={videoProps} />}
        {displayPostFooter && (
          <>
            {!insideModal && (
              <PostFooter
                post={post}
                isCommentsOpened={isCommentsOpened}
                setIsCommentsOpened={setIsCommentsOpened}
              />
            )}
            {/* <PostComments post={post} isOpened={isCommentsOpened} /> */}
          </>
        )}
      </div>
    </>
  )
})
