import React, { useCallback } from 'react'
import { useSelector } from 'react-redux'
import cn from 'classnames'
import { PostType } from '@models/Posts'
import { UserRoles } from '@models/Users'
import { currentUserRoleSelector, currentUserSelector } from '@store/Users/selectors'
import { TipButton } from '@components/TipButton/TipButton'
import { Button } from '@components/Button/Button'
import IconLike from '@icons/like.svg'
import IconMessage from '@icons/message.svg'
import IconTips from '@icons/tips.svg'
import { useStore } from '@root/utils'
import './PostFooter.sass'

interface PostFooterProps {
  post: PostType
  isCommentsOpened: boolean
  setIsCommentsOpened: (isOpened: boolean) => void
}

export const PostFooter: React.FunctionComponent<PostFooterProps> = React.memo(({
  post,
  isCommentsOpened,
  setIsCommentsOpened,
}) => {
  const store = useStore()
  const currentUser = useSelector(currentUserSelector)
  const isOwn = currentUser && currentUser._id === post.author._id
  const currentUserRole = useSelector(currentUserRoleSelector)

  const isSubscribed = post.subscribed
  const isLiked = post.liked
  const { likesCount, commentsCount } = post.statistic

  const requestSubscription = useCallback((onSubscribe) => {
    store.dispatch.users.subscribe({
      modelId: post.author._id,
      onSubscribe,
    })
  }, [post])

  const onClickLike = useCallback(() => store.dispatch.posts.toggleLike(post._id), [post._id])

  const canOpenComments = (currentUserRole === UserRoles.MODEL && isOwn) || isSubscribed || post.isPromo

  const toggleCommentsOpened = useCallback(() => {
    if (isCommentsOpened)
      setIsCommentsOpened(false)
    else
      setIsCommentsOpened(true)
  }, [isCommentsOpened, setIsCommentsOpened])

  const onClickComments = useCallback(() => {
    if (canOpenComments)
      toggleCommentsOpened()
    else
      requestSubscription(() => toggleCommentsOpened())
  }, [canOpenComments, toggleCommentsOpened, requestSubscription])

  return (
    <div className='PostFooter no-user-select'>
      <button
        type='button'
        onClick={onClickLike}
        className={cn('PostFooter__item PostFooter__likes', { isLiked })}
      >
        <IconLike isFilled={isLiked} isInline />
        <span>{likesCount || 0}</span>
      </button>
      <button
        type='button'
        onClick={onClickComments}
        className={cn('PostFooter__item PostFooter__comments', { isOpened: isCommentsOpened })}
      >
        <IconMessage isFilled={isCommentsOpened} isInline />
        <span>{commentsCount || 0}</span>
      </button>
      {/* {displayViews && ( */}
      {/*   <div className='PostFooter__item PostFooter__views'> */}
      {/*     <IconEye isInline /> */}
      {/*     <span>{viewsCount || 0}</span> */}
      {/*   </div> */}
      {/* )} */}
      {currentUserRole === UserRoles.FAN && (
        <TipButton model={post.author} post={post} className='PostFooter__item PostFooter__tipButton'>
          <IconTips isInline />
          <span>SEND TIP</span>
        </TipButton>
      )}
    </div>
  )
})
