import React from 'react'
import Routes from '@routes/config'
import { AppRoute } from '@models/commonTypes'
import { UserRoles } from '@models/Users'
import IconDotsBurger from '@icons/dots-burger.svg'
import IconChat from '@icons/chat.svg'
import IconRecommended from '@icons/recommended.svg'
import IconTwoUsers from '@icons/two-users.svg'
import IconStar from '@icons/star.svg'
import IconSupport from '@icons/support.svg'
import IconHighHeels from '@icons/high-heels.svg'
import IconCheckBadge from '@icons/check-badge.svg'
import IconWallet from '@icons/wallet.svg'
import IconPen from '@icons/pen.svg'
import { RootState } from '@store/store'
import { SvgIcon } from '*.svg'

interface NavigationMenuItem {
  route?: AppRoute
  href?: string
  target?: string
  label: React.ReactNode
  icon: SvgIcon
  getCounter?(state: RootState): number
  className?: string
}

type NavigationMenuConfig = {
  [userRole in UserRoles]: NavigationMenuItem[]
}

const menuItems: Record<string, NavigationMenuItem> = {
  feed: {
    route: Routes.findByName('feed'),
    label: 'Feed',
    icon: IconDotsBurger,
  },
  myPage: {
    route: Routes.findByName('modelPage'),
    label: 'My page',
    icon: IconDotsBurger,
  },
  chat: {
    route: Routes.findByName('chat'),
    label: 'Chat',
    icon: IconChat,
    // getCounter: (state) => newMessageCountSelector(state),
  },
  message: {
    route: Routes.findByName('chat'),
    label: 'Message',
    icon: IconChat,
    // getCounter: (state) => newMessageCountSelector(state),
  },
  recommended: {
    route: Routes.findByName('recommended'),
    label: 'Recommended',
    icon: IconRecommended,
  },
  following: {
    route: Routes.findByName('following'),
    label: 'Following',
    icon: IconTwoUsers,
  },
  favorites: {
    route: Routes.findByName('favorites'),
    label: 'Favorites',
    icon: IconStar,
  },
  fans: {
    route: Routes.findByName('fans'),
    label: 'Fans',
    icon: IconCheckBadge,
  },
  earnings: {
    route: Routes.findByName('earnings'),
    label: 'Earnings & Payouts',
    icon: IconWallet,
  },
  editProfile: {
    route: Routes.findByName('editProfile'),
    label: 'Edit profile',
    icon: IconPen,
  },
  support: {
    route: Routes.findByName('support'),
    label: 'Support',
    icon: IconSupport,
  },
  becomeAModel: {
    href: 'https://model.99vids.com',
    label: 'For models',
    icon: IconHighHeels,
  },
}

export const navigationMenuConfig: NavigationMenuConfig = {
  [UserRoles.GUEST]: [
    menuItems.recommended,
    menuItems.feed,
    menuItems.chat,
    menuItems.following,
    menuItems.favorites,
    menuItems.support,
    menuItems.becomeAModel,
  ],
  [UserRoles.FAN]: [
    menuItems.feed,
    menuItems.chat,
    menuItems.recommended,
    menuItems.following,
    menuItems.favorites,
    menuItems.support,
  ],
  [UserRoles.MODEL]: [
    menuItems.myPage,
    menuItems.message,
    menuItems.fans,
    menuItems.earnings,
    menuItems.editProfile,
    menuItems.support,
  ],
  [UserRoles.MODERATOR]: [],
  [UserRoles.ADMIN]: [],
}
