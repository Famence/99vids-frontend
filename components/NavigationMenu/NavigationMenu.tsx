import React from 'react'
import { useRouter } from 'next/router'
import { useSelector } from 'react-redux'
import cn from 'classnames'
import { UserRoles } from '@models/Users'
import { currentUserRoleSelector } from '@store/Users/selectors'
import { LinkWithA } from '@components/Link/LinkWithA'
import { useStore } from '@utils'
import { navigationMenuConfig } from './config'
import './NavigationMenu.sass'

interface NavigationMenuProps {
  userRole?: UserRoles
}

export const NavigationMenu: React.FunctionComponent<NavigationMenuProps> = React.memo(({ userRole }) => {
  const store = useStore()
  const state = store.getState()
  const router = useRouter()
  const currentUserRole = useSelector(currentUserRoleSelector)
  const displayForRole = userRole || currentUserRole

  return (
    <ul className='NavigationMenu'>
      {navigationMenuConfig[displayForRole].map((menuItem) => {
        const counterValue = menuItem.getCounter?.(state)
        const linkContent = (
          <>
            <div className='NavigationMenuItem__icon'>
              <menuItem.icon isFilled />
              {counterValue && (
                <div className='NavigationMenuItem__counter'>{counterValue}</div>
              )}
            </div>
            <span className='NavigationMenuItem__label'>{menuItem.label}</span>
          </>
        )
        return (
          <li
            className={cn(
              'NavigationMenuItem',
              menuItem.className,
              { isActive: router.route === menuItem.route?.page }
            )}
            key={menuItem.route?.name || menuItem.href}
          >
            {menuItem.route ? (
              <LinkWithA route={menuItem.route.name}>
                {linkContent}
              </LinkWithA>
            ) : (
              <a href={menuItem.href} target={menuItem.target || '_blank'}>
                {linkContent}
              </a>
            )}
          </li>
        )
      })}
    </ul>
  )
})
