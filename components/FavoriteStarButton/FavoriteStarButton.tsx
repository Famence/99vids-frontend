import React, { useCallback, useMemo } from 'react'
import { useSelector } from 'react-redux'
import cn from 'classnames'
import { User } from '@models/Users'
import IconStar from '@icons/star.svg'
import { useStore } from '@utils'
import { favoriteUsersSelector } from '@store/Favorites/selectors'
import './FavoriteStarButton.sass'

interface FavoritesStarButtonProps {
  user: User
  className?: string
}

export const FavoriteStarButton: React.FunctionComponent<FavoritesStarButtonProps> = React.memo(({
  user,
  className,
}) => {
  const store = useStore()
  const favoriteUsers = useSelector(favoriteUsersSelector)

  // TODO: Заменить на user.isFavorite когда будет готово на бэке
  const isFavorite = useMemo(() => (
    favoriteUsers && favoriteUsers.find((favorite: User) => favorite._id === user._id)
  ), [user, favoriteUsers])

  const onClick = useCallback(() => {
    store.dispatch.users.addToFavorites({ userId: user._id })
  }, [user._id])

  return (
    <button
      title={isFavorite ? 'Remove from favorites' : 'Add to favorites'}
      className={cn('FavoriteStarButton', className)}
      onClick={onClick}
      type='button'
    >
      <IconStar isFilled={isFavorite} />
    </button>
  )
})
