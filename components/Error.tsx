import React from 'react'
import { AccessError, ServerError } from '@models/errors'
import { getErrorMessage } from '@utils/helpers'

export const ErrorComponent = (error: ServerError | AccessError) => (
  <div className='Error'>
    <h2>Error{typeof error.statusCode === 'number' && ` ${error.statusCode}`}!</h2>
    <pre>{getErrorMessage(error)}</pre>
  </div>
)
