import React, { useCallback, useState } from 'react'
import { Modal, ModalProps } from '@components/Modal/Modal'
import { Editor } from '@components/Editor/Editor'
import { Button } from '@components/Button/Button'
import { isLess } from '@utils'
import './AvatarEditorModal.scss'

interface Props extends ModalProps {
  file: File
  onSubmit: () => void
  onCancel?: () => void
}

export const AvatarEditorModal: React.FunctionComponent<Props> = React.memo(({
  file,
  onSubmit: _onSubmit,
  onCancel,
  ...modalProps
}) => {
  const [step, setStep] = useState(1)
  const [firstImg, setFirstImg] = useState([])

  const onRequestClose: ModalProps['onRequestClose'] = useCallback((event) => {
    if (modalProps.onRequestClose) modalProps.onRequestClose(event)
    if (onCancel) onCancel()
  }, [modalProps.onRequestClose, onCancel])

  const goToStep2 = useCallback(() => setStep(2), [])

  const onSubmit = useCallback(() => {
    _onSubmit()
    if (modalProps.onRequestClose) modalProps.onRequestClose()
  }, [_onSubmit])

  return (
    <Modal
      className='modelAvatarUploadDialog'
      {...modalProps}
      onRequestClose={onRequestClose}
    >

      <div className="popup popup--white">
        <div className="popup-ava">
          {step === 1 && (
            <div className="popup-ava__title">Install avatar thumbnail photo</div>
          )}
          {step === 2 && (
            <div className="popup-ava__title">The selected area will be displayed on your page</div>
          )}
          <div className="popup-ava__description">
            Photo should be NON NUDE
          </div>
          <div className="popup-ava__holder">
            {step === 1 && (
              <Editor
                img={file}
                width={isLess(480) ? 204 : 334}
                height={isLess(480) ? 132 : 334}
                initScale={1}
                borderRadius={500}
                border={isLess(480) ? [36, 0, 36, 0] : [0, 0, 0, 0]}
              />
            )}
            {step === 2 && (
              <Editor
                img={file}
                width={isLess(480) ? 204 : 490}
                height={isLess(480) ? 132 : 334}
                initScale={1}
                borderRadius={5}
                border={isLess(480) ? [50, 0, 50, 0] : [134, 5, 134, 5]}
              />
            )}
            <div className="popup-ava__ctrl">
              <div className="popup-ava__ctrl-item">
                <Button small onClick={onRequestClose}>Cancel</Button>
              </div>
              <div className="popup-ava__ctrl-item">
                {step === 1 && (
                  <Button small onClick={goToStep2}>next</Button>
                )}
                {step === 2 && (
                  <Button small onClick={onSubmit}>save</Button>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  )
})
