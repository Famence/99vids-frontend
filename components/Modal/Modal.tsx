import React from 'react'
import ReactModal from 'react-modal'
import cn from 'classnames'
import CrossIcon from '@icons/cross.svg'
import './Modal.sass'

export enum ModalTheme {
  Purple = 'purple',
  Light = 'light',
}

export interface ModalProps extends ReactModal.Props {
  overlayClassName?: string
  theme: ModalTheme
  className?: string
}

export const Modal: React.FunctionComponent<ModalProps> = React.memo(({
  className,
  overlayClassName,
  theme = ModalTheme.Purple,
  children,
  ...props
}) => (
  <ReactModal
    {...props}
    overlayClassName={cn('Modal__overlay', overlayClassName )}
    className='Modal__container'
  >
    <div className={cn('Modal', `theme-${theme}`, className)}>
      {children}
      <div className='Modal__close'>
        <CrossIcon onClick={props.onRequestClose} />
      </div>
    </div>
  </ReactModal>
))
