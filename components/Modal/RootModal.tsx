import React from 'react'
import { useSelector } from 'react-redux'
import { openedModalsSelector } from '@store/Modals/selectors'
import { ModalTypes } from '@store/Modals/constants'
import { AuthenticateModal } from '@components/Modal/AuthenticateModal/AuthenticateModal'
import { SelectTariffModal } from '@components/Modal/SelectTariffModal/SelectTariffModal'
import { SubscribeCongratsModal } from '@components/Modal/SubscribeCongratsModal/SubscribeCongratsModal'
import { ModelAvatarUploadDialog } from '@components/Modal/ModelAvatarUploadDialog/ModelAvatarUploadDialog'
import { AvatarEditorModal } from '@components/Modal/AvatarEditorModal/AvatarEditorModal'
import {
  ConfirmUpgradeSubscriptionModal
} from '@components/Modal/ConfirmUpgradeSubscriptionModal/ConfirmUpgradeSubscriptionModal'
import { TipModal } from '@components/Modal/TipModal/TipModal'
import { useStore } from '@utils'

const modalComponentsByType = {
  [ModalTypes.Authenticate]: AuthenticateModal,
  [ModalTypes.SelectTariff]: SelectTariffModal,
  [ModalTypes.SubscribeCongrats]: SubscribeCongratsModal,
  [ModalTypes.ModelAvatarUploadDialog]: ModelAvatarUploadDialog,
  [ModalTypes.AvatarEditor]: AvatarEditorModal,
  [ModalTypes.ConfirmUpgradeSubscription]: ConfirmUpgradeSubscriptionModal,
  [ModalTypes.Tip]: TipModal,
}

export const RootModal: React.FunctionComponent = React.memo(() => {
  const store = useStore()
  const openedModals = useSelector(openedModalsSelector)

  return (
    <>
      {Object.values(openedModals).map(({ type, ...openedModalProps }) => {
        const ModalComponent = modalComponentsByType[type]
        const onRequestClose = openedModalProps.onRequestClose || (() => store.dispatch.modals.close(type))
        return ModalComponent ? (
          <ModalComponent isOpen {...openedModalProps} onRequestClose={onRequestClose} key={type} />
        ) : null
      })}
    </>
  )
})
