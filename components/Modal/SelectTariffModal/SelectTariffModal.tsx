import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { useSelector } from 'react-redux'
import { User } from '@models/Users'
import { TariffType } from '@models/TariffType'
import { Subscription, SubscriptionStatus } from '@models/Subscription'
import { ModalTypes } from '@store/Modals/constants'
import { Modal, ModalProps } from '@components/Modal/Modal'
import { Avatar } from '@components/Avatar/Avatar'
import { Tariff } from '@components/Tariff/Tariff'
import { Loader } from '@components/Loader/Loader'
import { currentUserSelector } from '@store/Users/selectors'
import { useStore } from '@utils'
import { api } from '@api'
import './SelectTariffModal.sass'

interface SelectTariffModalProps extends ModalProps {
  model: User
  onSubmit: (tariff: TariffType) => void
}

export const SelectTariffModal: React.FunctionComponent<SelectTariffModalProps> = React.memo(({
  model,
  onSubmit,
  ...modalProps
}) => {
  const store = useStore()
  const currentUser = useSelector(currentUserSelector)
  const [currentSubscription, setCurrentSubscription] = useState<Subscription | null>()
  const [selectedTariff, setSelectedTariff] = useState<TariffType | null>(null)

  const isCurrentSubscriptionActive = useMemo(() => (
    currentSubscription?.status === SubscriptionStatus.Active
  ), [currentSubscription])

  const isSubscribeMode = !currentSubscription || currentSubscription.status === SubscriptionStatus.Deleted

  const submitTariff = useCallback((tariff: TariffType) => {
    setSelectedTariff(tariff)
    onSubmit(tariff)
  }, [onSubmit])

  const handleClickSelectTariff = useCallback((tariff: TariffType) => () => {
    if (isSubscribeMode)
      submitTariff(tariff)
    else {
      store.dispatch.modals.open({
        type: ModalTypes.ConfirmUpgradeSubscription,
        tariff,
        onConfirm: () => submitTariff(tariff),
        onCancel: () => setSelectedTariff(null),
      })
    }
  }, [isSubscribeMode, onSubmit])

  useEffect(() => {
    if (!currentUser) return
    api.getSubscription({ fanId: currentUser._id, modelId: model._id })
      .then(({ data: subscription }) => setCurrentSubscription(subscription))
      .catch(() => setCurrentSubscription(null))
  }, [currentUser, model])

  return (
    <Modal className='SelectTariffModal' {...modalProps}>
      <div className='SelectTariffModal__title'>
        <Avatar user={model} />
        <div className='SelectTariffModal__title-text'>Get access to feed and chat of {model.displayName}</div>
      </div>
      <div className='SelectTariffModal__row'>
        {typeof currentSubscription !== 'undefined' ? model.tariffs.map((tariff) => (
          <Tariff
            tariff={tariff}
            isSubscribed={currentSubscription?.tariffId === tariff._id && isCurrentSubscriptionActive}
            onClickSelect={handleClickSelectTariff(tariff)}
            isSubscribeMode={isSubscribeMode}
            isLoading={selectedTariff?._id === tariff._id}
            key={tariff._id}
          />
        )) : <Loader white />}
      </div>
      <div className='SelectTariffModal__footer'>
        Tariffs include: Access to Posts and Private Messages
      </div>
    </Modal>
  )
})
