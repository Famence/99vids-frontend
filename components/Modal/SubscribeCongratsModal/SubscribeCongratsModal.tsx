import React from 'react'
import { User } from '@models/Users'
import { Modal, ModalProps } from '@components/Modal/Modal'
import { Avatar } from '@components/Avatar/Avatar'
import { Img } from '@components/Img/Img'
import { Button, ButtonTheme } from '@components/Button/Button'
import './SubscribeCongratsModal.sass'

interface SubscribeCongratsModalProps extends ModalProps {
  model: User
}

export const SubscribeCongratsModal: React.FunctionComponent<SubscribeCongratsModalProps> = React.memo(({
  model,
  ...modalProps
}) => (
  <Modal className='SubscribeCongratsModal' {...modalProps}>
    <div className="SubscribeCongratsModal__content">
      <div className="SubscribeCongratsModal__content-left">
        <h3 className='SubscribeCongratsModal__title'>Congratulations!</h3>
        {model.photoByUser && model.photoByUser[0] && (
          <Avatar
            user={model}
            className='SubscribeCongratsModal__avatar'
          />
        )}
        <p className='SubscribeCongratsModal__text'>You are subscribed to <b>@{model.nickname}</b> successfully.</p>
        <img
          className='SubscribeCongratsModal__image'
          src='/assets/img/congrats-on-subscribe.png'
          alt='Congrats on subscribe'
        />
        <Button
          onClick={modalProps.onRequestClose}
          theme={ButtonTheme.LIGHT}
          className='SubscribeCongratsModal__okButton'
        >
          OK
        </Button>
      </div>
      <div className="SubscribeCongratsModal__content-right">
        {model.photoByUser && model.photoByUser[0] && (
          <div className='SubscribeCongratsModal__modelCard'>
            <Img src={model.photoByUser[0].paths} />
            <div className='SubscribeCongratsModal__modelCard-name'>{model.firstname || (`@${model.nickname}`)}</div>
          </div>
        )}
      </div>
    </div>
  </Modal>
))
