import React, { useEffect } from 'react'
import { FormikConfig } from 'formik'
import { User } from '@models/Users'
import { Modal, ModalProps } from '@components/Modal/Modal'
import { UserIconWithName } from '@components/UserIconWithName/UserIconWithName'
import { useStore } from '@utils'
import { TipForm, TipFormValues } from '@components/Form/TipForm/TipForm'
import './TipModal.sass'

interface TipModalProps extends ModalProps {
  model: User
  onSubmit: FormikConfig<TipFormValues>['onSubmit']
}

export const TipModal: React.FunctionComponent<TipModalProps> = React.memo(({
  model,
  onSubmit,
  ...modalProps
}) => {
  const store = useStore()

  useEffect(() => { store.dispatch.references.getMinTip() }, [])

  return (
    <Modal className='TipModal' {...modalProps}>
      <div className='TipModal__title'>
        <div className='TipModal__title-text'>Send a tip to</div>
        <UserIconWithName user={model} />
        <TipForm onSubmit={onSubmit} />
      </div>
    </Modal>
  )
})
