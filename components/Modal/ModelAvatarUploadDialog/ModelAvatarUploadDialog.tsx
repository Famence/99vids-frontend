import React, { useCallback, useMemo, useRef } from 'react'
import { Modal, ModalProps } from '@components/Modal/Modal'
import { DropZone } from '@components/DropZone/DropZone'
import IconAllow from '@icons/allow.svg'
import IconProhibit from '@icons/prohibit.svg'
import './ModelAvatarUploadDialog.scss'

interface Props extends ModalProps {
  onImageSelect: (file: File) => void
  onCancel?: () => void
}

export const ModelAvatarUploadDialog: React.FunctionComponent<Props> = React.memo(({
  onImageSelect: _onImageSelect,
  onCancel,
  ...modalProps
}) => {
  const fileInput = useRef<HTMLElement>(null)

  const onMobileButtonClick = useMemo(() => (
    () => fileInput.current?.click()
  ), [fileInput])

  const onRequestClose: ModalProps['onRequestClose'] = useCallback((event) => {
    if (modalProps.onRequestClose) modalProps.onRequestClose(event)
    onCancel()
  }, [modalProps.onRequestClose, onCancel])

  const onImageSelect = useCallback((files) => {
    _onImageSelect(files[0])
    if (modalProps.onRequestClose) modalProps.onRequestClose()
  }, [_onImageSelect])

  return (
    <Modal
      className='modelAvatarUploadDialog'
      {...modalProps}
      onRequestClose={onRequestClose}
    >
      <h3 className='heading'>Upload a NON NUDE photo for Avatar</h3>
      <div className='modelAvatarUploadDialog__content'>
        <div className='examples'>
          <div className='examples__titles'>
            <div>
              <IconAllow isFilled isInline />
              <span>ALLOWED</span>
            </div>
            <div>
              <IconProhibit isFilled isInline />
              <span>NOT ALLOWED</span>
            </div>
          </div>
          <div className='examples__photos customScrollBar'>
            <img src="/assets/img/allowed-not-allowed/11.jpeg" alt='' />
            <img src="/assets/img/allowed-not-allowed/1.jpeg" alt='' />
            <img src="/assets/img/allowed-not-allowed/2.jpeg" alt='' />
            <img src="/assets/img/allowed-not-allowed/3.jpeg" alt='' />
            <img src="/assets/img/allowed-not-allowed/4.jpeg" alt='' />
            <img src="/assets/img/allowed-not-allowed/5.jpeg" alt='' />
            <img src="/assets/img/allowed-not-allowed/6.jpeg" alt='' />
            <img src="/assets/img/allowed-not-allowed/7.jpeg" alt='' />
            <img src="/assets/img/allowed-not-allowed/8.jpeg" alt='' />
            <img src="/assets/img/allowed-not-allowed/9.jpeg" alt='' />
            <img src="/assets/img/allowed-not-allowed/10.jpeg" alt='' />
            <img src="/assets/img/allowed-not-allowed/12.jpeg" alt='' />
          </div>
        </div>
        <DropZone
          className='dropZone'
          accept='image/jpeg, image/png'
          onDrop={onImageSelect}
          fileInputRef={fileInput}
        >
          <svg className='icon' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 65 60" fill="#fff">
            <path
              d="M52.3914 16.0285C50.2003 5.24015 39.5121 -1.76215 28.5185 0.388036C20.4777 1.96072 14.1905 8.12665 12.5829 16.0163C4.78814 16.8979 -0.803185 23.8132 0.0946346 31.463C0.921183 38.5032 6.99755 43.8147 14.2192 43.8099H24.3674C25.4885 43.8099 26.3972 42.9181 26.3972 41.818C26.3972 40.7179 25.4885 39.8262 24.3674 39.8262H14.2192C8.61402 39.7936 4.0977 35.3087 4.13037 29.8087C4.16353 24.3082 8.73379 19.8757 14.339 19.9082C15.3734 19.9082 16.2425 19.1447 16.3563 18.1354C17.3868 9.39384 25.4439 3.12688 34.3523 4.13811C41.8417 4.98808 47.7498 10.7858 48.616 18.1354C48.7862 19.1627 49.693 19.915 50.7531 19.9082C56.3583 19.9082 60.9018 24.3669 60.9018 29.8674C60.9018 35.3675 56.3583 39.8262 50.7531 39.8262H40.6049C39.4839 39.8262 38.5751 40.7179 38.5751 41.818C38.5751 42.9181 39.4839 43.8099 40.6049 43.8099H50.7536C58.5999 43.7618 64.9212 37.4808 64.8727 29.781C64.8282 22.769 59.4838 16.8804 52.3914 16.0285Z" />
            <path
              d="M39.7627 48.1108L34.6239 53.1777V28.0294C34.6239 26.9258 33.7164 26.0312 32.5973 26.0312C31.4777 26.0312 30.5702 26.9258 30.5702 28.0294V53.1777L25.4315 48.1108C24.654 47.3166 23.3703 47.2947 22.5651 48.0616C21.7599 48.8281 21.7376 50.093 22.5152 50.8867C22.532 50.9037 22.5483 50.9198 22.5651 50.9359L31.1619 59.412C31.3497 59.5967 31.5722 59.7434 31.8168 59.8437C32.3156 60.0518 32.8786 60.0518 33.3778 59.8437C33.6225 59.7434 33.8449 59.5967 34.0323 59.412L42.6291 50.9359C43.4343 50.1695 43.4565 48.9046 42.679 48.1108C41.901 47.3171 40.6178 47.2952 39.8126 48.0616C39.7958 48.0777 39.779 48.0938 39.7627 48.1108Z" />
          </svg>
          <span className='text'>Upload your photo here</span>
        </DropZone>
        <button className='btn btn--white mobileUploadButton' onClick={onMobileButtonClick}>
          UPLOAD PHOTO
        </button>
      </div>
    </Modal>
  )
})
