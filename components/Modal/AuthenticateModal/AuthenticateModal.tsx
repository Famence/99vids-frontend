import React from 'react'
import { User } from '@models/Users'
import { Modal, ModalProps } from '@components/Modal/Modal'
import { SignInForm, SignInFormProps } from '@components/Form/SignInForm/SignInForm'
import { Img } from '@components/Img/Img'
import { Logo, LogoTheme } from '@components/Logo/Logo'
import { UserIconWithName } from '@components/UserIconWithName/UserIconWithName'
import { LinkWithA } from '@components/Link/LinkWithA'
import './AuthenticateModal.sass'

export interface AuthenticateModalProps extends ModalProps {
  model?: User
  formProps: SignInFormProps
}

export const AuthenticateModal: React.FunctionComponent<AuthenticateModalProps> = React.memo(({
  model,
  formProps,
  ...props
}) => (
  <Modal className='AuthenticateModal' {...props}>
    <div className='AuthenticateModal__model'>
      <Logo theme={LogoTheme.Light} />
      <Img
        src={model?.backgroundPhoto?.paths || '/assets/img/authorization/aside-1.jpg'}
        className='AuthenticateModal__model-bg'
      />
      {model && <UserIconWithName user={model} />}
    </div>
    <div className='AuthenticateModal__form'>
      <h3>
        {model
          ? 'Log In or Register and get full access to this model’s content:'
          : 'Log In or Register and get full access to models’ content:'}
      </h3>
      <ul className='list'>
        <li>Full access to her private photos and videos.</li>
        <li>Direct message with {model ? 'her' : 'models'}.</li>
        <li>Cancel subscription at anytime.</li>
        {/* <li>Live streaming</li> */}
      </ul>
      <SignInForm {...formProps} buttonText='Log In / Sign Up' />
      <LinkWithA
        route='signUp'
        className='AuthenticateModal__registerAsModelLink'
      >
        Register as a model
      </LinkWithA>
    </div>
  </Modal>
))
