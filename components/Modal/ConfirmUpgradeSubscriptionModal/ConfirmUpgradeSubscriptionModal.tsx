import React, { useCallback } from 'react'
import { Modal, ModalProps, ModalTheme } from '@components/Modal/Modal'
import { Button, ButtonTheme } from '@components/Button/Button'
import './ConfirmUpgradeSubscriptionModal.sass'

interface Props extends ModalProps {
  onConfirm: () => void
  onCancel: () => void
}

export const ConfirmUpgradeSubscriptionModal: React.FunctionComponent<Props> = React.memo(({
  onConfirm: _onConfirm,
  onCancel,
  ...modalProps
}) => {
  const onRequestClose: ModalProps['onRequestClose'] = useCallback((event) => {
    if (modalProps.onRequestClose) modalProps.onRequestClose(event)
    onCancel()
  }, [modalProps.onRequestClose, onCancel])

  const onConfirm = useCallback((event) => {
    _onConfirm()
    if (modalProps.onRequestClose) modalProps.onRequestClose(event)
  }, [_onConfirm, onRequestClose])

  return (
    <Modal
      className='ConfirmUpgradeSubscriptionModal'
      {...modalProps}
      theme={ModalTheme.Light}
      onRequestClose={onRequestClose}
    >
      <h3>Are you sure want to change your tariff?</h3>
      <div className='Modal__footer'>
        <Button
          className='ConfirmUpgradeSubscriptionModal__button-cancel'
          onClick={onRequestClose}
          theme={ButtonTheme.LIGHT}
        >
          Cancel
        </Button>
        <Button
          className='ConfirmUpgradeSubscriptionModal__button-confirm'
          onClick={onConfirm}
        >
          Confirm
        </Button>
      </div>
    </Modal>
  )
})
