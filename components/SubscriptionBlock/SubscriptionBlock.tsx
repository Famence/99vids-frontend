import React, { useMemo } from 'react'
import { useSelector } from 'react-redux'
import { User } from '@models/Users'
import { SubscribeButton, SubscribeButtonMode, SubscribeButtonProps } from '@components/SubscribeButton/SubscribeButton'
import { findById } from '@utils'
import { followingUsersSelector } from '@store/Followings/selectors'

interface SubscriptionBlockProps extends SubscribeButtonProps {
  model: User
  canUnsubscribe?: boolean
  className?: string
}

export const SubscriptionBlock: React.FunctionComponent<SubscriptionBlockProps> = React.memo(({
  model,
  canUnsubscribe = false,
  ...buttonProps
}) => {
  const currentUserSubscriptions = useSelector(followingUsersSelector)

  const modelIsUserSubscriptions = useMemo(() => (
    findById<User>(currentUserSubscriptions, model)
  ), [currentUserSubscriptions, model])

  const isSubscribedToModel = Boolean(modelIsUserSubscriptions)

  const buttonMode = isSubscribedToModel ? (
    canUnsubscribe ? SubscribeButtonMode.Unsubscribe : SubscribeButtonMode.Keep
  ) : SubscribeButtonMode.Upgrade

  return (
    <>
      <SubscribeButton
        model={model}
        mode={buttonMode}
        {...buttonProps}
      />
      {buttonMode !== SubscribeButtonMode.Upgrade && (
        <SubscribeButton
          model={model}
          mode={SubscribeButtonMode.Upgrade}
          {...buttonProps}
        />
      )}
    </>
  )
})
