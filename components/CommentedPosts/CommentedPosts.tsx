import React, { useCallback } from 'react'
import { useSelector } from 'react-redux'
import { commentedPostsCountSelector, commentedPostsSelector } from '@store/Favorites/selectors'
import { useLoaderSelector } from '@store/Loaders/selectors'
import { LoaderTypes } from '@store/Loaders/constants'
import { useStore } from '@utils'
import { PostsFeed } from '@components/PostsFeed/PostsFeed'
import { PostDummy } from '@components/PostDummy/PostDummy'

export const CommentedPosts: React.FunctionComponent = React.memo(() => {
  const store = useStore()
  const commentedPosts = useSelector(commentedPostsSelector)
  const commentedPostsCount = useSelector(commentedPostsCountSelector)
  const isLoading = useLoaderSelector(LoaderTypes.CommentedPostsLoader)

  const onCanLoadMore = useCallback(async () => {
    if (typeof commentedPostsCount === 'number' && commentedPostsCount <= commentedPosts.length) return
    await store.dispatch.favorites.getCommentedPosts({ pagination: { offset: commentedPosts.length } })
  }, [commentedPosts.length, commentedPostsCount])

  return (
    <>
      <PostsFeed
        posts={commentedPosts}
        isLoading={isLoading}
        onCanLoadMore={onCanLoadMore}
      />
      {!commentedPosts.length && !isLoading && <PostDummy text='You have not commented posts yet' />}
    </>
  )
})
