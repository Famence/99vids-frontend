import { useMemo } from 'react'
import moment from 'moment'
import { PostsGroup, PostType } from '@models/Posts'
import { User } from '@models/Users'
import { isDateInFuture } from '@utils/date'
import { useSelector } from 'react-redux'
import { postsWithAuthorsSelector } from '@store/complexSelectors'
import { usersPostsSelector } from '@store/Posts/selectors'
import { UserPostsState } from '@store/Posts/model'

export const getPostDate = (post: PostType): string => {
  let res = moment(post.publicationTime).format('MMMM D [at] hh:mm a')

  if (isDateInFuture(post.publicationTime))
    res = `Will be published on ${res}`

  return res
}

const emptyUserPostsState: UserPostsState = {
  group: PostsGroup.All,
  posts: [],
  totalPostsOfGroup: 0,
}

export function useUserPosts(userId: User['_id']) {
  const posts = useSelector(postsWithAuthorsSelector)
  const usersPosts = useSelector(usersPostsSelector)

  const userPostsState = usersPosts[userId] || emptyUserPostsState

  return useMemo<UserPostsState<PostType>>(() => ({
    ...userPostsState,
    posts: userPostsState.posts.map((userPostId) => posts[userPostId]),
  }), [userPostsState, posts])
}
