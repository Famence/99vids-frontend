import { FetchedPostType } from '@models/Posts'
import { StoredPostType } from '@store/Posts/model'

export function convertPostToStoredFormat(fetchedPost: FetchedPostType) {
  const convertedPost = { ...fetchedPost, author: fetchedPost.creatorId }
  delete convertedPost.creatorId
  return convertedPost as StoredPostType
}
