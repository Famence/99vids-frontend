import { ValidationError, validationErrors } from '@utils/validation/constants'
import { MIN_PASSWORD_LENGTH } from '@config/constants'

const passwordValidationErrors = {
  ...validationErrors,
  [ValidationError.MinLength]: `Password must consist at least ${MIN_PASSWORD_LENGTH} symbols`,
}

export const passwordValidation = {
  findError(value: string) {
    if (!value) return passwordValidationErrors[ValidationError.EmptyValue]
    if (value.length < MIN_PASSWORD_LENGTH) return passwordValidationErrors[ValidationError.MinLength]
    return undefined
  },
  validate: (value: string) => !passwordValidation.findError(value),
}
