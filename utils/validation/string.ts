import { ValidationError, validationErrors } from '@utils/validation/constants'

const stringValidationErrors = validationErrors

export const stringValidation = {
  findError(value?: string) {
    if (!value) return stringValidationErrors[ValidationError.EmptyValue]
    return undefined
  },
  validate: (value: string) => !stringValidation.findError(value),
}

export const stringEqualityValidation = {
  findError(value1: string, value2: string) {
    if (value1 !== value2) return stringValidationErrors[ValidationError.NotEqual]
    return undefined
  },
  validate: (value1: string, value2: string) => !stringEqualityValidation.findError(value1, value2),
}
