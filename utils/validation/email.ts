import { emailRegEx, ValidationError, validationErrors } from '@utils/validation/constants'

const emailValidationErrors = {
  ...validationErrors,
  [ValidationError.InvalidValue]: 'This is not a valid email address',
}

export const emailValidation = {
  findError(value: string) {
    if (!value) return emailValidationErrors[ValidationError.EmptyValue]
    if (!emailRegEx.test(value)) return emailValidationErrors[ValidationError.InvalidValue]
    return undefined
  },
  validate: (value: string) => !emailValidation.findError(value),
}
