export enum ValidationError {
  EmptyValue,
  InvalidValue,
  MinLength,
  NotEqual,
}

export const validationErrors = {
  [ValidationError.EmptyValue]: 'Please fill in the field',
  [ValidationError.InvalidValue]: 'Invalid value',
  [ValidationError.NotEqual]: 'Values are not equal',
}

export const emailRegEx = /^[\w%+.-]+@[\d.a-z-]+\.[a-z]{2,}$/i
