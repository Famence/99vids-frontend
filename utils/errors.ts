import { AxiosError } from 'axios'
import { toast } from 'react-toastify'
import { IS_SERVER } from '@config/constants'
import { getErrorMessage } from '@utils/helpers'

class AppError {
  private readonly error!: AxiosError

  constructor(error: AxiosError) {
    this.error = error
  }

  getErrorMessage = () => getErrorMessage(this.error)

  show() {
    // TODO: Проверить работу тостов, вызванных при ssr. Вызвать их в getInitialProps например
    toast.error(this.getErrorMessage())
    if (!IS_SERVER) console.error(this.error)
  }
}

export const showError = (error: AxiosError) => new AppError(error).show()
