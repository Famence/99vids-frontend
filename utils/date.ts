import moment from 'moment'

export const isDateInFuture = (date: string) => moment(date) >= moment().add(1, 'minutes')
