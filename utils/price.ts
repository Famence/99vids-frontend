import _ from 'lodash'

export function roundPlus(x: number, n: number) {
  if (isNaN(x) || isNaN(n))
    return false

  const m = 10**n
  return Math.round(x * m) / m
}

export function numericalOrder(val: number | string) {
  const text = val && typeof val === 'number' ? String(val) : false

  return text ? text.replace(/\B(?=(\d{3})+(?!\d))/g, ' ') : 0
}

export const price = (val: number) => val && _.isFinite(val) ? roundPlus(val / 100, 2) : 0
