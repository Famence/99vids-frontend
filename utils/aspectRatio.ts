import { ExtendedPostMediaItem, ExtendedPostMediaPhotoItem, ExtendedPostMediaVideoItem } from '@models/Posts'

export enum AspectRatio {
  SQUARE = '1:1',
  VERTICAL = '4:5',
  HORIZONTAL = '16:9'
}

export const calculateAspectRatio = (mediaItem: ExtendedPostMediaItem): AspectRatio => {
  if (!mediaItem) return AspectRatio.SQUARE
  let aspectRatioFactor = 1

  if (mediaItem.type === 'video') {
    const videoMediaItem = mediaItem as ExtendedPostMediaVideoItem
    if (videoMediaItem.videoInfo && videoMediaItem.videoInfo.status === 'ready') {
      if (videoMediaItem.videoInfo.metaDataBySize.h480) {
        aspectRatioFactor = (
          videoMediaItem.videoInfo.metaDataBySize.h480.w / videoMediaItem.videoInfo.metaDataBySize.h480.h
        )
      }
    }
  }

  if (mediaItem.type === 'photo') {
    const photoMediaItem = mediaItem as ExtendedPostMediaPhotoItem
    if (photoMediaItem.photoInfo && photoMediaItem.photoInfo.status === 'ready') {
      const metaData = photoMediaItem.photoInfo.metaDataBySize.demo || photoMediaItem.photoInfo.metaDataBySize.thumb
      if (metaData)
        aspectRatioFactor = metaData.w / metaData.h
    }
  }

  if (aspectRatioFactor < 1) return AspectRatio.VERTICAL
  if (aspectRatioFactor > 1) return AspectRatio.HORIZONTAL
  return AspectRatio.SQUARE
}
