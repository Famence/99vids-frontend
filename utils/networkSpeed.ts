// taken from https://github.com/kenigbolo/network-speed
import http from 'http' // TODO: Заменить на что-нибудь. Нодовский модуль не нужно тянуть на фронт
import { VideoQuality } from '@models/MediaFile'

const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~!@#$%^&*()_+`-=[]{}|;\':,./<>?'

export class NetworkSpeed {
  checkDownloadSpeed(baseUrl: string, fileSize: number) {
    let startTime: number
    return new Promise((resolve) => {
      return http.get(baseUrl, (response) => {
        response.once('data', () => {
          startTime = new Date().getTime()
        })

        response.once('end', () => {
          const endTime = new Date().getTime()
          const duration = (endTime - startTime) / 1000
          const bitsLoaded = fileSize * 8
          const bps = (bitsLoaded / duration).toFixed(2)
          const kbps = (+bps / 1024).toFixed(2)
          const mbps = (+kbps / 1024).toFixed(2)
          resolve({ bps, kbps, mbps })
        })
      })
    })
      .catch((error) => {
        throw new Error(error)
      })
  }

  // checkUploadSpeed(options) {
  //   let startTime: number
  //   const data = `{"data": "${  this.generateTestData(20)  }"}`
  //   return new Promise((resolve, _) => {
  //     const req = http.request(options, (res) => {
  //       res.setEncoding('utf8')
  //       res.on('data', () => {
  //         startTime = new Date().getTime()
  //       })
  //       res.on('end', () => {
  //         const endTime = new Date().getTime()
  //         const duration = (endTime - startTime) / 1000
  //         const bitsLoaded = 20 * 8
  //         const bps = (bitsLoaded / duration).toFixed(2)
  //         const kbps = (bps / 1024).toFixed(2)
  //         const mbps = (kbps / 1024).toFixed(2)
  //         resolve({ bps, kbps, mbps })
  //       })
  //     })
  //     req.write(data)
  //     req.end()
  //   })
  //     .catch((error) => {
  //       throw new Error(error)
  //     })
  // }

  generateTestData(sizeInKmb: number) {
    const iterations = sizeInKmb * 1024 // get byte count
    let result = ''
    for (let index = 0; index < iterations; index++)
      result += chars.charAt(Math.floor(Math.random() * chars.length))

    return result
  }
}

export const getVideoQualityByNetworkSpeed = (availableQualities: VideoQuality[], mBts: number) => {
  const availableQualitiesMap = availableQualities.reduce<Record<string, VideoQuality>>((result, item) => ({
    ...result,
    [item]: item,
  }), {})
  if (mBts < 1) {
    return availableQualitiesMap[VideoQuality.h240]
      || availableQualitiesMap[VideoQuality.h360]
  }
  if (mBts < 5) {
    return availableQualitiesMap[VideoQuality.h480]
      || availableQualitiesMap[VideoQuality.h360]
      || availableQualitiesMap[VideoQuality.h240]
  }
  if (mBts < 8) {
    return availableQualitiesMap[VideoQuality.h720]
      || availableQualitiesMap[VideoQuality.h480]
      || availableQualitiesMap[VideoQuality.h360]
      || availableQualitiesMap[VideoQuality.h240]
  }
  if (mBts < 35) {
    return availableQualitiesMap[VideoQuality.h1080]
      || availableQualitiesMap[VideoQuality.h720]
      || availableQualitiesMap[VideoQuality.h480]
      || availableQualitiesMap[VideoQuality.h360]
      || availableQualitiesMap[VideoQuality.h240]
  }
  return availableQualitiesMap[VideoQuality.h2160]
    || availableQualitiesMap[VideoQuality.h1080]
    || availableQualitiesMap[VideoQuality.h720]
    || availableQualitiesMap[VideoQuality.h480]
    || availableQualitiesMap[VideoQuality.h360]
    || availableQualitiesMap[VideoQuality.h240]
}
