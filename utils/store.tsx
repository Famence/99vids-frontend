import { initStore, RootState, Store } from '@store/store'
import { IS_SERVER } from '@config/constants'

let store: Store | undefined

export const initializeStore = (preloadedState?: RootState): Store => {
  let newStore = store ?? initStore(preloadedState)

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    newStore = initStore({
      ...store.getState(),
      ...preloadedState,
    })
    // Reset the current store
    store = undefined
  }

  // For SSG and SSR always create a new store
  if (IS_SERVER) return newStore
  // Create the store once in the client
  if (!store) store = newStore

  return newStore
}

export const useStore = (initialState?: RootState) => initializeStore(initialState)
