import { User, UserRoles } from '@models/Users'
import { AccessError } from '@models/errors/AccessError'
import { UnauthorizedError } from '@models/errors/UnauthorizedError'
import { AppLocation, AppRoute } from '@models/commonTypes'
import { redirect } from '@utils/helpers'
import Routes from '@routes/config'
import { IS_SERVER } from '@config/constants'
import { initializeStore } from '@utils/store'
import { AppContext } from '@models/App'

export type AccessGuard = (user: User | null) => void

interface AccessGuards {
  requireAuthorization: AccessGuard
  allowRole(allowedRoles: UserRoles[], errorConfig?: Partial<AccessError>): AccessGuard
}

export const accessGuards: AccessGuards = {
  requireAuthorization(user) {
    if (!user) throw new UnauthorizedError()
  },
  allowRole: (allowedRoles: string[], errorConfig) => (user) => {
    if (!user && allowedRoles.includes(UserRoles.GUEST)) return
    if (!user || !allowedRoles.includes(user.roleId)) throw new AccessError(errorConfig)
  },
}

export function redirectToPreviousPage(context: AppContext) {
  let redirectTo: AppRoute['name'] | AppLocation = context.router.asPath
  if (!IS_SERVER) {
    const previousRoute = Routes.findAndGetUrls(context.router.asPath)?.route
    if (previousRoute && previousRoute.toPath(context.router.query) !== context.router.asPath) {
      if (Object.keys(context.router.query).length === 0) {
        redirectTo = previousRoute.name
      } else {
        const params = {}
        previousRoute.keyNames.forEach((keyName) => {
          params[keyName] = context.router.query[keyName]
        })
        redirectTo = {
          route: previousRoute,
          params,
        }
      }
    }
  }

  redirect(redirectTo, context.ctx.res)
}

export function requestAuthentication(context: AppContext, onSuccess: () => void) {
  const store = IS_SERVER ? context.ctx.res.locals.store : initializeStore()
  store.dispatch.users.authenticate({ onSuccess })
}
