import React, { Dispatch, SetStateAction, useCallback, useEffect, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Selector } from 'reselect'
import { AxiosError } from 'axios'
import Routes, { Router } from '@routes/config'
import { AnyObject, AppLocation, AppRoute, Entity, Url } from '@models/commonTypes'
import { AccessError, ServerError } from '@models/errors'
import { AppContext } from '@models/App'
import { RootState, Store } from '@store/store'
import { APP_ORIGIN, IS_SERVER } from '@config/constants'
import { SubscriptionStatus } from '@models/Subscription'
import { User } from '@models/Users'
import { followingUsersSelector } from '@store/Followings/selectors'

export const useRematchDispatch = (selector: Selector<RootState, Store>) => selector(useDispatch())

export const redirect = (location: AppRoute['name'] | AppLocation, response?: AppContext['ctx']['res']) => {
  if (!IS_SERVER) {
    const replaceRouteArgs = []
    if (typeof location === 'string') {
      replaceRouteArgs[0] = location
    } else {
      replaceRouteArgs[0] = location.route.name
      replaceRouteArgs[1] = location.params
    }
    Router.replaceRoute(...replaceRouteArgs)
  } else if (response) {
    let targetLocation
    if (typeof location === 'string') {
      targetLocation = Routes.findAndGetUrls(location).urls.as || location
    } else {
      targetLocation = location.route.getAs(location.params)
    }

    response.writeHead(302, {
      Location: targetLocation,
    })
    response.end()
    response.finished = true
  } else {
    throw new Error('Cannot process redirect on server without a `response` as second parameter')
  }
}

export const getTariffMonths = (durationString: string) => Math.round(+durationString.replace('d', '') / 30)

export const getErrorMessage = (error: string | ServerError | AccessError | AxiosError | Error): string => {
  if (typeof error === 'string') return error

  return error?.response?.data?.message || error?.message || 'Неизвестная ошибка'
}

export function arrayToObjectsByKeys<Item = Record<string, any>, ModifiedItem = Item>(
  array: Item[],
  key: string,
  modifier?: (item: Item) => ModifiedItem
): Record<string, ModifiedItem> {
  return (
    array.reduce((result: Record<string, ModifiedItem>, item: Item) => ({
      ...result,
      [item[key]]: modifier ? modifier(item) : item,
    }), {})
  )
}

export const resolveUri = (...parts: Url[]) => {
  return (`/${parts.filter(part => part).join('/')}`)
    .replace(/\/+/g, '/')
    .replace(/\/[^/]*\/\.\./g, '')
}

export const resolveApiUri = (...parts: Url[]): string => new URL(resolveUri('/api', ...parts), APP_ORIGIN).toString()

export const ensureAbsoluteUrl = (...parts: Url[]): string => {
  const url = resolveUri(...parts)
  if (url.startsWith('/assets')) return url
  try {
    return new URL(url).toString()
  } catch (error) {
    return resolveApiUri(url)
  }
}

type MediaSourceValueType = Url | undefined | null

export function useMediaSourceState<S = MediaSourceValueType>(initialValue?: S): [S, Dispatch<SetStateAction<S>>] {
  const [value, pureSetValue] = useState<S>(
    initialValue ? ensureAbsoluteUrl(initialValue) as S : initialValue as S
  )

  // @ts-ignore
  const setValue = useCallback<Dispatch<SetStateAction<S>>>((newValue?: MediaSourceValueType) => {
    // @ts-ignore
    pureSetValue(newValue ? ensureAbsoluteUrl(newValue) : newValue)
  }, [])

  return [value, setValue]
}

export const isExternalUri = (uri: Url) => {
  try {
    new URL(uri.toString()).toString()
    return true
  } catch (error) {
    return false
  }
}

export function clearObject(object: AnyObject) {
  const cleanObject: Partial<typeof object> = {}

  Object.entries(object).forEach(([key, value]) => {
    if (typeof value === 'undefined' ||  value === false || value === null) return
    cleanObject[key] = value
  })

  return cleanObject
}

export function findById<T = Entity>(list: T[], entity: T): T | undefined {
  return list ? list.find((listItem) => listItem._id === entity._id) : undefined
}

interface ObjectWithPositionProperty extends Record<string, any> {
  position: number
}

export const sortByPosition = <T = ObjectWithPositionProperty[]>(array: T): T => (
  array.sort((a, b) => a.position > b.position ? 1 : -1)
)

export type PromiseAllSettledMap = Record<string, Promise<any>>

export interface PromiseAllSettledByKeyOptions {
  onlyResolved?: boolean
  onlyRejected?: boolean
}

export interface PromiseAllSettledByKeyItem<T = any> {
  isResolved: boolean
  value: T
}
export type PromiseAllSettledByKeyResult<T = any> = Record<string, PromiseAllSettledByKeyItem<T>>

export function promiseAllSettledByKey<T = PromiseAllSettledByKeyResult>(
  promisesMap: PromiseAllSettledMap = {},
  { onlyResolved = false, onlyRejected = false }: PromiseAllSettledByKeyOptions = {}
): Promise<T> {
  const totalPromises = Object.keys(promisesMap).length
  if (!totalPromises) return new Promise((resolve) => resolve({}))

  const settledPromises: PromiseAllSettledByKeyResult = {}

  const isAllPromisesSettled = () => Object.keys(settledPromises).length === totalPromises

  function applyFilters(promises: PromiseAllSettledByKeyResult) {
    const filteredPromises = { ...promises }
    Object.keys(filteredPromises).forEach((key) => {
      if (onlyResolved && !filteredPromises[key].isResolved)
        delete filteredPromises[key]

      if (onlyRejected && filteredPromises[key].isResolved)
        delete filteredPromises[key]
    })
    return Object.keys(filteredPromises).length ? filteredPromises : undefined
  }

  return new Promise((resolve) => {
    // TODO: Use Promise.finally to keep the resolve() call DRY!
    Object.keys(promisesMap).forEach((promiseKey) => {
      promisesMap[promiseKey].then((data) => {
        settledPromises[promiseKey] = {
          isResolved: true,
          value: data,
        }

        if (isAllPromisesSettled()) resolve(applyFilters(settledPromises))
      }).catch((error) => {
        settledPromises[promiseKey] = {
          isResolved: false,
          value: error,
        }

        if (isAllPromisesSettled()) resolve(applyFilters(settledPromises))
      })
    })
  })
}

export function useForceUpdate() {
  const [dep, setDep] = useState(Symbol('useForceUpdate'))
  const forceUpdate = useCallback(() => setDep(Symbol('useForceUpdate')), [])
  return [dep, forceUpdate]
}

export type EqualHeightElement = React.RefObject<React.ReactElement>

export interface EqualHeightGroup {
  addElements(...newElements: EqualHeightElement[]): void

  removeElements(...elementsToRemove: EqualHeightElement[]): void
}

// TODO: В опенсорс в качестве npm-модуля.
export const useEqualHeight = (...initialElements: EqualHeightElement[]): EqualHeightGroup => {
  const [elements, setElements] = useState(initialElements)

  const addElements = useCallback((...newElements) => {
    setElements((currentElements) => [...currentElements, ...newElements])
  }, [])

  const removeElements = useCallback((...elementsToRemove) => {
    setElements((currentElements) => currentElements.filter((element) => !elementsToRemove.includes(element)))
  }, [])

  const equalizeElementsHeight = useCallback(() => {
    let maxHeight = 0
    elements.forEach((element) => {
      if (!element.current) return
      maxHeight = Math.max(element.current.offsetHeight, maxHeight)
    })
    elements.forEach((element) => {
      if (!element.current) return
      element.current.style.height = `${maxHeight}px`
    })
  }, [elements])

  useEffect(() => {
    equalizeElementsHeight()
  }, [elements])

  return { addElements, removeElements, equalizeElementsHeight }
}

export const useEqualHeightElement = (equalHeightGroup: EqualHeightGroup, element: EqualHeightElement) => {
  const { addElements, removeElements } = equalHeightGroup

  useEffect(() => {
    addElements(element)
    return () => removeElements(element)
  }, [addElements, removeElements, element])
}

export const useIsSubscribed = (targetUser: User) => {
  const currentUserSubscriptions = useSelector(followingUsersSelector)

  const isSubscribed = useMemo(() => (
    (targetUser.subscription?.status === SubscriptionStatus.Active)
    || findById<User>(currentUserSubscriptions, targetUser)
  ), [currentUserSubscriptions, targetUser])

  return isSubscribed
}

export type OnCanLoadMoreCallback = () => Promise<void>

interface UseCanLoadMoreOptions {
  numberOfScreens: number
}

export const useCanLoadMore = (
  onCanLoadMore: OnCanLoadMoreCallback | undefined,
  elementRef: React.RefObject<React.ReactElement>,
  { numberOfScreens = 2 }: UseCanLoadMoreOptions = { numberOfScreens: 2 },
) => {
  const [isLoadingMore, setIsLoadingMore] = useState(false)

  const onDocumentScroll = useCallback(async () => {
    if (!elementRef.current || !onCanLoadMore || isLoadingMore) return
    const wrapperBottomBorderPosition = elementRef.current.offsetTop + elementRef.current.offsetHeight
    const positionToAllowLoadMore = wrapperBottomBorderPosition - (numberOfScreens * window.innerHeight)
    const windowVisibleBottomPosition = window.scrollY + window.innerHeight
    if (windowVisibleBottomPosition >= positionToAllowLoadMore) {
      setIsLoadingMore(true)
      await onCanLoadMore()
      setIsLoadingMore(false)
    }
  }, [onCanLoadMore, elementRef, isLoadingMore, numberOfScreens])

  useEffect(() => {
    if (IS_SERVER || !onCanLoadMore || !elementRef) return undefined
    document.addEventListener('scroll', onDocumentScroll)
    return () => document.removeEventListener('scroll', onDocumentScroll)
  }, [onDocumentScroll])

  return { isLoadingMore }
}

interface UseStickyConfig {
  bottom: number
}

export const useSticky = (elementRef: React.RefObject<HTMLElement>, config: UseStickyConfig) => {
  const onScroll = useCallback<React.WheelEventHandler>((event) => {
    if (!elementRef.current) return
    const stickyElement = elementRef.current
    const stickyContainer = stickyElement.parentElement
    console.dir(stickyElement.offsetTop)
    // stickyElement.style.transform = 'translateY(500px)'
    // console.dir(stickyContainer)
    // console.log('onScroll', event)
  }, [elementRef])

  useEffect(() => {
    document.addEventListener('scroll', onScroll)
    return () => document.removeEventListener('scroll', onScroll)
  }, [onScroll])
}
