import { IS_SERVER } from '@config/constants'

export const isLess = (size: number) => !IS_SERVER ? window.innerWidth <= size : undefined

export function checkIsTouch() {
  try {
    document.createEvent('TouchEvent')
    return true
  } catch (error) {
    return false
  }
}

const iDevices = [
  'iPad Simulator',
  'iPhone Simulator',
  'iPod Simulator',
  'iPad',
  'iPhone',
  'iPod',
]

export const checkIsIOS = () => !IS_SERVER ? navigator.platform && iDevices.includes(navigator.platform) : undefined
