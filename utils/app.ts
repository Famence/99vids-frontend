import React from 'react'
import { isMobileResult } from 'ismobilejs/types/isMobile'

export interface AppReactContextType {
  isMobile: isMobileResult
}

export const AppReactContext = React.createContext<AppReactContextType>()
