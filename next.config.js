const withStyles = require('@webdeb/next-styles')
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');

const isProduction = process.env.NODE_ENV === 'production'

module.exports = withStyles({
  poweredByHeader: false,
  webpack(config, options) {
    config.module.rules.push({
      test: /\.svg$/,
      oneOf: [
        {
          resource: /fonts\//,
          use: ['svg-url-loader'],
        },
        {
          use: [
            options.defaultLoaders.babel,
            {
              loader: 'svg-sprite-loader',
              options: {
                runtimeGenerator: require.resolve('./svg-to-icon-component-runtime-generator'),
                runtimeOptions: {
                  iconModule: './icon.jsx' // Relative to current build context folder
                }
              }
            }
          ],
        },
      ]
    }, {
      test: /\.(png|woff2?|gif|eot|ttf)$/,
      loader: 'url-loader'
    })
    config.plugins.push(new SpriteLoaderPlugin())
    return config
  },
  less: false,
  sass: true,
  modules: false,
  sassLoaderOptions: {
    sassOptions: {
      includePaths: ['style'],
    },
  },
  miniCssExtractOptions: {
    ignoreOrder: true
  },
  useFileSystemPublicRoutes: false,
  // TODO: cdn for production
  //  https://nextjsorg.now.sh/docs/#cdn-support-with-asset-prefix
  assetPrefix: isProduction ? '' : '',
  typescript: {
    ignoreBuildErrors: true,
  }
})
