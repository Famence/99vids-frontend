import axios, { AxiosRequestConfig, AxiosResponse, AxiosTransformer } from 'axios'
import { API_URL } from '@config/constants'
import {
  EntityType,
  FollowingStatusParams,
  ID,
  PaginationParams,
  PostsGroupParams,
  PostsParams,
  SortParams,
} from '@models/commonTypes'
import { User, UserRoles } from '@models/Users'
import { Subscription } from '@models/Subscription'
import { TariffType } from '@models/TariffType'
import { FetchedPostType, PostType } from '@models/Posts'
import { Payment, PaymentPurpose } from '@models/Payment'
import { instanceOf } from 'prop-types'

export interface ApiResponseData<T = any> {
  status: boolean
  data: T
}

const mergeParamsToConfig = (params: AxiosRequestConfig['params'], config: AxiosRequestConfig = {}) => ({
  ...config,
  params: { ...config.params, ...params },
})

const axiosDefaultResponseTransformer = (
  axios.defaults.transformResponse instanceof Array
    ? axios.defaults.transformResponse[0]
    : axios.defaults.transformResponse
) as AxiosTransformer

export const apiRequest = axios.create({
  baseURL: API_URL,
  transformResponse(_response: AxiosResponse<ApiResponseData>){
    const response = axiosDefaultResponseTransformer(_response)
    return response.status ? response.data : response
  },
})

interface AuthenticateUserRequestBody {
  email: string
  password: string
}

export const authenticateUser = (body: AuthenticateUserRequestBody, config?: AxiosRequestConfig) =>
  apiRequest.post<typeof body, User>('/auth', body, config)

export interface RegisterUserRequestBody extends AuthenticateUserRequestBody {
  checkPassword: string
  roleId: UserRoles
}

export const registerUser = (body: RegisterUserRequestBody, config?: AxiosRequestConfig) =>
  apiRequest.post<typeof body, User>('/signup', body, config)

export interface RegisterUserRequestBody extends AuthenticateUserRequestBody {
  checkPassword: string
  roleId: UserRoles
}

export interface ChangePasswordBody {
  oldPassword: string
  newPassword: string
  checkPassword: string
}

export const changePassword = (body: ChangePasswordBody, config?: AxiosRequestConfig) =>
  apiRequest.post('/password/change', body, config)

export interface UpdateProfileBody {
  oldPassword: string
  newPassword: string
  checkPassword: string
}

export const updateProfile = (body: UpdateProfileBody, config?: AxiosRequestConfig) =>
  apiRequest.put('/profile', body, config)

export interface RequestRestoreAccessBody {
  email: string
}

export const requestRestoreAccess = (body: RequestRestoreAccessBody, config?: AxiosRequestConfig) =>
  apiRequest.post<typeof body, User>('/password/restore', body, config)

export const getCurrentUser = (config?: AxiosRequestConfig) =>
  apiRequest.get<User>('/profile/current', config)

export type GetModelsParams = Partial<PaginationParams & FollowingStatusParams & {
  id: string
  active: boolean
  favorited: boolean
  subscribed: boolean
  unsubscribed: boolean
  withSubscription: true
}>

export const getModels = (params: GetModelsParams, config?: AxiosRequestConfig) =>
  apiRequest.get<{ list: User<undefined>[], count: number }>('/model', mergeParamsToConfig(params, config))

export type GetFollowingParams = GetModelsParams

export const getFollowing = (params: Omit<GetFollowingParams, 'subscribed'>, config?: AxiosRequestConfig) =>
  getModels({
    ...params,
    withSubscription: true,
  }, config)

export const getModelById = (modelId: User['_id'], config?: AxiosRequestConfig) =>
  apiRequest.get<User>(`/model/${modelId}`, config)

export type GetRecommendedParams = PaginationParams

interface GetRecommendedResponse {
  list: User<FetchedPostType>[]
  count: number
}

export const getRecommended = (params: GetRecommendedParams, config?: AxiosRequestConfig) =>
  apiRequest.get<GetRecommendedResponse>('/model?recommended&withPromo', mergeParamsToConfig(params, config))

export type GetFeedParams = PaginationParams & SortParams & Partial<PostsGroupParams> & {
  liked?: boolean
  commented?: boolean
}

interface GetFeedResponse {
  list: FetchedPostType[]
  count: number
}

export const getFeed = (params: GetFeedParams, config?: AxiosRequestConfig) =>
  apiRequest.get<GetFeedResponse>('/post/subscribed', mergeParamsToConfig(params, config))

interface GetUserPostsOptions {
  userId: User['_id']
}

export type GetUserPostsParams = Partial<PaginationParams & SortParams & PostsGroupParams & PostsParams>

export interface GetUserPostsResponse {
  list: FetchedPostType[]
  count: number
}

export const getUserPosts = (
  { userId }: GetUserPostsOptions,
  params: GetUserPostsParams,
  config?: AxiosRequestConfig
) =>
  apiRequest.get<GetUserPostsResponse>(`/post/${userId}`, mergeParamsToConfig(params, config))

interface GetUserPostOptions {
  userId: User['_id']
  postId: PostType['_id']
}

export interface GetUserPostResponse {
  data: FetchedPostType
}

export const getUserPost = (
  { userId, postId }: GetUserPostOptions,
  config?: AxiosRequestConfig
) =>
  apiRequest.get<GetUserPostResponse>(`/post/${userId}/${postId}`, config)

interface GetSubscriptionOptions {
  fanId: User['_id']
  modelId: User['_id']
}

export const getSubscription = ({ fanId, modelId }: GetSubscriptionOptions, config?: AxiosRequestConfig) =>
  apiRequest.get<{ data: Subscription }>(`/subscription/${fanId}/${modelId}`, config)

export const getRejectReasons = (config?: AxiosRequestConfig) =>
  apiRequest.get<{ data: Record<string, string> }>('/config/rejectReasons', config)

export const getMinTip = (config?: AxiosRequestConfig) =>
  apiRequest.get<{ data: { minTip: number } }>('/config/minTip', config)

interface SubscribeProps {
  modelId: User['_id']
  tariff: TariffType
}

export const subscribe = ({ modelId, tariff }: SubscribeProps, config?: AxiosRequestConfig) =>
  apiRequest.post(`/subscription/subscribe/${modelId}`, {
    tariffId: tariff._id,
    price: tariff.priceRules[tariff._id],
    duration: tariff._id,
  }, config)

interface SendTipProps {
  modelId: User['_id']
  sum: number
  message?: string
  postId?: PostType['_id']
}

export const sendTip = ({ modelId, sum, message, postId }: SendTipProps, config?: AxiosRequestConfig) =>
  apiRequest.post('/payment/tips', {
    recipient: modelId,
    amount: sum,
    message,
    postId,
  }, config)

interface SetLikeOptions {
  id: ID
  type?: EntityType
}

export const toggleLike = ({ id, type = EntityType.Post }: SetLikeOptions, config?: AxiosRequestConfig) =>
  apiRequest.post<{ liked: boolean }>(`/like/${type}/${id}`, config)

export type GetPaymentsParams = PaginationParams & SortParams

interface GetPaymentsResponse {
  list: Payment[]
  count: number
}

export const getPayments = (purpose: PaymentPurpose, params: GetPaymentsParams, config?: AxiosRequestConfig) =>
  apiRequest.get<GetPaymentsResponse>(`/payment/${purpose}`, mergeParamsToConfig(params, config))
