import { AxiosResponse } from 'axios'
import { ApiResponseData } from '@api/api'

export function extractData<T>(response: AxiosResponse<ApiResponseData<T>>): T {
  return response.data.data
}
