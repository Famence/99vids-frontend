const dotenv = require('dotenv-flow')
const dotenvExpand = require('dotenv-expand')
dotenvExpand(dotenv.config())

const express = require('express')
const proxy = require('express-http-proxy')
const next = require('next')

const Routes = require('./routes/config')

const { SERVER_ADDRESS, SERVER_HOST, SERVER_PORT } = process.env
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })

const routesRequestHandler = Routes.getRequestHandler(app)

app.prepare().then(() => {
  const server = express()

  server.use('/api', proxy(process.env.API_URL))

  server.use(routesRequestHandler)

  server.listen(SERVER_PORT, SERVER_HOST, (err) => {
    if (err) throw err
    console.log(`> Ready on ${SERVER_ADDRESS}`)
  })
})
