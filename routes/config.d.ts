export * from 'next-routes'
import NextRoutes from 'next-routes'

const Routes = new NextRoutes()

export default Routes
