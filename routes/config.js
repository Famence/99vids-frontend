const NextRoutes = require('next-routes')

const Routes = new NextRoutes()

Routes.add({
  name: 'index',
  pattern: '/',
  page: 'IndexPage',
})
Routes.add({
  name: 'recommended',
  pattern: '/recommended',
  page: 'RecommendedPage',
})
Routes.add({
  name: 'feed',
  pattern: '/feed',
  page: 'FeedPage',
})
Routes.add({
  name: 'chat',
  pattern: '/chat/:roomId?',
  page: 'ChatPage',
})
Routes.add({
  name: 'following',
  pattern: '/following/:status?',
  page: 'FollowingPage',
})
Routes.add({
  name: 'favorites',
  pattern: '/favorites/:type?',
  page: 'FavoritesPage',
})
Routes.add({
  name: 'earnings',
  pattern: '/earnings',
  page: 'EarningsPage',
})
Routes.add({
  name: 'support',
  pattern: '/support',
  page: 'SupportPage',
})
Routes.add({
  name: 'paidRedirect',
  pattern: '/paid',
  page: 'PaidRedirectPage',
})
Routes.add({
  name: 'logIn',
  pattern: '/login',
  page: 'AuthPage/AuthPage',
})
Routes.add({
  name: 'signUp',
  pattern: '/signup/:role?',
  page: 'AuthPage/AuthPage',
})
Routes.add({
  name: 'restoreAccess',
  pattern: '/restore',
  page: 'AuthPage/AuthPage',
})
Routes.add({
  name: 'editProfile',
  pattern: '/profile-edit/:tab?',
  page: 'EditProfilePage',
})
Routes.add({ // Этот должен оставаться ПОСЛЕДНИМ
  name: 'userPage',
  pattern: '/:userId',
  page: 'UserPage',
})

module.exports = Routes
