import { currentUserRoleSelector, currentUserSelector } from '@store/Users/selectors'
import { Page } from '@models/Pages'
import { UserRoles } from '@models/Users'
import { redirect } from '@utils/helpers'
import { initializeStore } from '@utils/store'
import { AppContext } from '@models/App'
import { Store } from '@store/store'
import Routes from '@routes/config'

const IndexPage: Page = () => null

IndexPage.getInitialProps = (ctx: AppContext['ctx']) => {
  const store = ctx?.res?.locals?.store as Store || initializeStore()
  const currentUser = currentUserSelector(store.getState())
  const currentUserRole = currentUserRoleSelector(store.getState())

  switch (currentUserRole) {
    default:
    case UserRoles.GUEST:
      redirect('recommended', ctx.res)
      break
    case UserRoles.FAN:
      redirect('feed', ctx.res)
      break
    case UserRoles.MODEL:
      if (currentUser) {
        redirect({
          route: Routes.findByName('userPage'),
          params: {
            userId: currentUser?.nickname || currentUser?._id,
          },
        }, ctx.res)
      } else
        redirect('logIn', ctx.res)
      break
  }
  return {}
}

export default IndexPage
