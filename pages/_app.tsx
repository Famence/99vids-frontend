import React, { useMemo } from 'react'
import { Provider } from 'react-redux'
import { ToastContainer } from 'react-toastify'
import ReactModal from 'react-modal'
import isMobileJs from 'ismobilejs'
import { initializeStore, useStore } from '@utils/store'
import { IS_SERVER } from '@config/constants'
import { AppContext } from '@models/App'
import { AppProps as NextAppProps } from 'next/dist/pages/_app'
import { RootState, Store } from '@store/store'
import { currentUserSelector, isCurrentUserFetchedSelector } from '@store/Users/selectors'
import { RootModal } from '@components/Modal/RootModal'
import { AppReactContext } from '@utils/app'
import 'react-toastify/dist/ReactToastify.min.css'
import '@style/global.sass'
import { redirect } from '@utils'
import Routes from '@routes/config'

interface AppProps extends NextAppProps {
  store: Store
  initialReduxState: RootState
}

interface AppType {
  getInitialProps: (context: AppContext) => Promise<Partial<AppProps>>
}

const App: AppType = ({ Component, pageProps, store: _store, initialReduxState, userAgent }: AppProps) => {
  let store = useStore(initialReduxState)
  if (IS_SERVER) store = _store

  const isMobile = useMemo(() => isMobileJs(userAgent), [userAgent])

  const appContext = useMemo(() => ({ isMobile }), [isMobile])

  return (
    <Provider store={store}>
      <AppReactContext.Provider value={appContext}>
        <Component {...pageProps} />
        <RootModal />
        <ToastContainer position='bottom-right' autoClose={8000} />
      </AppReactContext.Provider>
    </Provider>
  )
}

App.getInitialProps = async (context) => {
  const store = initializeStore()
  const userAgent = IS_SERVER ? context.ctx.req?.headers['user-agent'] : navigator.userAgent

  if (IS_SERVER) {
    context.ctx.res.locals.store = store
    context.ctx.res.locals.requestConfig = { headers: context.ctx.req.headers }
    context.ctx.res.locals.isMobile = isMobileJs(userAgent)
  }

  const { getInitialDataConfig } = context.Component

  const { requestConfig } = context.ctx.res?.locals || {}

  const isCurrentUserFetched = isCurrentUserFetchedSelector(store.getState())
  if (!isCurrentUserFetched) {
    await store.dispatch.users.getCurrentUser(requestConfig)
    await store.dispatch.users.getInitialDataForCurrentUser({
      getInitialDataConfig,
      requestConfig,
    })
  }
  const currentUser = currentUserSelector(store.getState())

  if (context.Component.accessGuards) {
    try {
      context.Component.accessGuards.forEach(accessGuard => accessGuard(currentUser))
    } catch (error) {
      if (error.onCatch) {
        await error.onCatch(context)
      } else {
        if (error.redirectTo) {
          redirect(error.redirectTo, context.ctx.res)
          if (error.onRedirect) await error.onRedirect(context)
        } else {
          throw error
        }
      }
      return {}
    }
  }

  if (context.Component.requestInitialData) await context.Component.requestInitialData({ store, context })
  const pageProps = context.Component.getInitialProps ? await context.Component.getInitialProps(context.ctx) : {}


  return { pageProps, store, initialReduxState: IS_SERVER ? store.getState() : undefined, userAgent }
}

export default App

ReactModal.setAppElement('#__next')
