import React from 'react'
import { Page } from '@models/Pages'
import { PageContext } from '@models/App'
import { AccessError, ServerError } from '@models/errors'
import { AppRoot } from '@components/AppRoot/AppRoot'
import { ErrorComponent } from '@components/Error'

interface ErrorPageProps {
  error: ServerError | AccessError
}

const ErrorPage: Page<ErrorPageProps> = ({ error }) => (
  <AppRoot>
    <ErrorComponent {...error} />
  </AppRoot>
)

ErrorPage.getInitialProps = (ctx: PageContext) => {
  const statusCode = ctx.err?.statusCode || ctx.err?.error?.statusCode
  if (statusCode) ctx.res.statusCode = statusCode

  const error = ctx.err?.error || (
    statusCode ? new ServerError(statusCode, ctx.res.statusMessage) : 'Unknown error'
  )

  return { error }
}

export default ErrorPage
