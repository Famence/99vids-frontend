import React, { useCallback, useMemo } from 'react'
import Head from 'next/head'
import cn from 'classnames'
import { Page } from '@models/Pages'
import { UserRoles } from '@models/Users'
import { accessGuards, useStore } from '@utils'
import { useRouter } from 'next/router'
import Routes, { Router } from '@routes/config'
import { Logo, LogoTheme } from '@components/Logo/Logo'
import { Tabs } from '@components/Tabs/Tabs'
import { SignInForm } from '@components/Form/SignInForm/SignInForm'
import { Theme } from '@models/commonTypes'
import { SocialLoginThemeEnum } from '@components/SocialLogin/SocialLogin'
import { ButtonTheme } from '@components/Button/Button'
import { SignUpForm, SignUpFormProps } from '@components/Form/SignUpForm/SignUpForm'
import { RestoreAccessForm } from '@components/Form/RestoreAccessForm/RestoreAccessForm'
import './AuthPage.sass'

enum AuthPageTypes {
  LogIn = 'logIn',
  SignUp = 'signUp',
  Restore = 'restoreAccess',
}

const authPageTitles = {
  [AuthPageTypes.LogIn]: 'LogIn',
  [AuthPageTypes.SignUp]: 'SignUp',
  [AuthPageTypes.Restore]: 'Restore Access',
}

const AuthPage: Page = () => {
  const router = useRouter()

  const role = router.query.role as UserRoles

  const type = useMemo(() => {
    if (Routes.findByName('signUp').regex.test(router.asPath))
      return AuthPageTypes.SignUp
    if (Routes.findByName('restoreAccess').regex.test(router.asPath))
      return AuthPageTypes.Restore
    return AuthPageTypes.LogIn
  }, [router.asPath])

  return (
    <div className={cn('AuthPage', `type-${type}`, { [`role-${role}`]: role })}>
      <div className='AuthPage__aside'>
        <Logo theme={LogoTheme.Light} />
        <h1 className='heading'>Welcome to<br />99vids</h1>
        <p>
          Earn money as a model by posting
          <br />
          your nude photos and videos.
        </p>
      </div>
      <Head>
        <title>{authPageTitles[type]} — 99vids</title>
      </Head>
      <div className='AuthPage__content-container'>
        <div className='AuthPage__content'>
          {[AuthPageTypes.LogIn, AuthPageTypes.SignUp].includes(type) && (
            <AuthPageNav type={type} />
          )}
          <AuthPageContent type={type} role={role} />
        </div>
      </div>
    </div>
  )
}

AuthPage.accessGuards = [
  accessGuards.allowRole([UserRoles.GUEST], { redirectTo: 'index' }),
]

export default AuthPage

const authPageNavTabs = [{
  value: {
    type: AuthPageTypes.LogIn,
    page: 'logIn',
  },
  label: 'Log in',
  key: 'login',
}, {
  value: {
    type: AuthPageTypes.SignUp,
    page: 'signUp',
  },
  label: 'Sign up',
  key: 'signUp',
}]

const AuthPageNav: React.FunctionComponent<{ type: AuthPageTypes }> = React.memo(({ type }) => {
  const activeTab = useMemo(() => authPageNavTabs.find((tab) => tab.value.type === type), [type])

  const onChangeTab = useCallback((tab) => Router.pushRoute(tab.value.page), [])

  return (
    <div className='AuthPage__nav bigTabs'>
      <Tabs tabs={authPageNavTabs} active={activeTab} onChange={onChangeTab} />
    </div>
  )
})

interface AuthPageContentProps {
  type: AuthPageTypes
  role: UserRoles
}

const AuthPageContent: React.FunctionComponent<AuthPageContentProps> = React.memo(({ type, role }) => {
  const onAuthenticate = useCallback(() => Routes.Router.replaceRoute('index'), [])

  switch (type) {
    case AuthPageTypes.LogIn:
      return <AuthPageLogIn onAuthenticate={onAuthenticate} />
    case AuthPageTypes.SignUp:
      return <AuthPageSignUp role={role} onAuthenticate={onAuthenticate} />
    case AuthPageTypes.Restore:
      return <AuthPageRestoreAccess />
    default:
      return null
  }
})

const formProps = {
  theme: Theme.Dark,
}

interface AuthPageLogInProps {
  onAuthenticate: () => void
}

export const AuthPageLogIn: React.FunctionComponent<AuthPageLogInProps> = React.memo(({
  onAuthenticate,
}) => {
  const store = useStore()

  const onSubmit = useCallback((values, { setSubmitting }) => {
    store.dispatch.users.authenticate({ ...values, registerIfNot: false, onSuccess: onAuthenticate })
    setSubmitting(false)
  }, [store])

  return (
    <div className='AuthPage__login'>
      <SignInForm
        formProps={formProps}
        socialLoginTheme={SocialLoginThemeEnum.Colored}
        buttonTheme={ButtonTheme.PURPLE}
        onSubmit={onSubmit}
      />
    </div>
  )
})

interface AuthPageSignUpProps {
  role: UserRoles
  onAuthenticate: () => void
}

export const AuthPageSignUp: React.FunctionComponent<AuthPageSignUpProps> = React.memo(({
  onAuthenticate,
  ...props
}) => {
  const store = useStore()

  const onSubmit = useCallback((values, { setSubmitting }) => {
    store.dispatch.users.register({
      email: values.email,
      password: values.password,
      checkPassword: values.confirmPassword,
      roleId: values.role,
    })
      .then(() => onAuthenticate())
      .catch(() => {})
      .finally(() => setSubmitting(false))
  }, [store])

  const onChangeRole = useCallback<SignUpFormProps['onChangeRole']>((newRole) => {
    Routes.Router.replaceRoute('signUp', { role: newRole })
  }, []);

  return (
    <div className='AuthPage__signUp'>
      <SignUpForm {...props} onChangeRole={onChangeRole} onSubmit={onSubmit} />
    </div>
  )
})

export const AuthPageRestoreAccess: React.FunctionComponent = React.memo(() => {
  const store = useStore()

  const onSubmit = useCallback((values, { setSubmitting }) => {
    store.dispatch.users.requestRestoreAccess(values)
      .then(() => Routes.Router.pushRoute('logIn'))
      .catch(() => {})
      .finally(() => setSubmitting(false))
  }, [store])

  return (
    <div className='RestoreAccess'>
      <button type='button' onClick={Routes.Router.back} className='RestoreAccess__backButton'>back</button>
      <h2 className='RestoreAccess__title'>Forgot your password?</h2>
      <RestoreAccessForm onSubmit={onSubmit} />
    </div>
  )
})
