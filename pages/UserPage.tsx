import React, { useCallback, useContext, useEffect, useMemo, useRef, useState } from 'react'
import { useSelector } from 'react-redux'
import promiseAllProperties from 'promise-all-properties'
import { currentUserSelector, usersListSelector, usersSelector } from '@store/Users/selectors'
import Head from 'next/head'
import { Page } from '@models/Pages'
import { User } from '@models/Users'
import { ServerError } from '@models/errors'
import { AppRoot } from '@components/AppRoot/AppRoot'
import { DropZone, DropZoneProps } from '@components/DropZone/DropZone'
import { ModelCard } from '@components/ModelCard/ModelCard'
import { RecommendationsBlock } from '@components/RecommendationsBlock/RecommendationsBlock'
import { UsersListLayout } from '@components/UsersList/UsersList'
import { AppReactContext } from '@utils/app'
import { UserFeedBlock } from '@components/UserFeedBlock/UserFeedBlock'
import { initializeStore, useStore } from '@utils'
import { IS_SERVER } from '@config/constants'

const userIdKey = 'userId'

// const stickyConfig = {
//   bottom: 40,
// }

interface UserPageProps {
  userId: User['_id']
  needToLoadPosts?: boolean
}

const UserPage: Page<UserPageProps> = ({ userId, needToLoadPosts = false }) => {
  const infoColumnRef = useRef(null)
  // useSticky(infoColumnRef, stickyConfig)
  const store = useStore()
  const users = useSelector(usersSelector)
  const currentUser = useSelector(currentUserSelector)
  const { isMobile } = useContext(AppReactContext)
  const user = users[userId]

  const [isEditingPost, setIsEditingPost] = useState(false)

  const isOwnPage = useMemo(() => currentUser?._id === userId, [currentUser, userId])

  const showRecommendations = !isOwnPage && !isMobile.any

  // const infoContainerStyle = useMemo(() => ({
  //   minHeight: infoBlockRef.current && !isMobile.any ? infoBlockRef.current.clientHeight + 40 : undefined,
  // }), [isMobile.any])

  const infoBlockStyle = useMemo(() => ({}), [])

  const excludeUserIdsFromRecommended = useMemo(() => [userId], [userId])

  useEffect(() => {
    if (needToLoadPosts) store.dispatch.posts.getUserPosts({ userId })
  }, [userId, needToLoadPosts])

  return (
    <AppRoot>
      <Head>
        <title>{user.displayName} @ 99vids</title>
      </Head>
      <ModelPageDropZone isOwnPage={isOwnPage} disabled={isEditingPost}>
        <div className='container'>
          <div className='row'>
            <div
              className='col md-5'
              // style={infoContainerStyle}
              ref={infoColumnRef}
            >
              <div
                style={infoBlockStyle}
                className='model-info'
              >
                <ModelCard model={user} isOwn={isOwnPage} />
                {/*{isOwnPage && <ModelBalance />}*/}
                {showRecommendations && (
                  <RecommendationsBlock
                    excludeUserIds={excludeUserIdsFromRecommended}
                    layout={UsersListLayout.Horizontal}
                  />
                )}
              </div>
            </div>
            <div className='col md-7'>
              {/*{isOwnPage && <IDModerationStatus />}*/}
              {/*{isOwnPage && (*/}
              {/*  <NewPost*/}
              {/*    droppedFiles={this.state.droppedFiles}*/}
              {/*    onCreateNormal={this.onPublishNewPost}*/}
              {/*    onCreateDeffered={this.onPublishNewDeferredPost}*/}
              {/*  />*/}
              {/*)}*/}
              <UserFeedBlock user={user} />
            </div>
          </div>
        </div>
      </ModelPageDropZone>
    </AppRoot>
  )
}

UserPage.getInitialProps = async (ctx): Promise<UserPageProps> => {
  const { requestConfig } = ctx.res?.locals || {}
  const store = IS_SERVER ? ctx.res.locals.store : initializeStore()
  const userIdOrNickname = ctx.query[userIdKey]
  // if (!userIdOrNickname) throw new Error() // TODO: Выкидываем примерно 400 ошибку (юзерская ошибка)

  const props: Partial<UserPageProps> = {}

  const usersList1 = usersListSelector(store.getState())
  let user = usersList1.find(({ _id, nickname }) => [_id, nickname].includes(userIdOrNickname))
  if (!user) {
    await store.dispatch.users.getModelById(userIdOrNickname, requestConfig)
    const usersList2 = usersListSelector(store.getState())
    user = usersList2.find(({ _id, nickname }) => [_id, nickname].includes(userIdOrNickname))
  }
  const userId = user?._id
  if (!userId) throw new ServerError(404, 'User not found')
  props.userId = userId

  const actions: Record<string, Promise<void>> = {}
  if (IS_SERVER)
    actions.fetchUserPosts = store.dispatch.posts.getUserPosts({ userId, requestConfig })
  else
    props.needToLoadPosts = true

  await promiseAllProperties(actions)

  return props as UserPageProps
}

export default UserPage

interface ModelPageDropZoneProps extends DropZoneProps {
  isOwnPage: boolean
}

const ModelPageDropZone: React.FunctionComponent<ModelPageDropZoneProps> = React.memo(({
  isOwnPage,
  ...props
}) => {
  const [droppedFiles, setDroppedFiles] = useState(false)

  const onDropAcceptedFile: DropZoneProps['onDropAccepted'] = useCallback((files) => {
    window.scrollTo(0, 0)
    setDroppedFiles(files)
  }, [])

  return (
    isOwnPage ? (
      <DropZone
        className='ModelPage__DropZoneWrapper'
        onDropAccepted={onDropAcceptedFile}
        noClick
        {...props}
      />
    ) : props.children
  ) as React.ReactElement
})
