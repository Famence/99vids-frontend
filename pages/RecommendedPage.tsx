import React, { useCallback, useEffect } from 'react'
import Head from 'next/head'
import { useSelector } from 'react-redux'
import { recommendedPostsSelector, recommendedStateSelector } from '@store/Recommended/selectors'
import { Page } from '@models/Pages'
import { UserRoles } from '@models/Users'
import { accessGuards } from '@utils/access'
import { AppRoot } from '@components/AppRoot/AppRoot'
import { Block } from '@components/Block/Block'
import { useLoaderSelector } from '@store/Loaders/selectors'
import { LoaderTypes } from '@store/Loaders/constants'
import { PostsFeed } from '@components/PostsFeed/PostsFeed'
import { initializeStore, useStore } from '@utils'
import { IS_SERVER } from '@config/constants'
import { FollowingUsersBlock } from '@components/FollowingUsersBlock/FollowingUsersBlock'
import Routes from '@routes/config'

interface RecommendedPageProps {
  needToFetch?: boolean
}

const RecommendedPage: Page<RecommendedPageProps> = ({
  needToFetch = false,
}) => {
  const store = useStore()
  const recommendedPosts = useSelector(recommendedPostsSelector)
  const { isNoMore } = useSelector(recommendedStateSelector)
  const isRecommendedPostsLoading = useLoaderSelector(LoaderTypes.RecommendedLoader)

  const onCanLoadMore = useCallback(async () => {
    if (isNoMore) return
    await store.dispatch.recommended.getRecommended({ pagination: { offset: recommendedPosts.length } })
  }, [recommendedPosts.length, isNoMore])

  useEffect(() => {
    if (needToFetch) store.dispatch.recommended.getRecommended()
  }, [needToFetch])

  return (
    <AppRoot>
      <Head>
        <title>Recommended — 99vids</title>
      </Head>
      <div className='container'>
        <div className='row'>
          <div className='col md-8'>
            <Block className='PostsBlock' title='Recommended' withTopBorder>
              <PostsFeed
                posts={recommendedPosts}
                isRecommended
                noRecommendedDesign
                onCanLoadMore={onCanLoadMore}
                isLoading={isRecommendedPostsLoading}
              />
            </Block>
          </div>
          <div className='col md-4'>
            <FollowingUsersBlock />
          </div>
        </div>
      </div>
    </AppRoot>
  )
}

RecommendedPage.getInitialProps = async (ctx) => {
  const { requestConfig } = ctx.res?.locals || {}
  const store = IS_SERVER ? ctx.res.locals.store : initializeStore()

  const recommendedState = recommendedStateSelector(store.getState())

  const isRecommendedFetched = recommendedState.isNoMore || recommendedState.posts.length

  const props: RecommendedPageProps = {}
  const requests = []

  if (!isRecommendedFetched) {
    if (IS_SERVER)
      requests.push(store.dispatch.recommended.getRecommended({ requestConfig }))
    else
      props.needToFetch = true
  }

  await Promise.all(requests)

  return props
}

RecommendedPage.accessGuards = [
  accessGuards.allowRole([UserRoles.GUEST, UserRoles.FAN], { redirectTo: 'index' }),
]

export default RecommendedPage
