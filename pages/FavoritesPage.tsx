import React, { useCallback, useEffect, useMemo } from 'react'
import Head from 'next/head'
import { useRouter } from 'next//router'
import { Page } from '@models/Pages'
import { FavoritesType } from '@models/commonTypes'
import { AppRoot } from '@components/AppRoot/AppRoot'
import { Block } from '@components/Block/Block'
import { FavoritesTypeTabs } from '@components/Favorites/TypeTabs/FavoritesTypeTabs'
import Routes, { Router } from '@routes/config'
import {
  accessGuards,
  initializeStore,
  redirect,
  redirectToPreviousPage,
  requestAuthentication,
  useStore,
} from '@utils'
import { IS_SERVER } from '@config/constants'
import { FavoriteUsers } from '@components/FavoriteUsers/FavoriteUsers'
import { TippedUsers } from '@components/TippedUsers/TippedUsers'
import { LikedPosts } from '@components/LikedPosts/LikedPosts'
import { CommentedPosts } from '@components/CommentedPosts/CommentedPosts'
import { RecommendationsBlock } from '@components/RecommendationsBlock/RecommendationsBlock'
import { UserRoles } from '@models/Users'
import FeedPage from '@root/pages/FeedPage'

interface FollowingPageProps {
  needToFetch: boolean
}

const FavoritesPage: Page<FollowingPageProps> = ({ needToFetch = false }) => {
  const store = useStore()
  const router = useRouter()

  const type = router.query.type as FavoritesType

  const onTabChange = useCallback((newType: FavoritesType) => {
    Router.pushRoute('favorites', { type: newType })
    store.dispatch.favorites.initialFetchByType({ type: newType })
  }, [])

  const followBlockTopContent = useMemo(() => (
    <FavoritesTypeTabs
      active={type}
      onChange={onTabChange}
    />
  ), [type, onTabChange])

  useEffect(() => {
    if (!needToFetch) return
    store.dispatch.favorites.initialFetchByType({ type })
  }, [])

  return (
    <AppRoot>
      <Head>
        <title>Favorites — 99vids</title>
      </Head>
      <div className='container'>
        <div className='row'>
          <div className='col md-8'>
            <Block className='FavoritesBlock' withTopBorder bigTabs topContent={followBlockTopContent}>
              <FavoritesPageContent type={type} />
            </Block>
          </div>
          <div className='col md-4'>
            <RecommendationsBlock title='Recommended For You' />
          </div>
        </div>
      </div>
    </AppRoot>
  )
}

FavoritesPage.getInitialProps = async (ctx) => {
  const props: FollowingPageProps = {}
  const requests = []

  if (!ctx.query.type) {
    redirect({ route: Routes.findByName('favorites'), params: { type: FavoritesType.Favorite } }, ctx.res)
    return {}
  }
  const { requestConfig } = ctx.res?.locals || {}
  const store = IS_SERVER ? ctx.res.locals.store : initializeStore()
  const type = ctx.query.type as FavoritesType

  if (IS_SERVER)
    await store.dispatch.favorites.initialFetchByType({ type, requestConfig })
  else
    props.needToFetch = true

  await Promise.all(requests)

  return props
}

export default FavoritesPage

FavoritesPage.accessGuards = [
  accessGuards.allowRole([UserRoles.FAN], {
    onCatch(context) {
      redirectToPreviousPage(context)
      requestAuthentication(context, () => redirect('favorites'))
    },
  }),
]

interface FavoritesPageContentProps {
  type: FavoritesType
}

const FavoritesPageContent: React.FunctionComponent<FavoritesPageContentProps> = React.memo(({
  type,
}) => {
  switch (type) {
    case FavoritesType.Favorite:
      return <FavoriteUsers />
    case FavoritesType.Tipped:
      return <TippedUsers />
    case FavoritesType.Liked:
      return <LikedPosts />
    case FavoritesType.Commented:
      return <CommentedPosts />
    default:
      return null
  }
})
