import React, { useEffect, useMemo, useRef } from 'react'
import { useRouter } from 'next/router'
import Routes from '@routes/config'
import Head from 'next/head'
import { ParsedUrlQuery } from 'querystring'
import { toast } from 'react-toastify'
import { Page } from '@models/Pages'
import { Loader } from '@components/Loader/Loader'
import { ModalTypes } from '@store/Modals/constants'
import { useSelector } from 'react-redux'
import { usersSelector } from '@store/Users/selectors'
import { useStore } from '@utils'

const getTargetModelId = (query: ParsedUrlQuery) => {
  const custom1 = JSON.parse(Buffer.from(query.custom1 as string, 'base64').toString())
  const custom2 = JSON.parse(Buffer.from(query.custom2 as string, 'base64').toString())

  switch (query.type) {
    case 'subscription':
      return custom1.to
    case 'purchase':
      return custom2.recipient
    default:
      return null
  }
}

const PaidRedirectPage: Page = () => {
  const alreadyPerformed = useRef(false)
  const store = useStore()
  const router = useRouter()
  const { query } = router
  const users = useSelector(usersSelector)

  const { type, priceAmount } = query

  const targetModelId = useMemo(() => getTargetModelId(query), [query])

  const targetModel = useMemo(() => targetModelId ? users[targetModelId] : targetModelId, [users, targetModelId])

  useEffect(() => {
    if (!targetModel || alreadyPerformed.current) return
    Routes.Router.replaceRoute('userPage', { userId: targetModel.nickname || targetModel._id })
    switch (type) {
      case 'purchase':
        toast.success(`${targetModel.firstname} got $${priceAmount}`)
        break
      default:
      case 'subscription':
        store.dispatch.modals.open({ type: ModalTypes.SubscribeCongrats, model: targetModel })
        break
    }
    alreadyPerformed.current = true
  }, [targetModel, priceAmount, type])

  return (
    <>
      <Head>
        <title>Thank you for payment — 99vids</title>
      </Head>
      <Loader />
    </>
  )
}

PaidRedirectPage.requestInitialData = async ({ store, context }) => {
  const targetModelId = getTargetModelId(context.ctx.query)
  await store.dispatch.users.getModelById(targetModelId)
}

export default PaidRedirectPage
