import React, { useCallback, useEffect, useMemo } from 'react'
import Head from 'next/head'
import { useSelector } from 'react-redux'
import { Page } from '@models/Pages'
import { UserRoles } from '@models/Users'
import { accessGuards, redirectToPreviousPage, requestAuthentication } from '@utils/access'
import { AppRoot } from '@components/AppRoot/AppRoot'
import { Block } from '@components/Block/Block'
import { useLoaderSelector } from '@store/Loaders/selectors'
import { LoaderTypes } from '@store/Loaders/constants'
import { PostsFeed } from '@components/PostsFeed/PostsFeed'
import { initializeStore, redirect, useStore } from '@utils'
import { IS_SERVER } from '@config/constants'
import { feedPostsGroupSelector, feedPostsSelector, feedStateSelector } from '@store/Feed/selectors'
import { PostsFeedTabs, PostsFeedTabsProps } from '@components/PostsFeed/PostsFeedGroups/PostsFeedGroups'
import { FollowingUsersBlock } from '@components/FollowingUsersBlock/FollowingUsersBlock'

interface FeedPageProps {
  needToFetch?: boolean
}

const FeedPage: Page<FeedPageProps> = ({
  needToFetch = false,
}) => {
  const store = useStore()
  const feedPosts = useSelector(feedPostsSelector)
  const group = useSelector(feedPostsGroupSelector)
  const { isNoMore } = useSelector(feedStateSelector)
  const isFeedLoading = useLoaderSelector(LoaderTypes.FeedLoader)

  const onCanLoadMore = useCallback(async () => {
    if (isNoMore) return
    await store.dispatch.feed.getFeed({ pagination: { offset: feedPosts.length } })
  }, [feedPosts.length, isNoMore])

  useEffect(() => {
    if (needToFetch) store.dispatch.feed.getFeed()
  }, [needToFetch])

  const onGroupChange = useCallback<PostsFeedTabsProps['onGroupChange']>((group) => {
    store.dispatch.feed.getFeed({ group })
  }, [])

  const feedBlockTopContent = useMemo(() => (
    <PostsFeedTabs active={group} onGroupChange={onGroupChange} />
  ), [feedPosts, onGroupChange])

  return (
    <AppRoot>
      <Head>
        <title>Feed — 99vids</title>
      </Head>
      <div className='container'>
        <div className='row'>
          <div className='col md-8'>
            <Block className='PostsBlock' title='Feed' withTopBorder topContent={feedBlockTopContent}>
              <PostsFeed
                posts={feedPosts}
                onCanLoadMore={onCanLoadMore}
                isLoading={isFeedLoading}
              />
            </Block>
          </div>
          <div className='col md-4'>
            <FollowingUsersBlock />
          </div>
        </div>
      </div>
    </AppRoot>
  )
}

FeedPage.getInitialProps = async (ctx) => {
  const { requestConfig } = ctx.res?.locals || {}
  const store = IS_SERVER ? ctx.res.locals.store : initializeStore()

  const feedState = feedStateSelector(store.getState())

  const isFeedFetched = feedState.isNoMore || feedState.posts.length

  const props: FeedPageProps = {}
  const requests = []

  if (!isFeedFetched) {
    if (IS_SERVER)
      requests.push(store.dispatch.feed.getFeed({ requestConfig }))
    else
      props.needToFetch = true
  }

  await Promise.all(requests)

  return props
}

FeedPage.accessGuards = [
  accessGuards.allowRole([UserRoles.FAN], {
    onCatch(context) {
      redirectToPreviousPage(context)
      requestAuthentication(context, () => redirect('feed'))
    },
  }),
]

export default FeedPage
