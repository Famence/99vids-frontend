import React, { useCallback, useEffect, useMemo } from 'react'
import { useSelector } from 'react-redux'
import Head from 'next/head'
import { useRouter } from 'next//router'
import { Page } from '@models/Pages'
import { FollowingStatus } from '@models/Subscription'
import { AppRoot } from '@components/AppRoot/AppRoot'
import { Block } from '@components/Block/Block'
import { FollowingStatusTabs } from '@components/Following/StatusTabs/FollowingStatusTabs'
import { UsersGrid } from '@components/UsersGrid/UsersGrid'
import Routes, { Router } from '@routes/config'
import {
  accessGuards,
  initializeStore,
  redirect,
  redirectToPreviousPage,
  requestAuthentication,
  useStore,
} from '@utils'
import {
  followingCountSelector,
  followingsFetchedStatusSelector,
  followingUsersSelector,
} from '@store/Followings/selectors'
import { useLoaderSelector } from '@store/Loaders/selectors'
import { LoaderTypes } from '@store/Loaders/constants'
import { IS_SERVER } from '@config/constants'
import { RecommendationsBlock } from '@components/RecommendationsBlock/RecommendationsBlock'
import { PostDummy } from '@components/PostDummy/PostDummy'
import { UserRoles } from '@models/Users'
import FeedPage from '@root/pages/FeedPage'

interface FollowingPageProps {
  needToFetch: boolean
}

const FollowingPage: Page<FollowingPageProps> = ({
  needToFetch = false,
}) => {
  const store = useStore()
  const router = useRouter()
  const status = router.query.status as FollowingStatus
  const followingUsers = useSelector(followingUsersSelector)
  const followingCount = useSelector(followingCountSelector)
  const isLoading = useLoaderSelector(LoaderTypes.FollowingUsersLoader)

  const onStatusChange = useCallback((newStatus: FollowingStatus) => {
    Router.pushRoute('following', { status: newStatus })
    store.dispatch.followings.getFollowing({ status: newStatus })
  }, [])

  const followBlockTopContent = useMemo(() => (
    <FollowingStatusTabs active={status} onChange={onStatusChange} />
  ), [status, onStatusChange])

  const onCanLoadMore = useCallback(async () => {
    if (typeof followingCount === 'number' && followingCount <= followingUsers.length) return
    await store.dispatch.followings.getFollowing({ status, pagination: { offset: followingUsers.length } })
  }, [status, followingUsers.length, followingCount])

  useEffect(() => {
    if (!needToFetch) return
    store.dispatch.followings.getFollowing({ status })
  }, [])

  return (
    <AppRoot>
      <Head>
        <title>Following — 99vids</title>
      </Head>
      <div className='container'>
        <div className='row'>
          <div className='col md-8'>
            <Block className='FollowingBlock' title='Following' withTopBorder topContent={followBlockTopContent}>
              {followingUsers.length || isLoading ? (
                <UsersGrid
                  users={followingUsers}
                  isLoading={isLoading}
                  onCanLoadMore={onCanLoadMore}
                  withSubscription
                />
              ) : (
                <PostDummy text='There are no subscriptions...' />
              )}
            </Block>
          </div>
          <div className='col md-4'>
            <RecommendationsBlock title='Recommended For You' />
          </div>
        </div>
      </div>
    </AppRoot>
  )
}

FollowingPage.getInitialDataConfig = {
  requestFollowing: false,
}

FollowingPage.getInitialProps = async (ctx) => {
  const { requestConfig } = ctx.res?.locals || {}
  const store = IS_SERVER ? ctx.res.locals.store : initializeStore()
  const status = ctx.query.status as FollowingStatus

  const props: FollowingPageProps = { needToFetch: false }

  if (!status) {
    redirect({ route: Routes.findByName('following'), params: { status: FollowingStatus.Subscribed } }, ctx.res)
    return props
  }

  const fetchedStatus = followingsFetchedStatusSelector(store.getState())

  if (fetchedStatus !== status) {
    if (IS_SERVER) {
      await store.dispatch.followings.getFollowing({ status, requestConfig })
    } else {
      props.needToFetch = true
    }
  }

  return props
}

export default FollowingPage

FollowingPage.accessGuards = [
  accessGuards.allowRole([UserRoles.FAN], {
    onCatch(context) {
      redirectToPreviousPage(context)
      requestAuthentication(context, () => redirect('following'))
    },
  }),
]
