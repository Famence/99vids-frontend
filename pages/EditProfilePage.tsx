import React from 'react'
import { useSelector } from 'react-redux'
import { Page } from '@models/Pages'
import { UserRoles } from '@models/Users'
import { AppRoot } from '@components/AppRoot/AppRoot'
import { EditProfileModel } from '@components/EditProfile/EditProfileModel'
import { EditProfileFan } from '@components/EditProfile/EditProfileFan'
import { currentUserRoleSelector } from '@store/Users/selectors'
import { accessGuards, redirect } from '@utils'
import Routes from '@routes/config'
import { EditProfilePageTabs } from '@components/EditProfile/constants'

const EditProfilePage: Page = () => {
  const currentUserRole = useSelector(currentUserRoleSelector)

  let content

  switch (currentUserRole) {
    case UserRoles.MODEL:
      content = <EditProfileModel />
      break;
    case UserRoles.FAN:
      content = <EditProfileFan />
      break;
  }

  return (
    <AppRoot>
      <div className='container'>
        <div className='row'>
          <div className='col-12'>
            {content}
          </div>
        </div>
      </div>
    </AppRoot>
  )
}

EditProfilePage.getInitialProps = (ctx) => {
  if (!ctx.query.tab) {
    redirect({ route: Routes.findByName('editProfile'), params: { tab: EditProfilePageTabs.General } }, ctx.res)
    return {}
  }

  return {}
}

EditProfilePage.accessGuards = [
  accessGuards.allowRole([UserRoles.MODEL, UserRoles.FAN], { redirectTo: 'index' })
]

export default EditProfilePage
