{
  "rules": {
    "curly": ["error", "multi", "consistent"],
    "semi": "off",
    "indent": "off",
    "object-curly-spacing": "error",
    "comma-dangle": ["warn", {
      "arrays": "always-multiline",
      "exports": "always-multiline",
      "functions": "never",
      "imports": "always-multiline",
      "objects": "always-multiline"
    }],
    "no-param-reassign": "off",
    "no-nested-ternary": "off",
    "no-plusplus": "off",

    "react/require-default-props": "off",
    "react/jsx-props-no-spreading": "off",
    "react/jsx-one-expression-per-line": "off",
    "react/no-unescaped-entities": "off",

    // Не используем дефолтный экспорт, это приводит к путанице в именах компонентов.
    "import/prefer-default-export": "off",

    "jsx-a11y/click-events-have-key-events": "off",
    "jsx-a11y/no-static-element-interactions": "off",
    "jsx-a11y/anchor-is-valid": "off",
    "jsx-a11y/label-has-for": "off",
    "jsx-a11y/label-has-associated-control": "off",

    "react/prop-types": "off",
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "warn",
    "react/jsx-wrap-multilines": "warn",

    "@typescript-eslint/ban-ts-comment": "off",

    "@typescript-eslint/member-delimiter-style": ["error", {
      "multiline": {
          "delimiter": "none",
          "requireLast": true
      },
      "singleline": {
          "delimiter": "comma",
          "requireLast": false
      }
    }],
    "@typescript-eslint/no-empty-interface": "warn",

    "@typescript-eslint/semi": ["error", "never"],
    "@typescript-eslint/indent": ["error"],

    "no-underscore-dangle": "off",
    // вместо стандартного правила используем его аналог из Typescript
    "no-unused-expressions": "error",
    "@typescript-eslint/no-unused-expressions": "warn",
    "@typescript-eslint/no-unused-vars": "warn",
    "@typescript-eslint/explicit-function-return-type": "off",
    "@typescript-eslint/explicit-module-boundary-types": "off",

    "max-len": ["error", 120],
    "no-console": ["warn", { "allow": ["warn", "error", "info"] }],
    "no-debugger": "error",

    "optimize-regex/optimize-regex": "warn",

    "no-extend-native": "error",

    "import/no-cycle": "warn"
  },

  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "project": "./tsconfig.json"
  },

  "plugins": [
    "@typescript-eslint",
    "react-hooks",
    "optimize-regex",
    "prettier"
  ],

  "extends": [
    "plugin:@typescript-eslint/recommended",
    "airbnb-typescript",
    "prettier"
  ],

  "env": {
    "browser": true
  },

  "settings": {
    "import/resolver": {
      "typescript": {}
    }
  }
}
