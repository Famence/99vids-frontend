import React from 'react'
import cn from 'classnames'

const Icon = ({ className, glyph, isFilled, isInline, ...restProps }) => glyph && (
  <svg className={cn('icon', `icon-${glyph}`, { isFilled, isInline }, className)} {...restProps}>
    <use xlinkHref={`#${glyph}`} />
  </svg>
)

Icon.displayName = 'Icon'

export default Icon
