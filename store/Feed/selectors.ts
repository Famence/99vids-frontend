import { createSelector } from 'reselect'
import { RootState } from '@store/store'
import { FeedState } from '@store/Feed/model'
import { postsWithAuthorsSelector } from '@store/complexSelectors'

export const feedStateSelector: (state: RootState) => FeedState = (state) => state.feed

export const feedPostsSelector = createSelector(
  feedStateSelector,
  postsWithAuthorsSelector,
  (feed, posts) => (
    feed.posts.map(feedPostId => (
      posts[feedPostId]
    ))
  )
)

export const feedPostsGroupSelector = createSelector(
  feedStateSelector,
  (feed) => feed.group
)
