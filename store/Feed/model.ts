import { AxiosRequestConfig } from 'axios'
import { api } from '@api'
import { ModelConfig } from '@store/util'
import { PostsGroup, PostType } from '@models/Posts'
import { LoaderTypes } from '@store/Loaders/constants'
import { convertPostToStoredFormat, showError } from '@utils'
import { Pagination } from '@models/commonTypes'
import { DEFAULT_PAGINATION } from '@config/constants'
import { GetFeedParams } from '@api/api'
import { usersSelector } from '@store/Users/selectors'
import { RootState } from '@store/store'
import { User } from '@models/Users'

export interface FeedState {
  posts: PostType['_id'][]
  group: PostsGroup
  isNoMore: boolean
}

const initialFeedState: FeedState = {
  posts: [],
  group: PostsGroup.All,
  isNoMore: false,
}

export const feed: ModelConfig<FeedState> = {
  name: 'feed',
  state: initialFeedState,
  reducers: {
    setPosts(state, posts: PostType['_id'][]) {
      return {
        ...state,
        posts: Array.from(new Set(posts)),
      }
    },
    addPosts(state, posts: PostType['_id'][]) {
      return {
        ...state,
        posts: Array.from(new Set([...state.posts, ...posts])),
      }
    },
    setIsNoMore(state, isNoMore: boolean) {
      return {
        ...state,
        isNoMore,
      }
    },
    setPostsGroup(state, group: PostsGroup) {
      return {
        ...state,
        group,
      }
    },
  },
  // @ts-ignore
  effects: (dispatch) => ({
    async getFeed({
      group = PostsGroup.All,
      pagination: _pagination = DEFAULT_PAGINATION,
      requestConfig,
    }: {
      group?: PostsGroup
      pagination?: Partial<Pagination>
      requestConfig?: AxiosRequestConfig
    } = {}) {
      const pagination = { ...DEFAULT_PAGINATION, ..._pagination }

      try {
        const params: GetFeedParams = { ...pagination }

        switch (group) {
          case PostsGroup.Photos:
            params.withPhoto = true
            break
          case PostsGroup.Videos:
            params.withVideo = true
            break
          default:
            break
        }

        dispatch.loaders.showLoader(LoaderTypes.FeedLoader)
        const { data: { list: feedPosts } } = await api.getFeed(params, requestConfig)

        await dispatch.posts.addPosts({ posts: feedPosts, requestConfig })

        const postsIds = feedPosts.map(post => post._id)
        if (pagination.offset === 0)
          dispatch.feed.setPosts(postsIds)
        else
          dispatch.feed.addPosts(postsIds)

        dispatch.feed.setPostsGroup(group)
      } catch (error) {
        if (error.response?.status === 404) {
          dispatch.feed.setIsNoMore(true)
        }
        else
          showError(error)
      } finally {
        dispatch.loaders.hideLoader(LoaderTypes.FeedLoader)
      }
    },
  }),
}
