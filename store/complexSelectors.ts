import { createSelector } from 'reselect'
import { ObjectsByKey } from '@models/commonTypes'
import { User } from '@models/Users'
import { PostType } from '@models/Posts'
import { usersSelector } from '@store/Users/selectors'
import { postsSelector } from '@store/Posts/selectors'

export const usersWithPromoPostsSelector = createSelector(
  usersSelector,
  postsSelector,
  (users, posts) => {
    const modifiedUsers: ObjectsByKey<User<PostType>> = {}
    Object.entries(users).forEach(([userId, user]) => {
      modifiedUsers[userId] = {
        ...user,
        promoPost: posts[user.promoPost],
      }
    })
    return modifiedUsers
  }
)

export const postsWithAuthorsSelector = createSelector(
  postsSelector,
  usersSelector,
  (posts, users) => {
    const modifiedPosts: ObjectsByKey<PostType<User>> = {}
    Object.entries(posts).forEach(([postId, post]) => {
      modifiedPosts[postId] = {
        ...post,
        author: users[post.author],
      }
    })
    return modifiedPosts
  }
)
