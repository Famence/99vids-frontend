import * as Rematch from '@rematch/core'
import { Dispatch, RootState, Store } from '@store/store'

export type ModelConfig<ModelState> = Rematch.ModelConfig<ModelState, Store> & {
  effects?: (dispatch: Dispatch) => Rematch.ModelEffects<RootState>
}
