export enum ModalTypes {
  Authenticate = 'modal/Authenticate',
  ModelAvatarUploadDialog = 'modal/ModelAvatarUploadDialog',
  AvatarEditor = 'modal/AvatarEditor',
  SelectTariff = 'modal/SelectTariff',
  ConfirmUpgradeSubscription = 'modal/ConfirmUpgradeSubscription',
  SubscribeCongrats = 'modal/SubscribeCongrats',
  Tip = 'modal/Tip',
}
