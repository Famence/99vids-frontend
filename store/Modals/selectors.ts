import { RootState } from '@store/store'
import { ModalsState } from '@store/Modals/model'

export const modalsStateSelector = (state: RootState): ModalsState => state.modals
export const openedModalsSelector = modalsStateSelector
