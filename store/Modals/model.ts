import { ModelConfig } from '@rematch/core'
import { ModalTypes } from '@store/Modals/constants'
import { ModalProps } from '@components/Modal/Modal'

interface ModalData {
  type: ModalTypes
  [key: string]: any
}

export interface ModalsState {
  [key: string]: ModalData
}

export const modals: ModelConfig<ModalsState> = {
  state: {},
  reducers: {
    open<Props = Partial<ModalProps>>(state: ModalsState, modalData: (Props & { type: ModalTypes }) | ModalData) {
      return {
        ...state,
        [modalData.type]: modalData,
      }
    },
    close(state, type: ModalTypes) {
      const newState = { ...state }
      delete newState[type]
      return newState
    },
  },
}
