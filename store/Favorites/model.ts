import { AxiosRequestConfig } from 'axios'
import { RematchDispatch } from '@rematch/core'
import { ModelConfig } from '@store/util'
import { LoaderTypes } from '@store/Loaders/constants'
import { api } from '@api'
import { models } from '@store/models'
import { showError } from '@utils'
import { DEFAULT_USERS_PAGINATION } from '@config/constants'
import { FavoritesType, ID, Pagination, Sorting } from '@models/commonTypes'
import { RootState } from '@store/store'
import { Payment, PaymentPurpose } from '@models/Payment'
import { usersSelector } from '@store/Users/selectors'

export enum FavoriteCategory {
  FavoriteUsers = 'favoriteUsers',
  Tips = 'tips',
  LikedPosts = 'likedPosts',
  CommentedPosts = 'commentedPosts',
}

interface FavoriteCategoryState {
  ids: ID[]
  count: number | undefined
}

export interface FavoritesState {
  [FavoriteCategory.FavoriteUsers]: FavoriteCategoryState
  [FavoriteCategory.Tips]: {
    tips: Payment[]
    count: number | undefined
  }
  [FavoriteCategory.LikedPosts]: FavoriteCategoryState
  [FavoriteCategory.CommentedPosts]: FavoriteCategoryState
}

const defaultFavoriteCategory = {
  ids: [],
  count: undefined,
}

const initialFavoritesState: FavoritesState = {
  [FavoriteCategory.FavoriteUsers]: defaultFavoriteCategory,
  [FavoriteCategory.Tips]: {
    tips: [],
    count: undefined,
  },
  [FavoriteCategory.LikedPosts]: defaultFavoriteCategory,
  [FavoriteCategory.CommentedPosts]: defaultFavoriteCategory,
}

export const favorites: ModelConfig<FavoritesState> = {
  name: 'favorites',
  state: initialFavoritesState,
  reducers: {
    updateCategory: (state, { category, data }: {
      category: FavoriteCategory
      data: Partial<FavoritesState>
    }) => ({
      ...state,
      [category]: {
        ...state[category],
        ...data,
      },
    }),
  },
  // @ts-ignore
  effects: (dispatch: RematchDispatch<typeof models>) => ({
    async getFavoriteUsers({
      pagination: _pagination = DEFAULT_USERS_PAGINATION,
      requestConfig,
    }: { pagination?: Pagination, requestConfig?: AxiosRequestConfig }, state: RootState) {
      const pagination = { ...DEFAULT_USERS_PAGINATION, ..._pagination }

      try {
        dispatch.loaders.showLoader(LoaderTypes.FavoritesUsersLoader)
        const {
          data: { list: favoritesUsers, count },
        } = await api.getModels({ favorited: true, withSubscription: true, ...pagination }, requestConfig)
        dispatch.users.addUsers(favoritesUsers)

        const favoritesUsersIds = favoritesUsers.map((favoritesUser) => favoritesUser._id)

        dispatch.favorites.updateCategory({
          category: FavoriteCategory.FavoriteUsers,
          data: {
            count,
            ids: pagination.offset === 0 ? favoritesUsersIds : (
              [...state.favorites[FavoriteCategory.FavoriteUsers].ids, ...favoritesUsersIds]
            ),
          },
        })
      } catch (error) {
        if (error.response?.status === 404) {
          if (typeof state.favorites[FavoriteCategory.FavoriteUsers].offset === 'undefined') {
            dispatch.favorites.updateCategory({
              category: FavoriteCategory.FavoriteUsers,
              data: { count: 0 },
            })
          }
        } else {
          showError(error)
        }
        showError(error)
      } finally {
        dispatch.loaders.hideLoader(LoaderTypes.FavoritesUsersLoader)
      }
    },
    async getTips({
      sort = Sorting.ByCreatedAtAsc,
      pagination: _pagination = DEFAULT_USERS_PAGINATION,
      requestConfig,
    }: { sort: Sorting, pagination?: Pagination, requestConfig?: AxiosRequestConfig }, state: RootState) {
      const pagination = { ...DEFAULT_USERS_PAGINATION, ..._pagination }
      const users = usersSelector(state)

      try {
        dispatch.loaders.showLoader(LoaderTypes.TippedUsersLoader)
        const {
          data: { list: tips, count },
        } = await api.getPayments(PaymentPurpose.Tips, { sort, ...pagination }, requestConfig)

        const tippedUsersIdsToFetch: ID[] = []
        tips.forEach((tip) => {
          if (Object.keys(users).includes(tip.recipient)) return
          tippedUsersIdsToFetch.push(tip.recipient)
        })
        if (tippedUsersIdsToFetch.length) await dispatch.users.getModelsById(tippedUsersIdsToFetch)

        dispatch.favorites.updateCategory({
          category: FavoriteCategory.Tips,
          data: {
            count,
            tips: pagination.offset === 0 ? tips : (
              [...state.favorites[FavoriteCategory.Tips].tips, ...tips]
            ),
          },
        })
      } catch (error) {
        if (error.response?.status === 404) {
          if (typeof state.favorites[FavoriteCategory.FavoriteUsers].offset === 'undefined') {
            dispatch.favorites.updateCategory({
              category: FavoriteCategory.Tips,
              data: { count: 0 },
            })
          }
        } else {
          showError(error)
        }
      } finally {
        dispatch.loaders.hideLoader(LoaderTypes.TippedUsersLoader)
      }
    },
    async getLikedPosts({
      sort = Sorting.ByDateDesc,
      pagination: _pagination = DEFAULT_USERS_PAGINATION,
      requestConfig,
    }: { sort: Sorting, pagination?: Pagination, requestConfig?: AxiosRequestConfig }, state: RootState) {
      const pagination = { ...DEFAULT_USERS_PAGINATION, ..._pagination }

      try {
        dispatch.loaders.showLoader(LoaderTypes.LikedPostsLoader)
        const {
          data: { list: likedPosts, count },
        } = await api.getFeed({ liked: true, sort, ...pagination }, requestConfig)

        await dispatch.posts.addPosts({ posts: likedPosts, requestConfig })

        const likedPostsIds = likedPosts.map((post) => post._id)

        dispatch.favorites.updateCategory({
          category: FavoriteCategory.LikedPosts,
          data: {
            count,
            ids: pagination.offset === 0 ? likedPostsIds : (
              [...state.favorites[FavoriteCategory.LikedPosts].ids, ...likedPostsIds]
            ),
          },
        })
      } catch (error) {
        if (error.response?.status === 404) {
          if (typeof state.favorites[FavoriteCategory.FavoriteUsers].offset === 'undefined') {
            dispatch.favorites.updateCategory({
              category: FavoriteCategory.LikedPosts,
              data: { count: 0 },
            })
          }
        } else {
          showError(error)
        }
      } finally {
        dispatch.loaders.hideLoader(LoaderTypes.LikedPostsLoader)
      }
    },
    async getCommentedPosts({
      sort = Sorting.ByDateDesc,
      pagination: _pagination = DEFAULT_USERS_PAGINATION,
      requestConfig,
    }: { sort: Sorting, pagination?: Pagination, requestConfig?: AxiosRequestConfig }, state: RootState) {
      const pagination = { ...DEFAULT_USERS_PAGINATION, ..._pagination }

      try {
        dispatch.loaders.showLoader(LoaderTypes.CommentedPostsLoader)
        const {
          data: { list: commentedPosts, count },
        } = await api.getFeed({ commented: true, sort, ...pagination }, requestConfig)

        await dispatch.posts.addPosts({ posts: commentedPosts, requestConfig })

        const commentedPostsIds = commentedPosts.map((post) => post._id)

        dispatch.favorites.updateCategory({
          category: FavoriteCategory.CommentedPosts,
          data: {
            count,
            ids: pagination.offset === 0 ? commentedPostsIds : (
              [...state.favorites[FavoriteCategory.CommentedPosts].ids, ...commentedPostsIds]
            ),
          },
        })
      } catch (error) {
        if (error.response?.status === 404) {
          if (typeof state.favorites[FavoriteCategory.FavoriteUsers].offset === 'undefined') {
            dispatch.favorites.updateCategory({
              category: FavoriteCategory.CommentedPosts,
              data: { count: 0 },
            })
          }
        } else {
          showError(error)
        }
      } finally {
        dispatch.loaders.hideLoader(LoaderTypes.CommentedPostsLoader)
      }
    },
    async initialFetchByType({
      type,
      requestConfig,
    }: { type: FavoritesType, requestConfig?: AxiosRequestConfig }, state: RootState) {
      switch (type) {
        case FavoritesType.Favorite:
          if (typeof state.favorites[FavoriteCategory.FavoriteUsers].count === 'undefined') {
            await dispatch.favorites.getFavoriteUsers({ requestConfig })
          }
          break
        case FavoritesType.Tipped:
          if (typeof state.favorites[FavoriteCategory.Tips].count === 'undefined') {
            await dispatch.favorites.getTips({ requestConfig })
          }
          break
        case FavoritesType.Liked:
          if (typeof state.favorites[FavoriteCategory.LikedPosts].count === 'undefined') {
            await dispatch.favorites.getLikedPosts({ requestConfig })
          }
          break
        case FavoritesType.Commented:
          if (typeof state.favorites[FavoriteCategory.CommentedPosts].count === 'undefined') {
            await dispatch.favorites.getCommentedPosts({ requestConfig })
          }
          break
        default:
          break
      }
    },
  }),
}
