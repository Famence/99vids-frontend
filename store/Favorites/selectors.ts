import { createSelector } from 'reselect'
import { RootState } from '@store/store'
import { usersSelector } from '@store/Users/selectors'
import { FavoriteCategory, FavoritesState } from '@store/Favorites/model'
import { postsWithAuthorsSelector } from '@store/complexSelectors'

export const favoritesStateSelector = (state: RootState): FavoritesState => state.favorites

export const favoriteUsersSelector = createSelector(
  favoritesStateSelector,
  usersSelector,
  (favoritesState, users) => (
    favoritesState[FavoriteCategory.FavoriteUsers].ids.map((favoriteUserId) => users[favoriteUserId])
  )
)

export const favoriteUsersCountSelector = createSelector(
  favoritesStateSelector,
  (favoritesState) => favoritesState[FavoriteCategory.FavoriteUsers].count
)

export const tipsSelector = createSelector(
  favoritesStateSelector,
  (favoritesState) => favoritesState[FavoriteCategory.Tips].tips
)

export const tippedUsersSelector = createSelector(
  tipsSelector,
  usersSelector,
  (tips, users) => tips.map((tip) => users[tip.recipient])
)

export const tipsCountSelector = createSelector(
  favoritesStateSelector,
  (favoritesState) => favoritesState[FavoriteCategory.Tips].count
)

export const likedPostsSelector = createSelector(
  favoritesStateSelector,
  postsWithAuthorsSelector,
  (favoritesState, posts) => (
    favoritesState[FavoriteCategory.LikedPosts].ids.map((likedPostId) => posts[likedPostId])
  )
)

export const likedPostsCountSelector = createSelector(
  favoritesStateSelector,
  (favoritesState) => favoritesState[FavoriteCategory.LikedPosts].count
)

export const commentedPostsSelector = createSelector(
  favoritesStateSelector,
  postsWithAuthorsSelector,
  (favoritesState, posts) => (
    favoritesState[FavoriteCategory.CommentedPosts].ids.map((commentedPostId) => posts[commentedPostId])
  )
)

export const commentedPostsCountSelector = createSelector(
  favoritesStateSelector,
  (favoritesState) => favoritesState[FavoriteCategory.CommentedPosts].count
)
