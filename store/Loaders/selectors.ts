import { useSelector } from 'react-redux'
import { RootState } from '@store/store'
import { LoaderTypes } from './constants'

export const useLoaderSelector = (loader: LoaderTypes): boolean => (
  useSelector<RootState, boolean>((state) => !!state.loaders[loader])
)
