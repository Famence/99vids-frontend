import { ModelConfig } from '@store/util'
import { LoaderTypes } from './constants'

const initialState = {}

export type LoadersState = {
  [loaderType: string]: boolean | undefined
}

export const loaders: ModelConfig<LoadersState> = {
  state: initialState,
  reducers: {
    showLoader: (state, loaderType: LoaderTypes) => ({ ...state, [loaderType]: true }),
    hideLoader: (state, loaderType: LoaderTypes) => ({ ...state, [loaderType]: false }),
  },
}
