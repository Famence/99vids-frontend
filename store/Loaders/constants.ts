export enum LoaderTypes {
  UsersLoader = 'UsersLoader',
  UserPostsLoader = 'UserPostsLoader',
  FeedLoader = 'FeedLoader',
  RecommendedLoader = 'RecommendedLoader',
  FollowingUsersLoader = 'FollowingUsersLoader',
  FavoritesUsersLoader = 'FavoritesUsersLoader',
  TippedUsersLoader = 'TippedUsersLoader',
  LikedPostsLoader = 'LikedPostsLoader',
  CommentedPostsLoader = 'CommentedPostsLoader',
  RejectReasonsLoader = 'References/RejectReasonsLoader',
}
