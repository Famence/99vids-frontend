import { init, RematchDispatch, RematchRootState, RematchStore } from '@rematch/core'
import { models } from './models'

export type RootState = RematchRootState<typeof models>
export type Store = RematchStore<typeof models>
export type Dispatch = RematchDispatch<typeof models>

export const initStore = (initialState?: RootState): Store => init({
  models,
  redux: {
    initialState,
    devtoolOptions: {},
  },
  // plugins: [selectPlugin()],
})
