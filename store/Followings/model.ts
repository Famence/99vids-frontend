import { RematchDispatch } from '@rematch/core'
import { ModelConfig } from '@store/util'
import { LoaderTypes } from '@store/Loaders/constants'
import { api } from '@api'
import { models } from '@store/models'
import { showError } from '@utils'
import { AxiosRequestConfig } from 'axios'
import { StoredUserType } from '@store/Users/model'
import { FollowingStatus } from '@models/Subscription'
import { DEFAULT_USERS_PAGINATION } from '@config/constants'
import { Pagination } from '@models/commonTypes'
import { GetFollowingParams } from '@api/api'

export interface FollowingsState {
  users: StoredUserType['_id'][]
  fetchedStatus: FollowingStatus | null
  count: number | undefined
}

const initialFollowingsState: FollowingsState = {
  users: [],
  fetchedStatus: null,
  count: undefined,
}

export const followings: ModelConfig<FollowingsState> = {
  name: 'followings',
  state: initialFollowingsState,
  reducers: {
    setUsers: (state, userIds: StoredUserType['_id'][]) => ({
      ...state,
      users: Array.from(new Set(userIds)),
    }),
    addUsers: (state, userIds: StoredUserType['_id'][]) => ({
      ...state,
      users: Array.from(new Set([...state.users, ...userIds])),
    }),
    setCount: (state, count: number) => ({
      ...state,
      count,
    }),
    setFetchedStatus: (state, fetchedStatus: FollowingStatus) => ({
      ...state,
      fetchedStatus,
    }),
  },
  // @ts-ignore
  effects: (dispatch: RematchDispatch<typeof models>) => ({
    async getFollowing({
      status,
      pagination: _pagination = DEFAULT_USERS_PAGINATION,
      requestConfig,
    }: { status: FollowingStatus, pagination?: Pagination, requestConfig?: AxiosRequestConfig }) {
      const pagination = { ...DEFAULT_USERS_PAGINATION, ..._pagination }

      const requestParams: GetFollowingParams = { ...pagination }
      switch (status) {
        default:
        case FollowingStatus.All:
          requestParams[FollowingStatus.Subscribed] = true
          requestParams[FollowingStatus.Unsubscribed] = true
          break
        case FollowingStatus.Subscribed:
          requestParams.active = true
          requestParams[FollowingStatus.Subscribed] = true
          requestParams[FollowingStatus.Unsubscribed] = true
          break
        case FollowingStatus.Unsubscribed:
          requestParams[FollowingStatus.Subscribed] = true
          requestParams[FollowingStatus.Unsubscribed] = true
          break
      }

      try {
        dispatch.loaders.showLoader(LoaderTypes.FollowingUsersLoader)
        const {
          data: { list: followingUsers, count },
        } = await api.getFollowing(requestParams, requestConfig)
        dispatch.users.addUsers(followingUsers)

        const followingUsersIds = followingUsers.map((followingUser) => followingUser._id)

        if (pagination.offset === 0) {
          dispatch.followings.setUsers(followingUsersIds)
        } else {
          dispatch.followings.addUsers(followingUsersIds)
        }
        dispatch.followings.setCount(count)
        dispatch.followings.setFetchedStatus(status)
      } catch (error) {
        if (error.response?.status === 404) {
          if (pagination.offset === 0) {
            dispatch.followings.setUsers([])
            dispatch.followings.setCount(0)
          }
        } else {
          showError(error)
        }
      } finally {
        dispatch.loaders.hideLoader(LoaderTypes.FollowingUsersLoader)
      }
    },
  }),
}
