import { createSelector } from 'reselect'
import { RootState } from '@store/store'
import { FollowingsState } from '@store/Followings/model'
import { usersSelector } from '@store/Users/selectors'

export const followingsStateSelector = (state: RootState): FollowingsState => state.followings

export const followingUsersSelector = createSelector(
  followingsStateSelector,
  usersSelector,
  (followingsState, users) => (
    followingsState.users.map((followingUserId) => users[followingUserId])
  )
)

export const followingCountSelector = createSelector(
  followingsStateSelector,
  usersSelector,
  (followingsState) => followingsState.count
)

export const followingsFetchedStatusSelector = createSelector(
  followingsStateSelector,
  usersSelector,
  (followingsState) => followingsState.fetchedStatus
)
