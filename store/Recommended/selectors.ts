import { createSelector } from 'reselect'
import { RootState } from '@store/store'
import { RecommendedState } from '@store/Recommended/model'
import { usersSelector } from '@store/Users/selectors'
import { postsWithAuthorsSelector } from '@store/complexSelectors'

export const recommendedStateSelector: (state: RootState) => RecommendedState = (state) => state.recommended

export const recommendedPostsSelector = createSelector(
  recommendedStateSelector,
  postsWithAuthorsSelector,
  (recommended, posts) => (
    recommended.posts.map(recommendedPostId => (
      posts[recommendedPostId]
    ))
  )
)

export const recommendedUsersSelector = createSelector(
  recommendedStateSelector,
  usersSelector,
  (recommended, users) => (
    recommended.users.map(recommendedUserId => (
      users[recommendedUserId]
    ))
  )
)
