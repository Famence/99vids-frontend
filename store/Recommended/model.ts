import { AxiosRequestConfig } from 'axios'
import { api } from '@api'
import { ModelConfig } from '@store/util'
import { User } from '@models/Users'
import { FetchedPostType, PostType } from '@models/Posts'
import { LoaderTypes } from '@store/Loaders/constants'
import { convertPostToStoredFormat, showError } from '@utils'
import { Pagination } from '@models/commonTypes'
import { DEFAULT_PAGINATION } from '@config/constants'

export interface RecommendedState {
  posts: PostType['_id'][]
  users: User['_id'][]
  isNoMore: boolean
}

const initialRecommendedState: RecommendedState = {
  posts: [],
  users: [],
  isNoMore: false,
}

export const recommended: ModelConfig<RecommendedState> = {
  name: 'recommended',
  state: initialRecommendedState,
  reducers: {
    setPosts(state, posts: PostType['_id'][]) {
      return {
        ...state,
        posts: Array.from(new Set(posts)),
      }
    },
    addPosts(state, posts: PostType['_id'][]) {
      return {
        ...state,
        posts: Array.from(new Set([...state.posts, ...posts])),
      }
    },
    setUsers(state, users: User['_id'][]) {
      return {
        ...state,
        users: Array.from(new Set(users)),
      }
    },
    addUsers(state, users: User['_id'][]) {
      return {
        ...state,
        users: Array.from(new Set([...state.users, ...users])),
      }
    },
    setIsNoMore(state, isNoMore: boolean) {
      return {
        ...state,
        isNoMore,
      }
    },
  },
  // @ts-ignore
  effects: (dispatch) => ({
    async getRecommended({
      pagination: _pagination = DEFAULT_PAGINATION,
      requestConfig,
    }: { pagination?: Partial<Pagination>, requestConfig?: AxiosRequestConfig } = {}) {
      try {
        const pagination = { ...DEFAULT_PAGINATION, ..._pagination }
        dispatch.loaders.showLoader(LoaderTypes.RecommendedLoader)
        const {
          data: { list: recommendedModelsWithPromos },
        } = await api.getRecommended({ ...pagination }, requestConfig)

        dispatch.users.addUsers(recommendedModelsWithPromos.map((user) => ({
          ...user,
          promoPost: user.promoPost?._id,
        })))

        const usersIds = recommendedModelsWithPromos.map((model: User) => model._id)
        if (pagination.offset === 0)
          dispatch.recommended.setUsers(usersIds)
        else
          dispatch.recommended.addUsers(usersIds)

        const posts: FetchedPostType[] = []
        recommendedModelsWithPromos.forEach((model: User<FetchedPostType>) => {
          if (model.promoPost) posts.push(model.promoPost)
        })
        await dispatch.posts.addPosts({ posts, requestConfig })

        const postsIds = posts.map(post => post._id)
        if (pagination.offset === 0)
          dispatch.recommended.setPosts(postsIds)
        else
          dispatch.recommended.addPosts(postsIds)
      } catch (error) {
        if (error.response?.status === 404) {
          dispatch.recommended.setIsNoMore(true)
          // TODO: как-то сохранять тот факт, что рекомендации закончились.
          //  Сказать об этом юзеру и предотвратить дальнейшие запросы
        }
        else
          showError(error)
      } finally {
        dispatch.loaders.hideLoader(LoaderTypes.RecommendedLoader)
      }
    },
  }),
}
