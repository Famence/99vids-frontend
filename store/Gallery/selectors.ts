import { RootState } from '@store/store'
import { GalleryState } from '@store/Gallery/model'

export const galleryStateSelector: (state: RootState) => GalleryState = (state) => state.gallery
