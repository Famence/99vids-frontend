import { ModelConfig } from '@store/util'
import { RootState } from '@store/store'
import { PostMediaPhotoItem, PostType } from '@models/Posts'
import { postsSelector } from '@store/Posts/selectors'
import { ID } from '@models/commonTypes'
import { sortByPosition } from '@utils'

export interface GalleryState {
  isOpen: boolean
  postId: PostType['_id'] | null
  postPhotos: PostMediaPhotoItem[]
  photoIndex: number
}

const initialState: GalleryState = {
  isOpen: false,
  postId: null,
  postPhotos: [],
  photoIndex: 0,
}

interface OpenArgs {
  postId: PostType['_id']
  mediaItemId: ID
}

export const gallery: ModelConfig<GalleryState> = {
  state: initialState,
  reducers: {
    setIsOpen: (state, isOpen: boolean) => ({ ...state, isOpen }),
    setPostId: (state, postId: GalleryState['postId']) => ({ ...state, postId }),
    setPhotoIndex: (state, photoIndex: number) => ({ ...state, photoIndex }),
    setPostPhotos: (state, postPhotos: PostMediaPhotoItem[]) => ({ ...state, postPhotos }),
  },
  // @ts-ignore
  effects: (dispatch) => ({
    async open({ postId, mediaItemId }: OpenArgs, state: RootState) {
      const posts = postsSelector(state)
      const post = posts[postId]
      if (!post.media?.photo.length) return
      const postMediaPhotos = sortByPosition(post.media.photo)
      dispatch.gallery.setIsOpen(true)
      dispatch.gallery.setPostId(postId)
      dispatch.gallery.setPostPhotos(postMediaPhotos)
      dispatch.gallery.setPhotoIndex(postMediaPhotos.findIndex(photo => photo._id === mediaItemId))
    },
    async close() {
      dispatch.gallery.setIsOpen(false)
      dispatch.gallery.setPostId(null)
      dispatch.gallery.setPostPhotos([])
      dispatch.gallery.setPhotoIndex(0)
    },
  }),
}
