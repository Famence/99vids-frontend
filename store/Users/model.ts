import { AxiosRequestConfig } from 'axios'
import { toast } from 'react-toastify'
import { User, UserRoles } from '@models/Users'
import { ObjectsByKey } from '@models/commonTypes'
import { PostType } from '@models/Posts'
import { ModelConfig } from '@store/util'
import { LoaderTypes } from '@store/Loaders/constants'
import { arrayToObjectsByKeys } from '@utils/helpers'
import { currentUserSelector, usersSelector } from '@store/Users/selectors'
import { RootState } from '@store/store'
import { api } from '@api'
import { TariffType } from '@models/TariffType'
import { ModalTypes } from '@store/Modals/constants'
import { AuthenticateModalProps } from '@components/Modal/AuthenticateModal/AuthenticateModal'
import { RematchDispatch } from '@rematch/core'
import { models } from '@store/models'
import { showError } from '@utils'
import { FollowingStatus } from '@models/Subscription'
import { followingCountSelector } from '@store/Followings/selectors'
import { GetInitialDataConfig } from '@models/Pages'
import _ from 'lodash'

export type StoredUserType = User<PostType['_id']>

const getDisplayName = (user: StoredUserType): string => user.firstname || user.nickname || user._id
const getLink = (user: StoredUserType): string => `/${user.nickname || user._id}`

export interface UsersState {
  currentUserId: StoredUserType['_id'] | null
  isCurrentUserFetched: boolean
  users: ObjectsByKey<StoredUserType>
  favorites: StoredUserType['_id'][]
  tipped: StoredUserType['_id'][]
}

const initialUsersState: UsersState = {
  currentUserId: null,
  isCurrentUserFetched: false,
  users: {},
}

export const users: ModelConfig<UsersState> = {
  name: 'users',
  state: initialUsersState,
  reducers: {
    setCurrentUser(state, currentUser: StoredUserType) {
      return {
        ...state,
        currentUserId: currentUser._id,
        users: {
          ...state.users,
          [currentUser._id]: {
            ...currentUser,
            displayName: getDisplayName(currentUser),
            link: getLink(currentUser),
          },
        },
      }
    },
    setIsCurrentUserFetched(state, isCurrentUserFetched: boolean) {
      return {
        ...state,
        isCurrentUserFetched,
      }
    },
    setUsers(state, newUsers: StoredUserType[]) {
      const byIds: ObjectsByKey<StoredUserType> = arrayToObjectsByKeys(newUsers, '_id', (user) => ({
        ...user,
        displayName: getDisplayName(user),
        link: getLink(user),
      }))

      return {
        ...state,
        users: byIds,
      }
    },
    addUsers(state, newUsers: StoredUserType[]) {
      const byIds: ObjectsByKey<StoredUserType> = arrayToObjectsByKeys(newUsers, '_id', (user) => ({
        ...user,
        displayName: getDisplayName(user),
        link: getLink(user),
      }))

      return {
        ...state,
        users: { ...state.users, ...byIds },
      }
    },
  },
  // @ts-ignore
  effects: (dispatch: RematchDispatch<typeof models>) => ({
    async getCurrentUser(config) {
      try {
        dispatch.loaders.showLoader(LoaderTypes.UsersLoader)
        const { data: user } = await api.getCurrentUser(config)
        dispatch.users.setCurrentUser(user)
      } catch (error) {
        showError(error)
      } finally {
        dispatch.loaders.hideLoader(LoaderTypes.UsersLoader)
        dispatch.users.setIsCurrentUserFetched(true)
      }
    },
    async getModelById(userId: User['_id'], config) {
      try {
        dispatch.loaders.showLoader(LoaderTypes.UsersLoader)
        const { data: user } = await api.getModelById(userId, config)
        dispatch.users.addUsers([user])
      } catch (error) {
        showError(error)
      } finally {
        dispatch.loaders.hideLoader(LoaderTypes.UsersLoader)
      }
    },
    async getModelsById(userIds: User['_id'][], config) {
      try {
        dispatch.loaders.showLoader(LoaderTypes.UsersLoader)
        const { data: { list: users } } = await api.getModels({ id: userIds.join(',') }, config)
        dispatch.users.addUsers(users)
      } catch (error) {
        showError(error)
      } finally {
        dispatch.loaders.hideLoader(LoaderTypes.UsersLoader)
      }
    },
    async subscribe({
      modelId,
      tariff: _tariff,
      onSubscribe,
      // requestConfig,
    }: { modelId: User['_id'], tariff?: TariffType, onSubscribe?: (tariff: TariffType) => void }, state: RootState) {
      const model = usersSelector(state)[modelId]
      if (model.roleId !== UserRoles.MODEL) return

      async function requestSubscribe(tariff: TariffType) {
        try {
          const { data } = await api.subscribe({ modelId, tariff })
          if (data.redirect)
            document.location = data.redirect
          else {
            dispatch.followings.addUsers([modelId])
            dispatch.followings.setCount(followingCountSelector(state) + 1)
            if (onSubscribe) onSubscribe(tariff)
          }
        } catch (error) {
          console.error(error)
          toast.error(error.statusCode === 500 ? (
            `You were not subscribed to ${model.displayName} due to server error.` +
            'Please try again'
          ) : error)
        }
      }

      function ensureTariff(tariff: TariffType = _tariff) {
        if (!tariff) {
          dispatch.modals.open({
            type: ModalTypes.SelectTariff,
            model,
            onSubmit: (submittedTariff: TariffType) => requestSubscribe(submittedTariff),
          })
        } else
          requestSubscribe(tariff)
      }

      const currentUser = currentUserSelector(state)
      if (!currentUser)
        dispatch.users.authenticate({ model, registerIfNot: true, onSuccess: () => { ensureTariff(_tariff) } })
      else
        ensureTariff(_tariff)
    },
    async sendTip({
      modelId,
      sum: _sum,
      message: _message = '',
      postId,
      onTip,
      // requestConfig,
    }: {
      modelId: User['_id']
      sum?: number
      message?: string
      postId?: PostType['_id']
      onTip?: (sum: number, message: string) => void
    }, state: RootState) {
      const model = usersSelector(state)[modelId]
      if (model.roleId !== UserRoles.MODEL) return

      async function requestSendTip(sum: number, message: string) {
        try {
          const { data } = await api.sendTip({ modelId, sum, message, postId })
          if (data.redirect)
            document.location = data.redirect
          else {
            dispatch.users.addTipped(modelId)
            if (onTip) onTip(sum, message)
          }
        } catch (error) {
          console.error(error)
          toast.error(error.statusCode === 500 ? (
            `Sorry, we couldn't process your tip to ${model.displayName} due to server error.` +
            'Please try again'
          ) : error)
        }
      }

      function ensureSum(sum: number = _sum) {
        if (!sum) {
          dispatch.modals.open({
            type: ModalTypes.Tip,
            model,
            onSubmit: (values) => requestSendTip(values.sum, values.message),
          })
        } else
          requestSendTip(sum, _message)
      }

      const currentUser = currentUserSelector(state)
      if (!currentUser)
        dispatch.users.authenticate({ model, registerIfNot: true, onSuccess: () => { ensureSum(_sum) } })
      else
        ensureSum(_sum)
    },
    async authenticate({ email: _email, password: _password, model, registerIfNot = true, onSuccess: _onSuccess }: {
      email?: string
      password?: string
      model: User
      registerIfNot?: boolean
      onSuccess?: () => void
    }, state: RootState) {
      const currentUser = currentUserSelector(state)
      if (currentUser) return

      async function onSuccess() {
        if (_onSuccess) _onSuccess()
        await dispatch.users.getInitialDataForCurrentUser()
      }

      async function requestAuthentication(email: string, password: string) {
        try {
          const { data: user } = await api.authenticateUser({ email, password })
          dispatch.users.setCurrentUser(user)
          toast.success('Successfully logged in')
          await onSuccess()
        } catch (error) {
          if (registerIfNot) {
            await dispatch.users.register({ email, password, checkPassword: password, roleId: UserRoles.FAN })
            toast.success('Successfully signed up')
            await onSuccess()
          } else
            showError(error)
        }
      }

      if (_email && _password)
        await requestAuthentication(_email, _password)
      else {
        dispatch.modals.open<AuthenticateModalProps>({
          type: ModalTypes.Authenticate,
          model,
          formProps: {
            async onSubmit(values, { setSubmitting }) {
              await requestAuthentication(values.email, values.password)
              setSubmitting(false)
              dispatch.modals.close(ModalTypes.Authenticate)
            },
          },
        })
      }
    },
    async register(credentials: api.RegisterUserRequestBody, state: RootState) {
      const currentUser = currentUserSelector(state)
      if (currentUser) return

      try {
        await api.registerUser(credentials)
      } catch (error) {
        showError(error)
        throw error
      }
    },
    async requestRestoreAccess(credentials: api.RequestRestoreAccessBody, state: RootState) {
      const currentUser = currentUserSelector(state)
      if (currentUser) return

      try {
        await api.requestRestoreAccess(credentials)
        toast.success(`Check your email address: ${credentials.email}`)
      } catch (error) {
        showError(error)
        throw error
      }
    },
    async updateProfile(values: api.UpdateProfileBody, state: RootState) {
      try {
        await api.updateProfile(values)
        toast.success('Your profile is successfully updated')
      } catch (error) {
        showError(error)
        throw error
      }
    },
    async uploadAvatar(avatar: File, state: RootState) {
      if (!avatar) {
        dispatch.modals.open({
          type: ModalTypes.ModelAvatarUploadDialog,
          onImageSelect(file: File) {
            dispatch.modals.open({
              type: ModalTypes.AvatarEditor,
              file,
              onSubmit: console.log,
            })
          },
        })
      }
    },
    async changePassword(values: api.ChangePasswordBody, state: RootState) {
      try {
        await api.changePassword(values)
        toast.success('Password change success')
      } catch (error) {
        showError(error)
        throw error
      }
    },
    async getInitialDataForCurrentUser(configs: {
      getInitialDataConfig: GetInitialDataConfig
      requestConfig?: AxiosRequestConfig
    }, state: RootState) {
      const {
        getInitialDataConfig: {
          requestFollowing = true,
        },
        requestConfig,
      } = _.defaultsDeep(configs, {
        getInitialDataConfig: {
          requestFollowing: true,
        },
      })
      const currentUser = currentUserSelector(state)
      if (!currentUser) return

      const requests = []

      switch (currentUser.roleId) {
        case UserRoles.FAN:
          if (requestFollowing) {
            requests.push(dispatch.followings.getFollowing({
              status: FollowingStatus.Subscribed,
              requestConfig,
            }))
          }
          break
        case UserRoles.MODEL:
          // requests.push(dispatch.followers.getFollowers({
          //   status: SubscriptionStatus.Subscribed,
          //   requestConfig,
          // }))
          break
        default:
          break
      }

      await Promise.all(requests)
    },
  }),
}
