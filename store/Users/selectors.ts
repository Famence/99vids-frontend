import { createSelector } from 'reselect'
import { UserRoles } from '@models/Users'
import { RootState } from '@store/store'
import { StoredUserType, UsersState } from '@store/Users/model'

export const usersStateSelector = (state: RootState): UsersState => state.users

export const usersSelector = createSelector(
  usersStateSelector,
  (usersState) => usersState.users
)

export const currentUserIdSelector = createSelector(
  usersStateSelector,
  (usersState) => usersState.currentUserId
)

export const currentUserSelector = createSelector(
  currentUserIdSelector,
  usersSelector,
  (_id, users) => _id ? users[_id] : null
)

export const isCurrentUserFetchedSelector = createSelector(
  usersStateSelector,
  (usersState) => usersState.isCurrentUserFetched
)

export const userRoleSelector = (user: StoredUserType | null) => user?.roleId || UserRoles.GUEST
export const userNicknameSelector = (user: StoredUserType | null) => user?.nickname

export const currentUserRoleSelector = createSelector(
  currentUserSelector,
  userRoleSelector
)

export const currentUserNicknameSelector = createSelector(
  currentUserSelector,
  userNicknameSelector
)

export const usersListSelector = createSelector(
  usersSelector,
  (users) => Object.values(users)
)
