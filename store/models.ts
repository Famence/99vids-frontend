import { users } from './Users/model'
import { posts } from './Posts/model'
import { recommended } from './Recommended/model'
import { followings } from './Followings/model'
import { favorites } from './Favorites/model'
import { feed } from './Feed/model'
import { gallery } from './Gallery/model'
import { modals } from './Modals/model'
import { references } from './References/model'
import { loaders } from './Loaders/model'

export const models = {
  users,
  posts,
  recommended,
  followings,
  // followers,
  favorites,
  feed,
  gallery,
  modals,
  references,
  loaders,
}
