import { createSelector } from 'reselect'
import { RootState } from '@store/store'
import { PostsState } from '@store/Posts/model'

export const postsStateSelector = (state: RootState): PostsState => state.posts

export const postsSelector = createSelector(
  postsStateSelector,
  (postsState) => postsState.posts
)

export const usersPostsSelector = createSelector(
  postsStateSelector,
  (postsState) => postsState.usersPosts
)
