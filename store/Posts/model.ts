import { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios'
import { toast } from 'react-toastify'
import { FetchedPostType, PostsGroup, PostType } from '@models/Posts'
import { ModelConfig } from '@store/util'
import { ObjectsByKey, Pagination, Sorting } from '@models/commonTypes'
import { User } from '@models/Users'
import {
  arrayToObjectsByKeys,
  promiseAllSettledByKey,
  PromiseAllSettledByKeyItem,
  PromiseAllSettledMap,
} from '@utils/helpers'
import { RootState } from '@store/store'
import { currentUserSelector, usersSelector } from '@store/Users/selectors'
import { postsSelector } from '@store/Posts/selectors'
import { api } from '@api'
import { convertPostToStoredFormat, showError } from '@utils'
import { DEFAULT_PAGINATION } from '@config/constants'
import { LoaderTypes } from '@store/Loaders/constants'
import { GetUserPostResponse, GetUserPostsParams, GetUserPostsResponse } from '@api/api'

export type StoredPostType = PostType<User['_id']>

export interface UserPostsState<TypeOfPost = StoredPostType['_id']> {
  group: PostsGroup
  posts: TypeOfPost[]
  totalPostsOfGroup: number
}

export type UsersPostsState<TypeOfPost = StoredPostType['_id']> = Record<User['_id'], UserPostsState<TypeOfPost>>

export interface PostsState {
  usersPosts: UsersPostsState
  posts: ObjectsByKey<StoredPostType>
}

const initialPostsState: PostsState = {
  usersPosts: {},
  posts: {},
}

export const posts: ModelConfig<PostsState> = {
  name: 'posts',
  state: initialPostsState,
  reducers: {
    _addPosts(state, newPosts: StoredPostType[]) {
      const byIds = arrayToObjectsByKeys<StoredPostType>(newPosts, '_id')
      return {
        ...state,
        posts: { ...state.posts, ...byIds },
      }
    },
    addUserPosts: (state, { userId, group, postIds, totalPostsOfGroup }: {
      userId: User['_id']
      group: PostsGroup
      postIds: StoredPostType['_id'][]
      totalPostsOfGroup: number
    }) => ({
      ...state,
      usersPosts: {
        [userId]: {
          group,
          posts: Array.from(new Set([
            ...(state.usersPosts[userId]?.posts || []),
            ...postIds,
          ])),
          totalPostsOfGroup,
        },
      },
    }),
    setUserPosts: (state, { userId, group, postIds, totalPostsOfGroup }: {
      userId: User['_id']
      group: PostsGroup
      postIds: StoredPostType['_id'][]
      totalPostsOfGroup: number
    }) => ({
      ...state,
      usersPosts: {
        ...state.usersPosts,
        [userId]: {
          group,
          posts: Array.from(new Set(postIds)),
          totalPostsOfGroup,
        },
      },
    }),
    updatePost(state, post: StoredPostType) {
      const postToUpdate = state.posts[post._id]
      if (!postToUpdate) return state
      return {
        ...state,
        posts: {
          ...state.posts,
          [postToUpdate._id]: post,
        },
      }
    },
    setLiked(state, { postId, liked }: { postId: PostType['_id'], liked: boolean }) {
      const post = state.posts[postId]
      if (!post) return state
      return {
        ...state,
        posts: {
          ...state.posts,
          [post._id]: {
            ...post,
            liked,
            statistic: {
              ...post.statistic,
              likesCount: post.statistic.likesCount + (liked ? 1 : -1),
            },
          },
        },
      }
    },
  },
  // @ts-ignore
  effects: (dispatch) => ({
    async addPosts({
      posts: postsToAdd,
      requestConfig,
    }: { posts: FetchedPostType[], requestConfig: AxiosRequestConfig }, state: RootState) {
      const convertedPostsToAdd = postsToAdd.map(convertPostToStoredFormat)

      const fetchedUsers = usersSelector(state)
      let usersToFetch: User['_id'][] = []
      convertedPostsToAdd.forEach((post) => {
        if (!fetchedUsers[post.author]) usersToFetch.push(post.author)
      })
      usersToFetch = Array.from(new Set(usersToFetch))
      if (usersToFetch.length) {
        await dispatch.users.getModelsById(usersToFetch, requestConfig)
      }

      dispatch.posts._addPosts(convertedPostsToAdd)
    },
    async toggleLike(postId: PostType['_id'], state: RootState) {
      const post = postsSelector(state)[postId]
      if (!post) return

      const currentUser = currentUserSelector(state)
      if (!currentUser || !post.subscribed)
        if (!await dispatch.users.subscribe({ modelId: post.author })) return
      const targetState = !post.liked
      dispatch.posts.setLiked({ postId: post._id, liked: targetState })
      try {
        const { data: { liked } } = await api.toggleLike({ id: post._id })
        if (liked !== targetState) dispatch.posts.setLiked({ postId: post._id, liked })
      } catch (error) {
        console.error(error)
        const author = usersSelector(state)[post.author]
        toast.error(
          `Your like to ${author.displayName}'s post wasn't set due to server error.\n` +
          'Please try again'
        )
      }
    },
    async getUserPosts({
      userId,
      group = PostsGroup.All,
      pagination: _pagination = DEFAULT_PAGINATION,
      sort = Sorting.ByDateDesc,
      withPromo = true,
      requestConfig,
    }: {
      userId: User['_id']
      pagination?: Partial<Pagination>
      group?: PostsGroup
      sort?: Sorting
      withPromo: boolean
      requestConfig?: AxiosRequestConfig
    }) {
      const pagination = { ...DEFAULT_PAGINATION, ..._pagination }
      dispatch.loaders.showLoader(LoaderTypes.FavoritesUsersLoader)

      const params: GetUserPostsParams = { ...pagination, sort }
      switch (group) {
        case PostsGroup.Photos:
          params.withPromo = withPromo
          params.withPhoto = true
          break
        case PostsGroup.Videos:
          params.withPromo = withPromo
          params.withVideo = true
          break
        default:
          break
      }

      const fetchStandalonePromoPost = group === PostsGroup.All && withPromo

      try {
        const requests: PromiseAllSettledMap = {
          posts: api.getUserPosts({ userId }, params, requestConfig),
        }

        if (fetchStandalonePromoPost)
          requests.promo = api.getUserPost({ userId, postId: 'promo' }, requestConfig)

        const requestsResult = await promiseAllSettledByKey<{
          posts: PromiseAllSettledByKeyItem<AxiosResponse<GetUserPostsResponse> | AxiosError>
          promo: PromiseAllSettledByKeyItem<AxiosResponse<GetUserPostResponse> | AxiosError>
        }>(requests)

        const newPosts: StoredPostType[] = []
        const postIds: PostType['_id'][] = []
        let totalPostsOfGroup = 0

        if (requestsResult.posts.isResolved) {
          const response = (requestsResult.posts.value as AxiosResponse<GetUserPostsResponse>).data
          totalPostsOfGroup = response.count
          response.list.forEach((post) => {
            const convertedPost = convertPostToStoredFormat(post)
            newPosts.push(convertedPost)
            postIds.push(convertedPost._id)
          })
        } else {
          const error = requestsResult.posts.value as AxiosError
          if (error.response?.status === 404) {
            if (pagination.offset === 0)
              dispatch.posts.setUserPosts({ userId, group, postIds: [], totalPostsOfGroup: 0 })
            else
              dispatch.posts.addUserPosts({ userId, group, postIds: [] })
          } else
            showError(error)
        }

        if (fetchStandalonePromoPost) {
          if (requestsResult.promo.isResolved) {
            const { data: promoPost } = requestsResult.promo.value as AxiosResponse<GetUserPostResponse>
            const convertedPromoPost = convertPostToStoredFormat(promoPost)
            newPosts.unshift(convertedPromoPost)
            postIds.unshift(convertedPromoPost._id)
          } else {
            const error = requestsResult.promo.value as AxiosError
            if (error.response?.status !== 404) showError(error)
          }
        }

        dispatch.posts._addPosts(newPosts)
        if (pagination.offset === 0)
          dispatch.posts.setUserPosts({ userId, group, totalPostsOfGroup, postIds })
        else
          dispatch.posts.addUserPosts({ userId, group, totalPostsOfGroup, postIds })
      } finally {
        dispatch.loaders.hideLoader(LoaderTypes.FavoritesUsersLoader)
      }
    },
  }),
}
