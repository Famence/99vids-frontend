import { createSelector } from 'reselect'
import { RootState } from '@store/store'
import { ReferencesState } from '@store/References/model'

export const referencesSelectors: (state: RootState) => ReferencesState = (state) => state.references

export const rejectReasonsSelector = createSelector(
  referencesSelectors,
  (references) => references.rejectReasons
)

export const minTipSelector = createSelector(
  referencesSelectors,
  (references) => references.minTip
)
