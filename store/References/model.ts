import { ModelConfig } from '@store/util'
import { LoaderTypes } from '@store/Loaders/constants'
import { api } from '@api'
import { RootState } from '@store/store'
import { minTipSelector, rejectReasonsSelector } from '@store/References/selectors'
import { showError } from '@utils'

export interface ReferencesState {
  rejectReasons: Record<string, string>
  minTip: number
}

const initialState = {
  rejectReasons: {},
  minTip: 0,
}

export const references: ModelConfig<ReferencesState> = {
  state: initialState,
  reducers: {
    setRejectReasons: (state, rejectReasons: ReferencesState['rejectReasons']) => ({
      ...state,
      rejectReasons,
    }),
    setMinTip: (state, minTip: number) => ({
      ...state,
      minTip,
    }),
  },
  // @ts-ignore
  effects: (dispatch) => ({
    async getRejectReasons(forceFetch = false, state: RootState) {
      const exitedRejectReasons = rejectReasonsSelector(state)
      if (!forceFetch && exitedRejectReasons.length) return

      try {
        dispatch.loaders.showLoader(LoaderTypes.RejectReasonsLoader)
        const { data: rejectReasons } = await api.getRejectReasons()
        dispatch.references.setRejectReasons(rejectReasons)
      } catch (error) {
        showError(error)
      } finally {
        dispatch.loaders.hideLoader(LoaderTypes.RejectReasonsLoader)
      }
    },
    async getMinTip(forceFetch = false, state: RootState) {
      const existedMinTip = minTipSelector(state)
      if (!forceFetch && existedMinTip !== 0) return

      try {
        dispatch.loaders.showLoader(LoaderTypes.RejectReasonsLoader)
        const { data: { minTip } } = await api.getMinTip()
        dispatch.references.setMinTip(minTip)
      } catch (error) {
        showError(error)
      } finally {
        dispatch.loaders.hideLoader(LoaderTypes.RejectReasonsLoader)
      }
    },
  }),
}
