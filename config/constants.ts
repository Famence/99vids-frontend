import { Currency, Pagination } from '@models/commonTypes'

export const IS_SERVER = typeof window === 'undefined'

export const serverAppLocalAddress = IS_SERVER ? process.env.SERVER_ADDRESS : null

export const APP_ORIGIN = IS_SERVER ? serverAppLocalAddress as string : window.document.location.origin
export const API_URL = `${APP_ORIGIN}/api`

export const MIN_PASSWORD_LENGTH = 6

export const DEFAULT_PAGE_SIZE = 8

export const DEFAULT_PAGINATION: Pagination = {
  offset: 0,
  limit: DEFAULT_PAGE_SIZE,
}

export const DEFAULT_USERS_PAGINATION = {
  ...DEFAULT_PAGINATION,
  limit: 9,
}

export const currencySigns = {
  [Currency.USD]: '$',
}
