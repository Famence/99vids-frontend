import { FileInfo, FileMetaData, FilePaths, FileTypes } from '@models/File'
import { Url } from '@models/commonTypes'


interface VideoMetaData extends FileMetaData {
  durationInSec: number
}

export interface FileMetaDataBySize {
  [FileTypes.PHOTO]: {
    full: FileMetaData
    thumb: FileMetaData
    demo: FileMetaData
  }
  [FileTypes.VIDEO]: {
    h480: VideoMetaData
    h720: VideoMetaData
    h1080: VideoMetaData
  }
}

export interface PhotoInfo extends FileInfo {
  paths: FilePaths[FileTypes.PHOTO]
  metaDataBySize: FileMetaDataBySize[FileTypes.PHOTO]
}

export interface VideoInfo extends FileInfo {
  paths: FilePaths[FileTypes.VIDEO]
  metaDataBySize: FileMetaDataBySize[FileTypes.VIDEO]
}

export interface PhotoPaths {
  tmp: string
  full: string
  thumb: string
  demo: string
}

export enum VideoQuality {
  auto = 'auto',
  h240 = 'h240',
  h360 = 'h360',
  h480 = 'h480',
  h720 = 'h720',
  h1080 = 'h1080',
  h2160 = 'h2160'
}

export const videoQualities = Object.keys(VideoQuality)

export type VideoQualities = typeof videoQualities

export type VideoPaths = {
  [key in VideoQuality]: Url
}
