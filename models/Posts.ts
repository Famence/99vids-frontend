import { User } from '@models/Users'
import { ID } from '@models/commonTypes'
import { PhotoInfo, VideoInfo } from '@models/MediaFile'

export interface PostMediaItemBase {
  _id: ID
  position: number
}

export interface PostMediaPhotoItem extends PostMediaItemBase {
  photoInfo: PhotoInfo
}

export interface PostMediaVideoItem extends PostMediaItemBase {
  videoInfo: VideoInfo
  cover: ID
  coverInfo: PhotoInfo
}

export type PostMediaItemType = PostMediaPhotoItem | PostMediaVideoItem

export enum PostMediaItemTypes {
  PHOTO = 'photo',
  VIDEO = 'video'
}

export type ExtendedPostMediaPhotoItem = (PostMediaPhotoItem & { type: PostMediaItemTypes })
export type ExtendedPostMediaVideoItem = (PostMediaVideoItem & {
  type: PostMediaItemTypes
  playing: boolean
  canPlayOnScroll: boolean
})

export type ExtendedPostMediaItem = ExtendedPostMediaPhotoItem | ExtendedPostMediaVideoItem

export interface PostMedia {
  photo: PostMediaPhotoItem[]
  video: PostMediaVideoItem[]
}

export enum PostStatus {
  PUBLISHED = 'published',
  UNPUBLISHED = 'unpublished',
}

interface CommonStatistics {
  postsCount: number
  photosCount: number
  videosCount: number
  viewsCount: number
  likesCount: number
  commentsCount: number
  claimsCount: number
}

export type PostStatistics = CommonStatistics
export type UserPostsStatistics = CommonStatistics

export interface PostType<UserType = User> {
  _id: ID
  text: string
  media?: PostMedia
  publicationTime: string
  isPromo: boolean
  author: UserType
  liked: boolean
  statistic: PostStatistics
  subscribed: boolean
  demo?: boolean
  status: PostStatus
  moderated: boolean
  reason?: string
}

export interface FetchedPostType extends Omit<PostType<User['_id']>, 'author'> {
  creatorId: string
}

export enum PostsGroup {
  All = 'all',
  Photos = 'photos',
  Videos = 'videos',
  Published = 'published',
  Unpublished = 'unpublished',
}

export const postsGroupsLabels = {
  [PostsGroup.All]: 'All',
  [PostsGroup.Photos]: 'Photos',
  [PostsGroup.Videos]: 'Videos',
  [PostsGroup.Published]: 'Published',
  [PostsGroup.Unpublished]: 'Unpublished',
}

export const userPostsStatisticsKeysByPostsGroups = {
  [PostsGroup.All]: 'postsCount',
  [PostsGroup.Photos]: 'photosCount',
  [PostsGroup.Videos]: 'videosCount',
}
