import { NextPage } from 'next'
import { AppContext, PageContext } from '@models/App'
import { Store } from '@store/store'
import { AccessGuard } from '@utils/access'

export interface GetInitialDataConfig {
  requestFollowing: boolean
}

export type Page<Props = Record<string, unknown>> = NextPage<Props> & {
  getInitialDataConfig?: GetInitialDataConfig
  requestInitialData?: ({ store, context }: { store: Store, context: AppContext }) => Promise<void>
  accessGuards?: AccessGuard[]
  getInitialProps?: (ctx: PageContext) => Partial<Props> | Promise<Partial<Props>>
}
