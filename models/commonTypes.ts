import { UrlObject } from 'url'
import { FileInfo, FilePaths, FileTypes } from '@models/File'
import { User } from '@models/Users'
import { PostType } from '@models/Posts'
import { FollowingStatus } from '@models/Subscription'
import { RouteParams } from 'next-routes'

export type AnyObject = Record<string, unknown>

export interface ObjectsByKey<T> {
  [key: string]: T
}

export type Values<T> = T[keyof T]

export interface SelectBoxOption {
  value: string
  label: string
}

export type Url = string | URL | UrlObject

export type ID = string

export type ImgSource = FilePaths[FileTypes.PHOTO] | string | null

export enum EntityType {
  Post = 'post'
}

export type Entity = User | PostType | FileInfo

export enum Theme {
  Light = 'light',
  Dark = 'dark',
}

export interface Pagination {
  offset: number
  limit: number
}

export enum Sorting {
  ByDateAsc = 'publicationTime,1',
  ByDateDesc = 'publicationTime,-1',
  ByCreatedAtAsc = 'createdAt,1',
  ByCreatedAtDesc = 'createdAt,-1',
}

export interface SortParams {
  sort: Sorting
}

export interface PostsParams {
  withPromo: boolean
}

export interface PostsGroupParams {
  withPhoto: boolean
  withVideo: boolean
}

export interface FollowingStatusParams {
  [FollowingStatus.Subscribed]: boolean
  [FollowingStatus.Unsubscribed]: boolean
}

export type PaginationParams = Pagination

export enum Currency {
  USD = 'USD',
}

export interface AppRoute {
  name: string
  pattern?: string
  page?: string
  getAs: (params?: RouteParams) => string
}

export interface AppLocation {
  route: AppRoute
  params?: RouteParams
}

export enum FavoritesType {
  Favorite = 'favorite',
  Liked = 'liked',
  Commented = 'commented',
  Tipped = 'tipped',
}

export const favoritesTypeLabels = {
  [FavoritesType.Favorite]: 'Favorites',
  [FavoritesType.Liked]: 'Liked',
  [FavoritesType.Commented]: 'Commented',
  [FavoritesType.Tipped]: 'Tipped',
}
