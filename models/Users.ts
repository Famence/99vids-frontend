import { ID } from '@models/commonTypes'
import { PostType, UserPostsStatistics } from '@models/Posts'
import { TariffType } from '@models/TariffType'
import { PhotoInfo } from '@models/MediaFile'
import { Subscription } from '@models/Subscription'

export enum UserRoles {
  GUEST = 'guest',
  FAN = 'fan',
  MODEL = 'model',
  MODERATOR = 'moderator',
  ADMIN = 'admin'
}

interface UserModeration {
  photo: {
    reason: string
  }
}

export interface User<PromoPostType = PostType> {
  _id: ID
  roleId: UserRoles
  nickname: string
  firstname: string
  displayName: string
  link: string
  promoPost: PromoPostType
  photoByUser: PhotoInfo[]
  photoByProvider: string
  unmoderatedPhoto?: PhotoInfo
  backgroundPhoto: PhotoInfo
  tariffs: TariffType[]
  postsStatistic: UserPostsStatistics
  subscription?: Subscription
  moderation?: UserModeration
}
