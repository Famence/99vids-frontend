import { ServerError } from '@models/errors/ServerError'

export class UnauthorizedError extends ServerError {
  readonly redirectTo?: string

  constructor({ message = 'Ошибка доступа', redirectTo }: Partial<UnauthorizedError> = {}) {
    super(401, message)
    this.redirectTo = redirectTo
  }
}
