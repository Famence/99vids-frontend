export class ServerError extends Error {
  readonly statusCode: number

  readonly message: string

  constructor(statusCode = 500, message = 'Ошибка соединения с сервером') {
    super(message)
    this.statusCode = statusCode
    this.message = message
  }
}
