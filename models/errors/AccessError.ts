import { ServerError } from '@models/errors/ServerError'
import { AppContext } from '@models/App'

export class AccessError extends ServerError {
  readonly redirectTo?: string

  readonly onRedirect?: (context: AppContext) => (void | Promise<void>)

  readonly onCatch?: (context: AppContext) => (void | Promise<void>)

  constructor({ message = 'Ошибка доступа', redirectTo, onRedirect, onCatch }: Partial<AccessError> = {}) {
    super(403, message)
    this.redirectTo = redirectTo
    this.onRedirect = onRedirect
    this.onCatch = onCatch
  }
}
