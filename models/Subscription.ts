import { ID } from '@models/commonTypes'
import { User } from '@models/Users'
import { PriceRules } from '@models/TariffType'

export enum SubscriptionStatus {
  Active = 'active',
  Deleted = 'deleted',
}

export type Subscription = {
  _id: ID
  status: SubscriptionStatus
  isMuted: boolean
  creatorId: User['_id']
  to: User['_id']
  validUntil: string
  tariffId: ID
  price: number
  priceRules: PriceRules
  currentDuration: string
}

export enum FollowingStatus {
  Subscribed = 'subscribed',
  Unsubscribed = 'unsubscribed',
  All = 'all',
}

export const followingStatusLabels = {
  [FollowingStatus.Subscribed]: 'Active',
  [FollowingStatus.Unsubscribed]: 'Unsubscribed',
  [FollowingStatus.All]: 'All',
}
