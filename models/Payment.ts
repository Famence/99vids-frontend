import { Currency, ID } from '@models/commonTypes'

export enum PaymentPurpose {
  Tips = 'tips'
}

export interface Payment {
  _id: ID
  currency: Currency
  confirmed: boolean
  amount: number
  purpose: PaymentPurpose
  creatorId: ID
  recipient: ID
  message: string
  createdAt: string
  updatedAt: string
}
