import { AppContext as NextAppContext, AppInitialProps } from 'next/dist/pages/_app'
import { NextPageContext } from 'next'
import { AxiosRequestConfig } from 'axios'
import { isMobileResult } from 'ismobilejs/types/isMobile'
import { Page } from '@models/Pages'
import { AccessError, ServerError } from '@models/errors'
import { Store } from '@store/store'

export interface PageContext extends NextPageContext {
  res: NextAppContext['ctx']['res'] & {
    locals: Record<string, any> & {
      store: Store
      requestConfig: AxiosRequestConfig
      isMobile: isMobileResult
    }
  }
  err?: ServerError | AccessError // TODO: more error types (i.e. ModuleBuildError)
}

export type AppContext = NextAppContext & {
  Component: Page<AppInitialProps>
  ctx: PageContext
}
