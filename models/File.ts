import { ID } from '@models/commonTypes'
import { PhotoPaths, VideoPaths } from '@models/MediaFile'


export interface FileMetaData {
  sizeInBytes: number
  h: number
  w: number
}

export enum FileTypes {
  PHOTO,
  VIDEO
}

export enum FileStatus {
  READY = 'ready',
  IN_PROGRESS = 'inProgress'
}

export interface FilePaths {
  [FileTypes.PHOTO]: PhotoPaths
  [FileTypes.VIDEO]: VideoPaths
}

export interface FileInfo {
  _id: ID
  status: FileStatus
}
