import { ID } from '@models/commonTypes'

export interface PriceRules {
  [key: string]: number
}

export interface TariffType {
  _id: ID
  name: string
  discount: string
  priceRules: PriceRules
}
