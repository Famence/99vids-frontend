declare module '*.svg' {
  import React from 'react'

  export interface SvgIconProps extends React.SVGProps {
    isFilled?: boolean
    isInline?: boolean
    className?: string
  }

  export type SvgIcon = React.FunctionComponent<SvgIconProps>

  export default (props: SvgIconProps) => React.ReactSVGElement
}
